using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

namespace Sound_Engine
{
    public static class SoundEngine
    {
        private static List<SoundEffectInstance> _sound_instances = new List<SoundEffectInstance>();
        private static List<SoundEffect> _sounds = new List<SoundEffect>();
        private static ContentManager _content;
        private static Dictionary<string, int> _sound_dictionary = new Dictionary<string, int>();
        private static Dictionary<string, int> _soundInstance_dictionary = new Dictionary<string, int>();
        
        private static Random rand = new Random();
        /// <summary>
        /// Initializes the SoundEngine
        /// </summary>
        /// <param name="content"></param>
        public static void Initialize(ContentManager content)
        {
            try
            {
                _content = content;
            }
            catch
            {
                Debug.Write("SoundEngine Initialization Failed ");
            }
        }
        /// <summary>
        /// Allow the addition of a new sound into the sound dictionary
        /// </summary>
        /// <param name="name">A name to reference the sound</param>
        /// <param name="path">A path to the .wav file loaded using _content variable</param>
        public static void AddSound(string name, string path)
        {
            _sounds.Add(_content.Load<SoundEffect>(path));
            _sound_dictionary.Add(name, _sounds.Count-1);
        }

        /// <summary>
        /// Allow the addition of a new sound into the sound dictionary
        /// </summary>
        /// <param name="name">A name to reference the sound</param>
        /// <param name="path">A path to the .wav file loaded using _content variable</param>
        public static void AddSoundInstance(string name, string path)
        {
            _sound_instances.Add(_content.Load<SoundEffect>(path).CreateInstance());
            _soundInstance_dictionary.Add(name, _sound_instances.Count - 1);
        }


        /// <summary>
        /// Allows the addition of multiple sounds in one function.
        /// </summary>
        /// <param name="names">a List of names to reference each sound. Size must be the same as size of paths</param>
        /// <param name="paths">a List of paths to reference each sound. Size must be the same as size of names</param>
        public static void AddSoundList(List<string> names, List<string> paths)
        {
            for(int i = 0;i < names.Count; i ++)
            {
                AddSound(names[i], paths[i]);
            }
        }
        /// <summary>
        /// Play a sound with given string name
        /// </summary>
        /// <param name="name">The name of the sound you want to play</param>
        public static void Play(string name)
        {
            try
            {
                if (_soundInstance_dictionary.ContainsKey(name))
                {
                    _sound_instances[_soundInstance_dictionary[name]].Play();
                }
                else if (_sound_dictionary.ContainsKey(name))
                {
                    _sounds[_sound_dictionary[name]].Play();
                }
            }
            catch
            {
                
                Debug.Write("Play Failed ");
            }
        }

        public static void Stop(string name)
        {
            try
            {
                _sound_instances[_soundInstance_dictionary[name]].Stop();
            }
            catch
            {
                Debug.Write("Stop Failed ");
            }
        }

        public static void Pause(string name)
        {
            try
            {
                _sound_instances[_soundInstance_dictionary[name]].Pause();
            }
            catch
            {
                Debug.Write("Stop Failed ");
            }
        }

        public static SoundEffectInstance getInstance(string name)
        {
            try
            {
                return _sound_instances[_soundInstance_dictionary[name]];
            }
            catch
            {
                Debug.Write("Stop Failed ");
                return null;
            }
        }

        
        //public static SoundEffectInstance CreateInstance(string name)
        //{
        //    try
        //    {
        //      return _sounds[_sound_dictionary[name]].CreateInstance();
        //    }
        //    catch
        //    {
        //        Debug.Write("SoundEffectInstance Creation Failed ");
        //        return null;
        //    }
        
        //}
    }
}
