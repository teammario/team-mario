﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Level_Editor
{
    class XMLReader
    {
        public static List<List<int>> tileLayers = new List<List<int>>();
        public static Dictionary<string, int> tileLayerList = new Dictionary<string, int>();

        public static List<List<GameObject>> objectLayers = new List<List<GameObject>>();
        public static Dictionary<string, int> objectLayerList = new Dictionary<string, int>();

        public static int mapWidth = 1;
        public static int mapHeight = 1;
        public static int tileWidth = 1;
        public static int tileHeight = 1;

        public static void read(string filename)
        {
            XmlReader reader =XmlReader.Create(filename);
            string name = "";
            

            tileLayerList.Add("Background", tileLayerList.Count);
            tileLayerList.Add("Foreground", tileLayerList.Count);

            objectLayerList.Add("Enemy", objectLayerList.Count);
            objectLayerList.Add("Interaction", objectLayerList.Count);
            objectLayerList.Add("Spawn", objectLayerList.Count);
            
            while (reader.Read())
            {
                if (reader.HasAttributes)
                {

                    switch (reader.Name)
                    {
                        case "map": //map properties
                            mapWidth = Convert.ToInt32(reader.GetAttribute("width"));
                            mapHeight = Convert.ToInt32(reader.GetAttribute("height"));
                            tileWidth = Convert.ToInt32(reader.GetAttribute("tilewidth"));
                            tileHeight = Convert.ToInt32(reader.GetAttribute("tileheight"));
                            break;
                        case "tileset":
                            break;
                        case "layer": //tile layers
                            name = reader.GetAttribute("name");
                            break;
                        case "tile":
                            if (tileLayerList.ContainsKey(name))
                            {
                                tileLayers[tileLayerList[name]].Add(Convert.ToInt32(reader.GetAttribute("gid")));
                            }
                            if (objectLayerList.ContainsKey(name))
                            {
                                objectLayers[objectLayerList[name]].Add(new GameObject(reader.GetAttribute("name"),
                                    Convert.ToInt32(reader.GetAttribute("x")), Convert.ToInt32(reader.GetAttribute("y"))));
                            }
                            break;
                        case "objectgroup": //object layers
                            name = reader.GetAttribute("name");

                            break;
                        case "object":

                            objectLayers[objectLayerList[name]].Add(new GameObject(reader.GetAttribute("name"),
                                Convert.ToInt32(reader.GetAttribute("x")), Convert.ToInt32(reader.GetAttribute("y"))));

                            break;
                        case "property": //a property set for an object or tile
                            if (reader.GetAttribute("name") == "color")
                            {
                                objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count].color = reader.GetAttribute("value");
                            }
                            if (reader.GetAttribute("name") == "direction")
                            {
                                objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count].direction = reader.GetAttribute("value");
                            }
                            if (reader.GetAttribute("name") == "objectTypes")
                            {
                                objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count].objectTypes = reader.GetAttribute("value");
                            }
                            if (reader.GetAttribute("name") == "item")
                            {
                                objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count].item = reader.GetAttribute("value");
                            }
                            if (reader.GetAttribute("name") == "count")
                            {
                                objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count].count = Convert.ToInt32(reader.GetAttribute("value"));
                            }
                            if (reader.GetAttribute("name") == "warpInfo")
                            {
                                objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count].warpInfo = reader.GetAttribute("value");
                            }
                            if (reader.GetAttribute("name") == "ID")
                            {
                                objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count].ID = Convert.ToInt32(reader.GetAttribute("value"));
                            }
                            break;

                    }

                }
            }
        }
    }

    public class GameObject
    {
        public string name ="";
        public int x = 0;
        public int y = 0;
        public string color = "";
        public string direction = "";
        public string objectTypes = ""; //walker, jumper, Swimmer, Shooter, Piranha Plant, Firebar
        public string item = "";
        public int count = 0;
        public string warpInfo = "";
        public int ID = 0;

        public GameObject(string name,int x,int y)
        {
            this.name = name;
            this.x = x;
            this.y = y;
        }

        public GameObject(string name, int x, int y, string color,
            string direction, string objectTypes, string item,
            int count, string warpInfo, int ID)
        {
            this.name = name;
            this.x = x;
            this.y = y;
            this.color = color;
            this.direction = direction;
            this.objectTypes = objectTypes;
            this.item = item;
            this.count = count;
            this.warpInfo = warpInfo;
            this.ID = ID;
        }
    }
}
