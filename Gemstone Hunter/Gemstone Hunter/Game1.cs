using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Tile_Engine;

namespace Gemstone_Hunter
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Player player;
        SpriteFont pericles8;
        Vector2 scorePosition = new Vector2(20, 10); //sets position of score on hud
        enum GameState { TitleScreen, Playing, PlayerDead, GameOver };
        GameState gameState = GameState.TitleScreen;
        int maxLevelTimer = 400000;
        int levelTimer = 0;
        

        Vector2 gameOverPosition = new Vector2(350, 300);//sets game over position
        Vector2 livesPosition = new Vector2(550, 10); //sets lives hud  position
        Vector2 timerPosition = new Vector2(650, 10); //sets timer hud position
        Vector2 worldNumber = new Vector2(450, 10);  //sets world number hud position
        Vector2 coinNumber = new Vector2(300, 10);  //sets the coins hud position


        Texture2D titleScreen;
        
        float deathTimer = 0.0f;
        float deathDelay = 5.0f;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            this.graphics.PreferredBackBufferWidth = 800;
            this.graphics.PreferredBackBufferHeight = 600;
            this.graphics.ApplyChanges();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            TileMap.Initialize(
                Content.Load<Texture2D>(@"Textures\PlatformTiles"));
            TileMap.spriteFont =
                Content.Load<SpriteFont>(@"Fonts\Pericles8");

            pericles8 = Content.Load<SpriteFont>(@"Fonts\Pericles8");

            titleScreen = Content.Load<Texture2D>(@"Textures\TitleScreen");

            Camera.WorldRectangle = new Rectangle(0, 0, 160 * 48, 12 * 48);
            Camera.Position = Vector2.Zero;
            Camera.ViewPortWidth = 800;
            Camera.ViewPortHeight = 600;

            player = new Player(Content);
            LevelManager.Initialize(Content, player);
        }

        private void StartNewGame()
        {
            player.Revive();
            player.LivesRemaining = 3;
            
            player.WorldLocation = Vector2.Zero;
            LevelManager.LoadLevel(0);
            levelTimer = maxLevelTimer;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back ==
                ButtonState.Pressed)
                this.Exit();

            KeyboardState keyState = Keyboard.GetState();
            GamePadState gamepadState = GamePad.GetState(PlayerIndex.One);
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (gameState == GameState.TitleScreen)
            {
                if (keyState.IsKeyDown(Keys.Space) ||
                    gamepadState.Buttons.A == ButtonState.Pressed)
                {
                    StartNewGame();
                    gameState = GameState.Playing;
                }
            }

            if (gameState == GameState.Playing)
            {
                player.Update(gameTime);
                LevelManager.Update(gameTime);
                levelTimer -= gameTime.ElapsedGameTime.Milliseconds;
                if (player.Dead)
                {
                    if (player.LivesRemaining > 0)
                    {
                        gameState = GameState.PlayerDead;
                        deathTimer = 0.0f;
                    }
                    else
                    {
                        gameState = GameState.GameOver;
                        deathTimer = 0.0f;
                    }
                }
            }

            if (gameState == GameState.PlayerDead)
            {
                player.Update(gameTime);
                LevelManager.Update(gameTime);
                deathTimer += elapsed;
                if (deathTimer > deathDelay)
                {
                    player.WorldLocation = Vector2.Zero;
                    LevelManager.ReloadLevel();
                    player.Revive();
                    levelTimer = maxLevelTimer;
                    gameState = GameState.Playing;
                }
            }

            if (gameState == GameState.GameOver)
            {
                deathTimer += elapsed;
                if (deathTimer > deathDelay)
                {
                    gameState = GameState.TitleScreen;
                }
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin(
                SpriteSortMode.BackToFront,
                BlendState.AlphaBlend);

            if (gameState == GameState.TitleScreen)
            {
                spriteBatch.Draw(titleScreen, Vector2.Zero, Color.White);
            }

            if ((gameState == GameState.Playing) ||
                (gameState == GameState.PlayerDead) ||
                (gameState == GameState.GameOver))
            {
                TileMap.Draw(spriteBatch);
                player.Draw(spriteBatch);
                LevelManager.Draw(spriteBatch);
                //prints the score at the desired position was original to gem hunters game
                spriteBatch.DrawString(pericles8, "   Mario \nScore: " + player.Score.ToString(),scorePosition,Color.White);
                //gets the number of lives this was defaulted in the original gem hunters game
                spriteBatch.DrawString(pericles8,"Lives : " + player.LivesRemaining.ToString(),livesPosition,Color.White);
                //gets the time left. I need someone to come up with a timer class that does a timer clock to count down when the level loads
                //and stops at the end of the level. It also has to kill mario if it reaches 000 before mario finishes the level
                spriteBatch.DrawString(pericles8, "Time : " + levelTimer/1000, timerPosition, Color.White);
                //spriteBatch.DrawString(pericles8,"Time : " + player.TimeRemaining.ToString(), timerPosition, Color.White);
                //prints the world number.  This works and is based on the original world numbers loaded to the LevelManager class
                spriteBatch.DrawString(pericles8, "World \n " + LevelManager.CurrentLevel.ToString(), worldNumber, Color.White);
                //prints the number of coins gotten.  This also works but assumes the gemstones are coins. It is figured in LevelManager
                //but sent to the Player class just like gem half of score is 
                spriteBatch.DrawString(pericles8, "Coins X  " + player.Coins.ToString(), coinNumber, Color.White);
            }

            if (gameState == GameState.PlayerDead)
            {
            }

            if (gameState == GameState.GameOver)
            {
                spriteBatch.DrawString(
                    pericles8,
                    "G A M E  O V E R !",
                    gameOverPosition,
                    Color.White);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }

    }
}
