﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class Coin : GameObject
    {
        #region Constructor
        public Coin(ContentManager Content, int cellX, int cellY)
        {
            worldLocation.X = cellX;
            worldLocation.Y = cellY;

            animations.Add("idle",
                new AnimationStrip(
                    Content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "item_coin_std", 3));

            animations["idle"].LoopAnimation = true;
            animations["idle"].FrameLength = 0.37f;
            currentAnimation = "idle";
            animations["idle"].NextAnimation = "idle";
            drawDepth = 0.875f;

            PlayAnimation("idle");
            collisionRectangle = new Rectangle(0, 0, animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
            enabled = true;
        }
        #endregion

    }
}
