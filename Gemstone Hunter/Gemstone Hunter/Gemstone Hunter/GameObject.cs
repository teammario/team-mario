﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tile_Engine;


namespace Gemstone_Hunter
{
    public class GameObject
    {
        #region Declarations
        protected SpriteEffects effect = SpriteEffects.None;
        protected bool isVerticallyCollidable = true;
        protected bool isHorizontallyCollidable = true;
        protected Vector2 worldLocation;
        protected Vector2 velocity;
        protected int frameWidth;
        protected int frameHeight;
        public bool movingVertically = false;
       
        protected bool enabled;
        protected bool visible = true;
        protected bool flipped = false;
        protected float rotation = 0.0f;
        protected Color visibleColor = Color.White;

        protected enum myColor {red, white, blue};
        new Dictionary<myColor,Color> myDict = new Dictionary<myColor,Color>();
        protected myColor paintColor = myColor.white;
        
        protected bool onGround;
        protected bool isClimbable = false;
        protected bool passable = true;
        protected bool breakable = false;
       
        protected Rectangle collisionRectangle;
        protected int collideWidth;
        protected int collideHeight;
        protected bool codeBasedBlocks = true;

        protected float drawDepth = 0.85f;
        protected Dictionary<string, AnimationStrip> animations =
            new Dictionary<string, AnimationStrip>();
        protected string currentAnimation;

        //new for breaking blocks
        protected bool canBreak = false;
        protected bool canBump = false;

        #endregion
        

        #region Properties

        //new for breaking blocks

        public bool CanBreak
        {
            get
            {
                return canBreak;
            }
            set
            {
                canBreak = value;
            }
        }

        public bool Enabled
        {
            get { return enabled; }
            set { enabled = value; }
        }

        public bool Passable
        {
            get { return passable; }
            set { passable = value; }
        }

        public bool Breakable
        {
            get { return breakable; }
            set { breakable = value; }
        }

        public bool Flipped
        {
            get { return flipped; }
            set { flipped = value; }
        }

        public Vector2 WorldLocation
        {
            get { return worldLocation; }
            set { worldLocation = value; }
        }

        public bool IsVerticallyCollidable
        {
            get { return isVerticallyCollidable; }
            set { isVerticallyCollidable = value; }
        }

        public bool IsHorizontallyCollidable
        {
            get { return isHorizontallyCollidable; }
            set { isHorizontallyCollidable = value; }
        }

        public Vector2 WorldCenter
        {
            get
            {
                return new Vector2(
                  (int)worldLocation.X + (int)(frameWidth / 2),
                  (int)worldLocation.Y + (int)(frameHeight / 2));
            }
        }

        public Rectangle WorldRectangle
        {
            get
            {
                return new Rectangle(
                    (int)worldLocation.X,
                    (int)worldLocation.Y,
                    frameWidth,
                    frameHeight);
            }
        }

        public Rectangle CollisionRectangle
        {
            get
            {
                //this.collisionRectangle.X = 0;
                return new Rectangle(
                    (int)worldLocation.X + collisionRectangle.X,
                    (int)WorldRectangle.Y + collisionRectangle.Y,
                    collisionRectangle.Width,
                    collisionRectangle.Height);
            }
            set { collisionRectangle = value; }
        }
        #endregion 

        #region Helper Methods
        public bool animationFinished
        {
            get
            {
                return animations[currentAnimation].FinishedPlaying;
            }
        }

        private void updateAnimation(GameTime gameTime)
        {
            if (animations.ContainsKey(currentAnimation))
            {
                if (animations[currentAnimation].FinishedPlaying)
                {
                    PlayAnimation(animations[currentAnimation].NextAnimation);
                }
                else
                {
                    animations[currentAnimation].Update(gameTime);
                }
                resetCollisionRectangle();
            }
        }
        #endregion

        #region Map-Based Collision Detection Methods
        protected Vector2 horizontalCollisionTest(Vector2 moveAmount)
        {
            if (moveAmount.X == 0)
                return moveAmount;

            Rectangle afterMoveRect = CollisionRectangle;
            afterMoveRect.Offset((int)moveAmount.X, 0);
            Vector2 corner1, corner2, center;

            if (moveAmount.X < 0)
            {
                corner1 = new Vector2(afterMoveRect.Left,
                                      afterMoveRect.Top + 1);
                corner2 = new Vector2(afterMoveRect.Left,
                                      afterMoveRect.Bottom - 5);
                center = new Vector2(afterMoveRect.Left,
                                      (afterMoveRect.Top+1 + afterMoveRect.Bottom -5)/ 2);
            }
            else
            {
                corner1 = new Vector2(afterMoveRect.Right,
                                      afterMoveRect.Top + 1);
                corner2 = new Vector2(afterMoveRect.Right,
                                      afterMoveRect.Bottom - 5);
                center = new Vector2(afterMoveRect.Right,
                                      (afterMoveRect.Top+1 + afterMoveRect.Bottom-5)/ 2);
            }

            Vector2 mapCell1 = TileMap.GetCellByPixel(corner1);
            Vector2 mapCell2 = TileMap.GetCellByPixel(corner2);
            Vector2 mapCell3 = TileMap.GetCellByPixel(center);

            if ((!TileMap.CellIsPassable(mapCell1) ||
                !TileMap.CellIsPassable(mapCell2) ||
                !TileMap.CellIsPassable(mapCell3))||
               (LevelManager.isHorizontallyColliding(this, moveAmount)))
            {
                moveAmount.X = 0;
                velocity.X = 0;
                //BumpLeft();
            }

            return moveAmount;
        }

        protected Vector2 verticalCollisionTest(Vector2 moveAmount, bool movingVertically)
        {
            if (moveAmount.Y == 0)
                return moveAmount;

            Rectangle afterMoveRect = CollisionRectangle;
            afterMoveRect.Offset((int)moveAmount.X, (int)moveAmount.Y);
            Vector2 corner1, corner2, center;

            if (moveAmount.Y < 0)
            {
                corner1 = new Vector2(afterMoveRect.Left + 5,
                                      afterMoveRect.Top);
                corner2 = new Vector2(afterMoveRect.Right - 5,
                                      afterMoveRect.Top);
                center = new Vector2((afterMoveRect.Right+ afterMoveRect.Left)/2,
                                      afterMoveRect.Top);
            }
            else
            {
                corner1 = new Vector2(afterMoveRect.Left + 5,
                                      afterMoveRect.Bottom);
                corner2 = new Vector2(afterMoveRect.Right - 5,
                                      afterMoveRect.Bottom);
                center = new Vector2((afterMoveRect.Right + afterMoveRect.Left) / 2,
                                      afterMoveRect.Bottom);
            }

            //test Tiles and Objects
            Vector2 mapCell1 = TileMap.GetCellByPixel(corner1);
            Vector2 mapCell2 = TileMap.GetCellByPixel(corner2);
            Vector2 mapCell3 = TileMap.GetCellByPixel(center);

            if ((!TileMap.CellIsPassable(mapCell1) ||
                !TileMap.CellIsPassable(mapCell2) ||
                !TileMap.CellIsPassable(mapCell3)) ||
            (LevelManager.isVerticallyColliding(this,moveAmount,movingVertically)))
            {
                if (moveAmount.Y > 0)
                   onGround = true;
                else
                {

                    if (Sound_Engine.SoundEngine.getInstance("HitWall").State != Microsoft.Xna.Framework.Audio.SoundState.Playing)
                    {
                        Sound_Engine.SoundEngine.Play("HitWall");
                    }

         //           TileMap.MapCells[(int)mapCell1.X, (int)mapCell1.Y] = new MapSquare(0, 0, true, false, false);

                    if (//TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].Breakable == true
                        TileMap.CellIsBreakable(mapCell3) == true)
                    {
                        if (CanBreak)
                        {
                            //TileMap.MapCells[(int)mapCell1.X, (int)mapCell1.Y] = new MapSquare(0, 0, true, false, false);

                            if (TileMap.mapCells[(int)mapCell3.X, (int)mapCell3.Y].LayerTiles[0] == 2 ||
                                TileMap.mapCells[(int)mapCell3.X, (int)mapCell3.Y].LayerTiles[0] == 3)
                            {
                                LevelManager.newBreakBlock((int)mapCell3.X * 48, (int)mapCell3.Y * 48, 2, true);
                            }
                            else if (TileMap.mapCells[(int)mapCell3.X, (int)mapCell3.Y].LayerTiles[0] == 31 ||
                                TileMap.mapCells[(int)mapCell3.X, (int)mapCell3.Y].LayerTiles[0] == 32)
                            {
                                LevelManager.newBreakBlock((int)mapCell3.X * 48, (int)mapCell3.Y * 48, 31, true);
                            }
                            else if (TileMap.mapCells[(int)mapCell3.X, (int)mapCell3.Y].LayerTiles[0] == 60 ||
                                TileMap.mapCells[(int)mapCell3.X, (int)mapCell3.Y].LayerTiles[0] == 61)
                            {
                                LevelManager.newBreakBlock((int)mapCell3.X * 48, (int)mapCell3.Y * 48, 60, true);

                            }
                            TileMap.MapCells[(int)mapCell3.X, (int)mapCell3.Y] = new MapSquare(0, 0, true, false, false);
                            if (Sound_Engine.SoundEngine.getInstance("BlockBreak").State != Microsoft.Xna.Framework.Audio.SoundState.Playing)
                            {
                                Sound_Engine.SoundEngine.Play("BlockBreak");
                            }
                        }
                        else if(canBump)
                        {
                         //   TileMap.MapCells[(int)mapCell1.X, (int)mapCell1.Y] = new MapSquare(1, 0, false, true, false);
                            if (TileMap.mapCells[(int)mapCell3.X, (int)mapCell3.Y].LayerTiles[0] == 2 ||
                                TileMap.mapCells[(int)mapCell3.X, (int)mapCell3.Y].LayerTiles[0] == 3)
                            {
                                LevelManager.newBreakBlock((int)mapCell3.X * 48, (int)mapCell3.Y * 48,
                                    TileMap.mapCells[(int)mapCell3.X, (int)mapCell3.Y].LayerTiles[0], false);
                            }
                            else if (TileMap.mapCells[(int)mapCell3.X, (int)mapCell3.Y].LayerTiles[0] == 31 ||
                                TileMap.mapCells[(int)mapCell3.X, (int)mapCell3.Y].LayerTiles[0] == 32)
                            {
                                LevelManager.newBreakBlock((int)mapCell3.X * 48, (int)mapCell3.Y * 48,
                                    TileMap.mapCells[(int)mapCell3.X, (int)mapCell3.Y].LayerTiles[0], false);
                            }
                            else if (TileMap.mapCells[(int)mapCell3.X, (int)mapCell3.Y].LayerTiles[0] == 60 ||
                                TileMap.mapCells[(int)mapCell3.X, (int)mapCell3.Y].LayerTiles[0] == 61)
                            {
                                LevelManager.newBreakBlock((int)mapCell3.X * 48, (int)mapCell3.Y * 48,
                                    TileMap.mapCells[(int)mapCell3.X, (int)mapCell3.Y].LayerTiles[0], false);
                            }

                            TileMap.MapCells[(int)mapCell3.X, (int)mapCell3.Y] = new MapSquare(0, 0, false, false, false);

                        }
                    }
                    else if (//TileMap.MapCells[(int)mapCell2.X, (int)mapCell2.Y].Breakable == true)
                             TileMap.CellIsBreakable(mapCell2) == true)
                    {
                        if (CanBreak)
                        {
                            //TileMap.MapCells[(int)mapCell2.X, (int)mapCell2.Y] = new MapSquare(0, 0, true, false, false);

                            if (TileMap.mapCells[(int)mapCell2.X, (int)mapCell2.Y].LayerTiles[0] == 2 ||
                                TileMap.mapCells[(int)mapCell2.X, (int)mapCell2.Y].LayerTiles[0] == 3)
                            {
                                LevelManager.newBreakBlock((int)mapCell2.X * 48, (int)mapCell2.Y * 48, 2, true);
                            }
                            else if (TileMap.mapCells[(int)mapCell2.X, (int)mapCell2.Y].LayerTiles[0] == 31 ||
                                TileMap.mapCells[(int)mapCell2.X, (int)mapCell2.Y].LayerTiles[0] == 32)
                            {
                                LevelManager.newBreakBlock((int)mapCell2.X * 48, (int)mapCell2.Y * 48, 31, true);
                            }
                            else if (TileMap.mapCells[(int)mapCell2.X, (int)mapCell2.Y].LayerTiles[0] == 60 ||
                                TileMap.mapCells[(int)mapCell2.X, (int)mapCell2.Y].LayerTiles[0] == 61)
                            {
                                LevelManager.newBreakBlock((int)mapCell2.X * 48, (int)mapCell2.Y * 48, 60, true);
                            }

                            TileMap.MapCells[(int)mapCell2.X, (int)mapCell2.Y] = new MapSquare(0, 0, true, false, false);
                            if (Sound_Engine.SoundEngine.getInstance("BlockBreak").State != Microsoft.Xna.Framework.Audio.SoundState.Playing)
                            {
                                Sound_Engine.SoundEngine.Play("BlockBreak");
                            }
                        }
                        else if(canBump)
                        {
                            if (TileMap.mapCells[(int)mapCell2.X, (int)mapCell2.Y].LayerTiles[0] == 2 ||
                                TileMap.mapCells[(int)mapCell2.X, (int)mapCell2.Y].LayerTiles[0] == 3 )
                            {
                                LevelManager.newBreakBlock((int)mapCell2.X * 48, (int)mapCell2.Y * 48,
                                    TileMap.mapCells[(int)mapCell2.X, (int)mapCell2.Y].LayerTiles[0], false);
                            }
                            else if (TileMap.mapCells[(int)mapCell2.X, (int)mapCell2.Y].LayerTiles[0] == 31 ||
                                TileMap.mapCells[(int)mapCell2.X, (int)mapCell2.Y].LayerTiles[0] == 32)
                            {
                                LevelManager.newBreakBlock((int)mapCell2.X * 48, (int)mapCell2.Y * 48,
                                    TileMap.mapCells[(int)mapCell2.X, (int)mapCell2.Y].LayerTiles[0], false);
                            }
                            else if (TileMap.mapCells[(int)mapCell2.X, (int)mapCell2.Y].LayerTiles[0] == 60 ||
                                TileMap.mapCells[(int)mapCell2.X, (int)mapCell2.Y].LayerTiles[0] == 61)
                            {
                                LevelManager.newBreakBlock((int)mapCell2.X * 48, (int)mapCell2.Y * 48,
                                    TileMap.mapCells[(int)mapCell2.X, (int)mapCell2.Y].LayerTiles[0], false);
                            }

                            TileMap.MapCells[(int)mapCell2.X, (int)mapCell2.Y] = new MapSquare(0, 0, false, false, false);

                        }
                    }
                    else if (//TileMap.MapCells[(int)mapCell3.X, (int)mapCell3.Y].Breakable == true
                             TileMap.CellIsBreakable(mapCell1) == true)
                    {
                        if (CanBreak)
                        {
                            //TileMap.MapCells[(int)mapCell3.X, (int)mapCell3.Y] = new MapSquare(0, 0, true, false, false);

                            if (TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].LayerTiles[0] == 2 ||
                                TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].LayerTiles[0] == 3)
                            {
                                LevelManager.newBreakBlock((int)mapCell1.X * 48, (int)mapCell1.Y * 48, 2, true);
                            }
                            else if (TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].LayerTiles[0] == 31 ||
                                    TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].LayerTiles[0] == 32)
                            {
                                LevelManager.newBreakBlock((int)mapCell1.X * 48, (int)mapCell1.Y * 48, 31, true);
                            }
                            else if (TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].LayerTiles[0] == 60 ||
                                    TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].LayerTiles[0] == 61)
                            {
                                LevelManager.newBreakBlock((int)mapCell1.X * 48, (int)mapCell1.Y * 48, 60, true);
                            }

                            TileMap.MapCells[(int)mapCell1.X, (int)mapCell1.Y] = new MapSquare(0, 0, true, false, false);
                            if (Sound_Engine.SoundEngine.getInstance("BlockBreak").State != Microsoft.Xna.Framework.Audio.SoundState.Playing)
                            {
                                Sound_Engine.SoundEngine.Play("BlockBreak");
                            }
                        }
                        else if(canBump)
                        {
                            if (TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].LayerTiles[0] == 2 ||
                                TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].LayerTiles[0] == 3)
                            {
                                LevelManager.newBreakBlock((int)mapCell1.X * 48, (int)mapCell1.Y * 48,
                                    TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].LayerTiles[0], false);
                            }
                            else if (TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].LayerTiles[0] == 31 ||
                                TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].LayerTiles[0] == 32)
                            {
                                LevelManager.newBreakBlock((int)mapCell1.X * 48, (int)mapCell1.Y * 48,
                                    TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].LayerTiles[0], false);
                            }
                            else if (TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].LayerTiles[0] == 60 ||
                                TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].LayerTiles[0] == 61)
                            {
                                LevelManager.newBreakBlock((int)mapCell1.X * 48, (int)mapCell1.Y * 48,
                                    TileMap.mapCells[(int)mapCell1.X, (int)mapCell1.Y].LayerTiles[0], false);
                            }

                            TileMap.MapCells[(int)mapCell1.X, (int)mapCell1.Y] = new MapSquare(0, 0, false, false, false);

                        }
                    }

                }
                moveAmount.Y = 0;
                velocity.Y = 0;
                

            }
            
            return moveAmount;
        }

        protected void resetCollisionRectangle()
        {
            Rectangle newRectangle = new Rectangle(0, 0, animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);

            if (this is Player)
            {
                newRectangle.Width = 48;
                Player p = this as Player;
                if (p.isBig||p.isFire||p.isStarBig)
                {
                    newRectangle.Height = 96;
                }
                else
                {
                    newRectangle.Height = 48;
                }
            }
            collisionRectangle = newRectangle;
        }

        protected void BumpLeft()
        {
            //find the location of the block you are standing in and bump above it
            resetCollisionRectangle();
            Rectangle x = TileMap.CellWorldRectangle(TileMap.GetCellByPixelX((int)WorldLocation.X+5), TileMap.GetCellByPixelY((int)WorldLocation.Y));
            WorldLocation = new Vector2(TileMap.CellWorldRectangle(TileMap.GetCellByPixelX((int)WorldLocation.X+5), TileMap.GetCellByPixelY((int)WorldLocation.Y)).X,WorldLocation.Y);
            WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y);
        }
        #endregion

        #region Public Methods
        public void PlayAnimation(string name)
        {
            if (!(name == null) && animations.ContainsKey(name))
            {
                currentAnimation = name;
                animations[name].Play();
            }
        }

        public virtual void Update(GameTime gameTime)
        {
            if (currentAnimation != "")
            {
                resetCollisionRectangle();
            }
            if (!enabled)
                return;

            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            updateAnimation(gameTime);

            if (velocity.Y != 0)
            {
                onGround = false;
            }

            Vector2 moveAmount = velocity * elapsed;
            if (isHorizontallyCollidable)
            {
                moveAmount = horizontalCollisionTest(moveAmount);
            }
            if (isVerticallyCollidable)
            {
                moveAmount = verticalCollisionTest(moveAmount, movingVertically);
            }
            

            Vector2 newPosition = worldLocation + moveAmount;

            newPosition = new Vector2(
                MathHelper.Clamp(newPosition.X, 0,
                  Camera.WorldRectangle.Width - frameWidth),
                MathHelper.Clamp(newPosition.Y, 2 * (-TileMap.TileHeight),
                  Camera.WorldRectangle.Height - frameHeight));

            worldLocation = newPosition;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (!enabled)
                return;
            if (animations.ContainsKey(currentAnimation))
            {

                if (flipped)
                {
                    effect = SpriteEffects.FlipHorizontally;
                }
                else
                {
                    effect = SpriteEffects.None;
                }
                Rectangle drawRectangle;
                if (animations.Count > 1)
                {
                    drawRectangle = new Rectangle(WorldRectangle.X + (48 - animations[currentAnimation].FrameRectangle.Width) / 2, WorldRectangle.Y, animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
                }
                else
                {
                    drawRectangle = new Rectangle(WorldRectangle.X, WorldRectangle.Y, animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
                }
                spriteBatch.Draw(
                    animations[currentAnimation].Texture,
                    Camera.WorldToScreen(drawRectangle),
                    animations[currentAnimation].FrameRectangle,
                    visibleColor, rotation, Vector2.Zero, effect, drawDepth);
            }

            if (visible == false)
            {
                visibleColor = new Color(0, 0, 0, 0);
            }
            else
            {
                visibleColor = new Color(myDict[paintColor].R, myDict[paintColor].G, myDict[paintColor].B, myDict[paintColor].A);
            }
        }
        #endregion
    
        public bool onGrounds() {
        bool ongrounds = false;
        if ( onGround == true){
        ongrounds = true;
        };
        if (!onGround){
        ongrounds = false;
        }

        return (ongrounds);
        }

        #region Constructor
        public GameObject()
        {
            myDict.Add(myColor.red, Color.Orange);
            myDict.Add(myColor.white, Color.White);
            myDict.Add(myColor.blue, Color.YellowGreen);            
        }
        #endregion

    }
}
