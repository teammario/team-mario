﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    public class Hammer : Enemy
    {
        private float fallspeed = 0.1f;
        private int jump = 200;
        private bool left = true;
        private int movement = 3;
        private float yvalue = -5;
        private float rotator = 0;
        private float bumperX = 0f;
        private float bumperY = 0f;
        private const int INTERVAL = 3;
        private int delay = 0;
        

        public override void Update(GameTime gameTime)
        {
            

            int thisFrameMove;
            if (left)
            {
                thisFrameMove = -1 * (movement);
            }
            else
            {
                thisFrameMove = movement;
            }

            WorldLocation = new Vector2(WorldLocation.X + thisFrameMove, WorldLocation.Y + yvalue);

            yvalue += fallspeed;

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            //rotator += 0.5f;
            bumperX = CollisionRectangle.Width;
            bumperY = CollisionRectangle.Height;

            int Xloc = (int)WorldLocation.X;
            int Yloc = (int)WorldLocation.Y;

            if (delay == INTERVAL)
            {
                rotator -= (float)(Math.PI * 90 / 180);
                delay = 0;
            }
            delay++;

            if(rotator <= (Math.PI * -360 / 180) || rotator >= (Math.PI * 360 / 180))
            {
                rotator = 0;
            }



            if (rotator == (float)(Math.PI * -90 / 180))
            {
                Yloc += (int)bumperY;
            }
            else if (rotator == (float)(Math.PI * -180 / 180))
            {
                Xloc += (int)bumperX;
                Yloc += (int)bumperY;
            }
            else if (rotator == (float)(Math.PI * -270 / 180))
            {
                Xloc += (int)bumperX;
            }

            //rotation = rotator;

            Rectangle drawRectangle = new Rectangle(Xloc , Yloc,
             animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
            spriteBatch.Draw(
                animations[currentAnimation].Texture,
                Camera.WorldToScreen(drawRectangle),
                animations[currentAnimation].FrameRectangle,
                Color.White, rotator, Vector2.Zero, SpriteEffects.None, 0.1f);


            //base.Draw(spriteBatch);
        }

        public Hammer(int x, int y, ContentManager content, Player gamePlayer) : base(gamePlayer, content, x, y)
        {
            topVulnerable = false;
            canBounce = false;
            movementSpeed = 0;
            enabled = true;
            WorldLocation = new Vector2(x, y);
            CollisionRectangle = new Rectangle(0, 0, 97, 97);
            
            animations.Add("Hammer",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                    "hammer",
                    1));

            //animations["pause"].NextAnimation = "jitter";
            animations["Hammer"].LoopAnimation = true;
            animations["Hammer"].FrameLength = 0.4f;

            PlayAnimation("Hammer");



        }
    }
}
