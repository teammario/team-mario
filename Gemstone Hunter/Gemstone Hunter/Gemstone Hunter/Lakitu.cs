﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class Lakitu:Enemy
    {
        //declare variable specific to lakitu
        float currentVelocity = 0;
        float throwTimer = 0;
        public List<SpinyEgg> Eggs = new List<SpinyEgg>();
        public int MaxEggs = 4;
        bool frameUpdated = false;
        int currentFrame = 0;
        bool readyToThrow = false;
        bool turnAround; //used to tell if we are changing directions

        public Lakitu(Player player, ContentManager content, int cellX, int cellY)
            : base(player,content, cellX, cellY)
        {
            //initialize global variables
            passable = true;
            this.player = player;
            worldLocation = new Vector2(0, 48);
            collisionRectangle = new Rectangle(0, 0, 48, 48);
            canRespawn = true;
            movementSpeed = 5;

            //add animations
            animations.Add("Throw", new AnimationStrip(EnemyTexture, EnemyPlist,
                "lakitu", 2));
            animations["Throw"].LoopAnimation = true;
            //setup how long until the next throw
            animations["Throw"].FrameLength = 8f;
            currentAnimation = "Throw";

            Enabled = true;
        }

        public void Throw(GameTime gameTime)
        {
            if (Enabled)
            {
                Random rand = new Random(gameTime.TotalGameTime.Milliseconds);

                animations["Throw"].FrameLength = (float)rand.Next(8, 15) + (float)rand.NextDouble();
                if (animations["Throw"].CurrentFrame != 1)
                {
                    worldLocation = new Vector2(worldLocation.X, worldLocation.Y - 24);
                }
                animations["Throw"].CurrentFrame = 1;

                Eggs.Add(new SpinyEgg(player,content, (int)worldLocation.X, (int)worldLocation.Y - 48));
                flipped = !flipped;
            }
        }

        public override void Update(GameTime gameTime)
        {
            //find the elapsed time value
            float elapsed = (float)gameTime.TotalGameTime.Milliseconds / 1000;

            //update our speed
                if (player.WorldLocation.X > WorldLocation.X)
                {
                    currentVelocity += elapsed/8;

                }
                else
                {
                    currentVelocity -= elapsed/8;

                }

            //cap the speed we can move
            if (currentVelocity >= 0)
            {
                currentVelocity = Math.Min(currentVelocity, movementSpeed);
            }
            else
            {
                currentVelocity = Math.Max(currentVelocity, -1*movementSpeed);
            }
            //update location

            worldLocation = new Vector2(worldLocation.X + currentVelocity*elapsed, worldLocation.Y);

            //update our timer
            throwTimer += elapsed/10;
            //check timer
            if (throwTimer >= animations["Throw"].FrameLength/2 && Eggs.Count < MaxEggs)
            {
                if (animations[currentAnimation].CurrentFrame != 0)
                {
                    worldLocation = new Vector2(worldLocation.X, worldLocation.Y + 24);
                }
                animations[currentAnimation].CurrentFrame = 0;

                readyToThrow = true;
            }
            if (throwTimer >= animations["Throw"].FrameLength && Eggs.Count < MaxEggs)
            {
                Throw(gameTime);
                throwTimer = 0;
            }
            //update our eggs
            for (int eggs = Eggs.Count-1; eggs >= 0; eggs--)
            {
                  Eggs[eggs].Update(gameTime);
                if (Eggs[eggs].dead == true)
                {
                    Eggs.RemoveAt(eggs);
                }
            }
            currentFrame = animations[currentAnimation].CurrentFrame;
            base.Update(gameTime);
            
            frameUpdated = (currentFrame != animations[currentAnimation].CurrentFrame);
            if (frameUpdated)
            {
                if (animations[currentAnimation].CurrentFrame == 1)
                {
                    worldLocation = new Vector2(worldLocation.X, worldLocation.Y-24);
                }
                else if (animations[currentAnimation].CurrentFrame == 0)
                {
                    worldLocation = new Vector2(worldLocation.X, worldLocation.Y+24);
                }
             }
             
            if (dead)
            {
                WorldLocation = new Vector2(player.WorldLocation.X + 20*48, 48);
                HP++;
                dead = false;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            foreach (SpinyEgg egg in Eggs)
            {
                egg.Draw(spriteBatch);
            }
            base.Draw(spriteBatch);
        }
    }
}
