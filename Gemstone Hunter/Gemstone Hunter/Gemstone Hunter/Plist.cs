﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Microsoft.Xna.Framework;
using System.Text.RegularExpressions;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class Plist
    {

        public Dictionary<string, Rectangle> Sprites = new Dictionary<string, Rectangle>();
        
        public Plist(string coordinatesFile)
        {
            XmlDocument doc = new XmlDocument();
            doc.XmlResolver = null;
            doc.Load(coordinatesFile);
            XmlNode metadata = doc.SelectSingleNode("/plist/dict/key[.='metadata']");
            XmlNode realTextureFileName =
            metadata.NextSibling.SelectSingleNode("key[.='realTextureFileName']");
            string spritesheetName = realTextureFileName.NextSibling.InnerText;
            XmlNode frames = doc.SelectSingleNode("/plist/dict/key[.='frames']");
            XmlNodeList list = frames.NextSibling.SelectNodes("key");

            foreach (XmlNode node in list)
            {
                XmlNode dict = node.NextSibling;
                string strRectangle = dict.SelectSingleNode
                ("key[.='frame']").NextSibling.InnerText;
                string strOffset = dict.SelectSingleNode
                ("key[.='offset']").NextSibling.InnerText;
                string strSourceRect = dict.SelectSingleNode
                ("key[.='sourceColorRect']").NextSibling.InnerText;
                string strSourceSize = dict.SelectSingleNode
                ("key[.='sourceSize']").NextSibling.InnerText;
                Rectangle frame = parseRectangle(strRectangle);
                string spriteFrameName = node.InnerText;
                
                this.Sprites.Add(spriteFrameName, frame);
            }
        }

        private static Rectangle parseRectangle(string rectangle)
        {
            Regex expression = new Regex(@"\{\{(\d+),(\d+)\},\{(\d+),(\d+)\}\}");
            Match match = expression.Match(rectangle);
            if (match.Success)
            {
                int x = int.Parse(match.Groups[1].Value);
                int y = int.Parse(match.Groups[2].Value);
                int w = int.Parse(match.Groups[3].Value);
                int h = int.Parse(match.Groups[4].Value);
                return new Rectangle(x, y, w, h);
            }
            return Rectangle.Empty;
        }

    }
}
