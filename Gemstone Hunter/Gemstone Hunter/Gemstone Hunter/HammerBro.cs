﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    public class HammerBro : Enemy
    {
        private ContentManager Content;
        //private bool HasJumped = false;
        private Random Rand = new Random();
        private const int INTERVAL = 500;
        private int count = 0;
        private const int OFFSET = 48;
        private bool jumpNow = false;
        private bool jumpPrev = false;
        private bool jumpPrev2 = false;
        private bool jumpPrev3 = false;
        private bool jumpPrev4 = false;
        private bool jumpPrev5 = false;
        private bool jumpPrev6 = false;
        private bool jumpPrev7 = false;
        private bool jumpPrev8 = false;
        private bool jumpPrev9 = false;
        private bool jumpPrev10 = false;
        private bool reached = true;
        private bool upward = true;
        private int fallspeed = 12;
        private bool goRight = true;
        private int maxLeft = 0;
        private int maxRight = 0;
        private int theHeight = 0;
        private int theBuffer = 0;
        public bool bumped = false;
        private bool throws = false;
        public bool threw = false;
        private int delay = 50;
        //private bool canJump = false;
        private Vector2 jumpLocation = new Vector2(0, 0);
        private Vector2 previousLocation = new Vector2(0, 0);
        private bool canJump = false;
        private int floating = 0;

        public override void Update(GameTime gameTime)
        {
            bool found = false;

            int going = Rand.Next(0, 100);

            int throwing = Rand.Next(0, 200);

            if (throwing == 1 && throws == false && WorldLocation.X < (Camera.Position.X + Camera.ViewPortWidth))
            {
                throws = true;
            }

            if (onGround)
            {
                if (throws == true)
                {
                    PlayAnimation("Throw");
                    throws = false;
                    //          threw = true;
                }
            }

            if (delay > 0)
            {
                delay--;
            }
            
            count++;

            if (count >= INTERVAL)
            {
                if (canJump == true)
                {
                    jumpNow = true;
                    onGround = false;
                    count = 0;
                }
                else
                {
                    jumpNow = false;
                    count = 0;
                }
            }

            if (jumpNow == false)
            {
                if (bumped == false) 
                {
                    velocity = new Vector2(velocity.X, velocity.Y + fallspeed);
                }
                if (going == 1)
                {
                    goRight = true;
                }
                else if (going == 2)
                {
                    goRight = false;
                }

                if (goRight == true && WorldLocation.X <= maxRight)
                {
                    velocity = new Vector2(24, velocity.Y);
                }
                else if (goRight == false && WorldLocation.X >= maxLeft)
                {
                    velocity = new Vector2(-24, velocity.Y);
                }
                else if (WorldLocation.X < maxLeft + 5)
                {
                    velocity = new Vector2(24, velocity.Y);
                    goRight = true;
                }
                else if (WorldLocation.X > maxRight - 5)
                {
                    velocity = new Vector2(-24, velocity.Y);
                    goRight = false;
                }
            }

            if (jumpNow == true)
            {
                if (upward == true && reached == false)
                {
                    if (WorldLocation.Y > jumpLocation.Y - 5)
                    {
                        WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y - 8);
                    }
                    else
                    {
                        reached = true;
                        upward = false;
                        jumpNow = false;
                    }
                }
                else if (upward == false && reached == false)
                {
                    
                    if ((jumpPrev == false || jumpPrev2 == false || jumpPrev3 == false || jumpPrev4 == false ||
                        jumpPrev5 == false || jumpPrev6 == false || jumpPrev7 == false || jumpPrev8 == false ||
                        jumpPrev9 == false || jumpPrev10 == false) && !(WorldLocation.Y > previousLocation.Y + (2 * 48)))
                    {
                        WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y - 4);
                    }
                    else
                    {
                        if (WorldLocation.Y < jumpLocation.Y + 8)
                        {
                            WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y + 8);
                        }
                        else
                        {
                            reached = true;
                            upward = true;
                            jumpNow = false;
                        }
                    }
                }
            }


            
            Vector2 Up = new Vector2(0, 0);
            Vector2 Down = new Vector2(0, 0);
            Vector2 tileCell1 = new Vector2(0, 0);
            Vector2 tileCell2 = new Vector2(0, 0);
            Vector2 tileCell3 = new Vector2(0, 0);
            
            if (reached == true)
            {
                if (upward == true)
                {
                    for (int x = 1; x < 8 && found == false; x++)
                    {
                        Up = new Vector2(WorldCenter.X, WorldCenter.Y - (48 * x));

                        tileCell1 = TileMap.GetCellByPixel(Up);

                        if (TileMap.CellIsBreakable(tileCell1))
                        {
                            found = true;
                            //upward = false;
                        }
                    }

                    tileCell2 = TileMap.GetCellByPixel(new Vector2(Up.X, Up.Y - OFFSET));
                    tileCell3 = TileMap.GetCellByPixel(new Vector2(Up.X, Up.Y - (2 * OFFSET)));
                }
                else
                {
                    for (int x = 1; x < 8 && found == false; x++)
                    {
                        Up = new Vector2(WorldCenter.X, WorldCenter.Y + (48 * x));

                        tileCell1 = TileMap.GetCellByPixel(Up);

                        if (TileMap.CellIsPassable(tileCell1) == false && TileMap.CellIsBreakable(tileCell1) == false)
                        {
                            found = true;
                            //upward = false;
                        }
                    }

                    tileCell2 = TileMap.GetCellByPixel(new Vector2(Up.X, Up.Y - OFFSET));
                    tileCell3 = TileMap.GetCellByPixel(new Vector2(Up.X, Up.Y - (2 * OFFSET)));
                }
            }


            if (found == true && reached == true)
            {
                if (TileMap.CellIsPassable(tileCell2) && TileMap.CellIsPassable(tileCell3))
                {
                    previousLocation = jumpLocation;
                    jumpLocation = new Vector2((int)tileCell3.X * 48, (int)tileCell3.Y * 48);
                    reached = false;
                }
            }
            
            jumpPrev10 = jumpPrev9;
            jumpPrev9 = jumpPrev8;
            jumpPrev8 = jumpPrev7;
            jumpPrev7 = jumpPrev6;
            jumpPrev6 = jumpPrev5;
            jumpPrev5 = jumpPrev4;
            jumpPrev4 = jumpPrev3;
            jumpPrev3 = jumpPrev2;
            jumpPrev2 = jumpPrev;
            jumpPrev = jumpNow;

            //wasFound = found;

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (animations[currentAnimation].FrameRectangle.Height > theHeight + 4 && bumped == false)
            {
                floating = (animations[currentAnimation].FrameRectangle.Height - theHeight);
                WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y - floating);
                bumped = true;
                delay = 50;
            }
            else if (animations[currentAnimation].FrameRectangle.Height <= theHeight + 2 && bumped == true)
            {

                WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y + floating);
                if (threw == false)
                {
                    threw = true;
                }

                bumped = false;
            }

            Rectangle drawRectangle = new Rectangle((int)worldLocation.X, (int)(worldLocation.Y),
             animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
            spriteBatch.Draw(
                animations[currentAnimation].Texture,
                Camera.WorldToScreen(drawRectangle),
                animations[currentAnimation].FrameRectangle,
                Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.1f);

        }


        public HammerBro(int x, int y, Player gamePlayer, ContentManager content)
            : base(gamePlayer, content, x, y)
        {
            topVulnerable = false;
            movementSpeed = 0;
            canBounce = true;
            enabled = true;
            passable = false;
            WorldLocation = new Vector2(x, y);
            CollisionRectangle = new Rectangle(0, 0, 48, (2 * 48));
            //HasJumped = false;
            upward = true;
            reached = true;
            maxLeft = (int)WorldLocation.X - 48;
            maxRight = (int)WorldLocation.X + 48;
            HP = 1;

            animations.Add("Default",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                    "hammer_bro",
                    2));

            animations.Add("Throw",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                    "hammer_bro",
                    3));

            theHeight = animations["Default"].FrameRectangle.Height;
            theBuffer = (2 * 48) - theHeight;

            //animations["pause"].NextAnimation = "jitter";
            animations["Default"].LoopAnimation = true;
            animations["Default"].FrameLength = 0.4f;

            animations["Throw"].NextAnimation = "Default";
            animations["Throw"].LoopAnimation = false;
            animations["Throw"].FrameLength = 0.4f;


            PlayAnimation("Default");

            Vector2 Up = new Vector2(0, 0);
            Vector2 Down = new Vector2(0, 0);
            Vector2 tileCell1 = new Vector2(0, 0);
            Vector2 tileCell2 = new Vector2(0, 0);
            Vector2 tileCell3 = new Vector2(0, 0);
            bool upFound = false;
            bool canUp = false;
            bool canDown = false;
            
                    for (int checkUp = 1; checkUp < 8 && upFound == false; checkUp++)
                    {
                        Up = new Vector2(WorldCenter.X, WorldCenter.Y - (48 * checkUp));

                        tileCell1 = TileMap.GetCellByPixel(Up);

                        if (TileMap.CellIsBreakable(tileCell1))
                        {
                            upFound = true;
                            //upward = false;
                        }
                    }

                    tileCell2 = TileMap.GetCellByPixel(new Vector2(Up.X, Up.Y - OFFSET));
                    tileCell3 = TileMap.GetCellByPixel(new Vector2(Up.X, Up.Y - (2 * OFFSET)));

                    if (upFound == true && TileMap.CellIsPassable(tileCell2) && TileMap.CellIsPassable(tileCell3))
                    {
                        canUp = true;
                    }


                bool downFound = false;

                    for (int checkDown = 1; checkDown < 8 && downFound == false; checkDown++)
                    {
                        Up = new Vector2(WorldCenter.X, WorldCenter.Y + (48 * checkDown));

                        tileCell1 = TileMap.GetCellByPixel(Up);

                        if (TileMap.CellIsPassable(tileCell1) == false && TileMap.CellIsBreakable(tileCell1) == false)
                        {
                            downFound = true;
                            //upward = false;
                        }
                    }

                    tileCell2 = TileMap.GetCellByPixel(new Vector2(Up.X, Up.Y - OFFSET));
                    tileCell3 = TileMap.GetCellByPixel(new Vector2(Up.X, Up.Y - (2 * OFFSET)));

                    if (downFound == true && TileMap.CellIsPassable(tileCell2) && TileMap.CellIsPassable(tileCell3))
                    {
                        canDown = true;
                    }

                    if (canUp == true && canDown == false)
                    {
                        upward = true;
                        reached = true;
                        canJump = true;
                    }
                    else if (canUp == true && canDown == true)
                    {
                        upward = false;
                        reached = false;
                        canJump = true;
                    }
                    else if (canUp = false && canDown == true)
                    {
                        upward = false;
                        reached = false;
                        canJump = true;
                    }
                    else
                    {
                        upward = false;
                        reached = true;
                        canJump = false;
                    }
            
            
        }
    }
}
