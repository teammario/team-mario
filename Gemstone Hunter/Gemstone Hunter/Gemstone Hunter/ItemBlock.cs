﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    public class ItemBlock : GameObject
    {
        public string Item;
 //       public Rectangle collisionRectangle;
        public const int sideLength = 48;
        private Player player;
 //       private Rectangle extendedPlayer;
 //       public Vector2 worldLocation;
 //       public Vector2 worldCenter;
        private string WarpInfo;
 //       private string currentAnimation;
        private int jump = 0;
        public bool isBroken = false;
        public ContentManager Content;
        private int count;
        private bool canCollide;
        private int delay;
        private bool jumpEnabled = true;
        public string Type = "Question";

 //       private Dictionary<string, AnimationStrip> animations =
 //           new Dictionary<string, AnimationStrip>();

        public int Jump
        {
            get { return jump; }
        }

        public Rectangle Extended()
        {
            Rectangle Extended = new Rectangle((int)WorldLocation.X, (int)WorldLocation.Y - 10, 48, 48);
            return Extended;
        }

        public void destroyBlock()
        {
            //insert change animation logic here
            if (Item != "TenCoin" || (Item == "TenCoin" && count < 1))
            {
 //               currentAnimation = "broken";
 //               animations["broken"].Play();
                PlayAnimation("broken");
                isBroken = true;

                if (jumpEnabled)
                {
                    jump = 10;
                }
                SoundEngine.Play("HitWall");
            }
            
            
            
            switch (Item)
            {
                case "Coin" :
                    //insert spawn logic
                    //temporary cheat
                    LevelManager.newFlowerCoin((int)WorldLocation.X, (int)WorldLocation.Y, Content, "Coin");
                    SoundEngine.Play("Coin");
                    break;
                case "TenCoin" :
                    //insert spawn logic
                    if (canCollide == true)
                    {
                        LevelManager.newFlowerCoin((int)WorldLocation.X, (int)WorldLocation.Y, Content, "Coin");
                        count -= 1;
                        jump = 10;
                        delay = 30;
                        canCollide = false;
                        SoundEngine.Play("Coin");
                    }
                    break;
                case "Flower" :
                    //insert spawn logic
                    if (player.isBig || player.isFire)
                    {
                        LevelManager.newFlowerCoin((int)WorldLocation.X, (int)WorldLocation.Y, Content, "Flower");
                    }
                    else
                    {
                        LevelManager.newMushroom((int)WorldLocation.X, (int)WorldLocation.Y - 48, Content, "Mushroom");
                    }
                    SoundEngine.Play("SpawnItem");
                    break;
                case "Mushroom" :
                    LevelManager.newMushroom((int)WorldLocation.X, (int)WorldLocation.Y - 48, Content, "Mushroom");
                    SoundEngine.Play("SpawnItem");
                    break;
                case "Star" :
                    LevelManager.newMushroom((int)WorldLocation.X, (int)WorldLocation.Y - 48, Content, "Star");
                    SoundEngine.Play("SpawnItem");
                    //insert spawn logic
                    break;
                case "OneUp" :
                    LevelManager.newMushroom((int)WorldLocation.X, (int)WorldLocation.Y - 48, Content, "OneUp");
                    SoundEngine.Play("SpawnItem");
                    //insert spawn logic
                    break;
                case "Vine" :
                    LevelManager.newVineTop((int)WorldLocation.X, (int)WorldLocation.Y - TileMap.TileHeight, WarpInfo);
                    break;
                default :
                    //insert spawn logic: coin
                    LevelManager.newFlowerCoin((int)WorldLocation.X, (int)WorldLocation.Y, Content, "Coin");
                    SoundEngine.Play("Coin");
                    break;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            int height = 0;

             if (animations.ContainsKey(currentAnimation))
            {
                if (jump > 0 && jumpEnabled == true)
                {
                    switch (jump)
                    {
                        case 1:
                            height = 1;
                            break;
                        case 2:
                            height = 2;
                            break;
                        case 3:
                            height = 3;
                            break;
                        case 4:
                            height = 4;
                            break;
                        case 5:
                            height = 5;
                            break;
                        case 6:
                            height = 6;
                            break;
                        case 7:
                            height = 8;
                            break;
                        case 8:
                            height = 6;
                            break;
                        case 9:
                            height = 4;
                            break;
                        case 10:
                            height = 2;
                            break;
                        default:
                            height = 0;
                            break;
                    }

                    jump -= 1;
                }

                if (jump == 0 && isBroken == true)
                {
                    jumpEnabled = false;
                }
                if (Type != "Transparent" || isBroken == true)
                {
                    Rectangle drawRectangle = new Rectangle((int)WorldLocation.X, (int)(WorldLocation.Y - height), animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
                    spriteBatch.Draw(
                        animations[currentAnimation].Texture,
                        Camera.WorldToScreen(drawRectangle),
                        animations[currentAnimation].FrameRectangle,
                        Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.85f);
                }
            }
             
        }

        public override void Update(GameTime gameTime)
        {
            if (jumpEnabled == false)
            {
                jump = 0;
            }

            if (Type == "Transparent" && isBroken == true)
            {
                passable = false;
                IsHorizontallyCollidable = true;
                IsVerticallyCollidable = true;
            }

            if (canCollide == false && delay > 0 && Item == "TenCoin")
            {
                delay -= 1;
            }
            else if (delay <= 0)
            {
                canCollide = true;
            }

            //extendedPlayer = new Rectangle((int)player.WorldLocation.X - 10, (int)player.WorldLocation.Y - 12, sideLength + 20, sideLength + 20);

            if (player.ExtendedRectangle.Intersects(CollisionRectangle) && player.Velocity.Y < 0)
            {
                if((player.WorldCenter.X > WorldLocation.X) && (player.WorldCenter.X < (WorldLocation.X + sideLength)))
                {
                    if (player.WorldLocation.Y > WorldCenter.Y + 12)
                    {
                        if (isBroken == false)
                        {
                            destroyBlock();
                            
                        }
                        else
                        {
//            updateAnimation(gameTime);
                            SoundEngine.Play("HitWall");
                        }
                        
                    }
                }
            }
//            updateAnimation(gameTime);

            base.Update(gameTime);
        }

        public ItemBlock(string item, int positionX, int positionY, string warpInfo, ContentManager content, Player gamePlayer, string type = "Question")
        {
            enabled = true;
            passable = false;
            Type = type;
            Content = content;

            if (Type == "Transparent")
            {
                passable = true;
                IsHorizontallyCollidable = false;
                IsVerticallyCollidable = false;
            }


            //set location variables
            WorldLocation = new Vector2(positionX, positionY);
            //worldCenter = new Vector2(positionX + (sideLength / 2), positionY + (sideLength / 2));

            //create rectangle
            CollisionRectangle = new Rectangle(positionX, positionY, sideLength, sideLength);

            if (item == "TenCoin")
            {
                canCollide = true;
                delay = 20;
                count = 10;
            }
            else
            {
                canCollide = false;
                delay = 0;
                count = 0;
            }

            //determine item
            if (item == "Flower" ||
                item == "Mushroom" ||
                item == "OneUp" ||
                item == "Star" ||
                item == "Coin" ||
                item == "TenCoin"||
                item == "Vine")
            {
                Item = item;
            }
            else if (item == "Empty")
            {
                Item = "Empty";
            }
            else //default to coin
            {
                Item = "Coin";
            }
            
            //determine if we passed valid warpInfo
            if (item == "Vine")
            {
                WarpInfo = warpInfo;
            }



              animations.Add("broken",
                 new AnimationStrip(
                     content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                     new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                     "block_empty",
                     1));

              switch (type)
              {
                  case "Question" :
                      animations.Add("default",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                            "question",
                            3));

                      break;
                  case "Red" :
                      animations.Add("default",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                            "bricks_red",
                            1));
                        
                      break;

                  case "Blue" :
                      animations.Add("default",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                            "bricks_blue",
                            1));
                        
                      break;

                  case "Grey" :
                      animations.Add("default",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                            "bricks_grey",
                            1));
                        
                      break;

                  case "Transparent" :
                      animations.Add("default",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                            "bricks_red",
                            1));
                        
                      break;

                  default :
                      animations.Add("default",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                            "bricks_red",
                            1));
                        
                      break;
              }

              animations["default"].LoopAnimation = true;
              animations["default"].FrameLength = 0.37f;
              

            PlayAnimation("default");

//animations["default"].Play();
              
            player = gamePlayer;

//            extendedPlayer = new Rectangle((int)player.WorldLocation.X - 10, (int)player.WorldLocation.Y - 10, sideLength + 20, sideLength + 20);

        }

        #region Helper Methods
//        private void updateAnimation(GameTime gameTime)
//        {
//            if (animations.ContainsKey(currentAnimation))
//            {
//                if (animations[currentAnimation].FinishedPlaying)
//                {
//                    currentAnimation = "default";
//                    animations["default"].Play();
//                }
//                else
//                {
//                    animations[currentAnimation].Update(gameTime);
//                }
//            }
//        }
        #endregion
    }
}
/*
 2:
                        animations.Add("default",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                            "bricks_red",
                            1));
                        break;
                    case 3:
                        animations.Add("default",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                            "bricks_red",
                            1));
                        break;
                    case 31:
                        animations.Add("default",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "bricks_blue",
                    1));
                        break;
                    case 32:
                        animations.Add("default",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                            "bricks_blue",
                            1));
                        break;
                    case 60:
                        animations.Add("default",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "bricks_grey",
                    1));
                        break;
                    case 61:
                        animations.Add("default",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "bricks_grey",
                    1));
*/