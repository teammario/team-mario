﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class BulletBill:Enemy
    {
        String direction;

        public BulletBill(Player player, ContentManager content, int cellX, int cellY, String Direction):base(player,content, cellX, cellY)
        {
            direction = Direction;
            topVulnerable = true;
            fireballVulnerable = false;
            movementSpeed = 150;
            fallSpeed = new Vector2(0, 0);
            isVerticallyCollidable = false;
            isHorizontallyCollidable = false;
            worldLocation = new Vector2(cellX, cellY);
            collisionRectangle = new Rectangle(0, 0, 48, 48);
            animations.Add("Shoot", new AnimationStrip(EnemyTexture,EnemyPlist,"bulletbill",1));
            animations["Shoot"].LoopAnimation = true;
            if (direction == "Left")
            {
                movementSpeed = -movementSpeed;
                flipped = false;
            }
            else
            {
                flipped = true;
            }

            currentAnimation = "Shoot";
            Enabled = true;
        }
        public override void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.TotalGameTime.Milliseconds / 1000;
            if (worldLocation.Y < 0)
            {
                worldLocation.Y = 1;
            }
            currentAnimation = "Shoot";
            velocity = new Vector2(movementSpeed, fallSpeed.Y);
            Vector2 moveAmount = velocity;
            if (player.CollisionRectangle.Intersects(this.CollisionRectangle))
            {
                player.Kill();
            }

            if (worldLocation.X <= 0 || worldLocation.X == TileMap.MapWidth - collisionRectangle.Width
                || worldLocation.Y <= TileMap.MapHeight - collisionRectangle.Height
                || worldLocation.Y >= (15 * 48)
                || player.CollisionRectangle.X - (48 * 12) >= this.WorldRectangle.X)
            {
                this.dead = true;
            }

            base.Update(gameTime);
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }
    }
}
