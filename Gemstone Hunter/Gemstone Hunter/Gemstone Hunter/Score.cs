﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class Score
    {
        public string score = "";
        public Vector2 worldLocation = Vector2.Zero;
        public Vector2 initialLocation = Vector2.Zero;
        SpriteFont pericles8;

        public void Update(GameTime gameTime)
        {
            worldLocation = new Vector2(worldLocation.X, worldLocation.Y - 1);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(pericles8, score, worldLocation, Color.White);
        }

        public Score(int x, int y, string scored, ContentManager Content)
        {
            pericles8 = Content.Load<SpriteFont>(@"Fonts\Pericles8");
            worldLocation = new Vector2(x, y);
            initialLocation = new Vector2(x, y);
            score = scored;
        }
    }
}
