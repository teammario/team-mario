﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class PulleyCorner : GameObject
    {
        public PulleyCorner(ContentManager content, int x, int y, bool flip)
        {
            worldLocation = new Vector2(x, y);

            if (flip == true)
            {
                flipped = true;
            }

            animations.Add("idle",
                   new AnimationStrip(
                       content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                       new Plist(@"content\Textures\Super_Mario_Sprites\Items.plist"),
                       "pulley_left",
                       1));
            animations["idle"].LoopAnimation = true;
            animations["idle"].FrameLength = 0.37f;
            currentAnimation = "idle";
            animations["idle"].NextAnimation = "idle";
            drawDepth = 0.875f;

            PlayAnimation("idle");
            collisionRectangle = new Rectangle(0, 0, animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
            enabled = true;
        }
    }
}
