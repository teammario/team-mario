﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class SpinyEgg:Enemy
    {
        bool hasHatched = false;

        public SpinyEgg(Player player, ContentManager content, int cellX, int cellY):base(player,content, cellX, cellY)
        {
            topVulnerable = true;
            movementSpeed = 25;
            worldLocation = new Vector2(cellX, cellY);
            collisionRectangle = new Rectangle(0, 0, 48, 48);
            animations.Add("Roll", new AnimationStrip(EnemyTexture,EnemyPlist,"spiny_egg",2));
            animations["Roll"].LoopAnimation = true;
            
            animations.Add("Walk", new AnimationStrip(EnemyTexture, EnemyPlist, "spiny", 2));
            animations["Walk"].LoopAnimation = true;
            animations["Walk"].FrameLength =.5f;

            currentAnimation = "Roll";
            Enabled = true;
            flipped = true;
        }
        public override void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.TotalGameTime.Milliseconds / 1000;
            if (worldLocation.Y < 0)
            {
                worldLocation.Y = 1;
            }
            Vector2 moveAmound = verticalCollisionTest(fallSpeed,false);
            if (!onGround)
            {
                worldLocation += fallSpeed;
            }

            if (onGround)
            {
                hasHatched = true;
            }

            if (hasHatched)
            {
                currentAnimation = "Walk";
                velocity = new Vector2(movementSpeed, fallSpeed.Y);
                Vector2 moveAmount = horizontalCollisionTest(velocity);
                if (moveAmount.X != movementSpeed)
                {
                    flipped = !flipped;
                    movementSpeed = -movementSpeed;
                }
            }

            base.Update(gameTime);
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }
    }
}
