﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Gemstone_Hunter
{
    public class pricessToad: GameObject
    {
        Player player;
        ContentManager Content;
        public String Message;
        public bool startTimer = false;
        private int conversationTimer = 0;
        private int maxConversationTimer = 5000;
        public pricessToad(ContentManager content,Player player, int x, int y, String type)
        {
            Enabled = true;
            Content = content;
            this.player = player;
            

            animations.Add("Toad",new AnimationStrip(Content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),"npc_toadstool",1));
            animations.Add("Princess",new AnimationStrip(Content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),"npc_princess",1));

            if (type.ToLower() == "Princess".ToLower())
            {
                worldLocation = new Vector2(x, y - (70 - 48));
                PlayAnimation("Princess");
                Message = "Thank you for saving me,\nbut I have a new mission for you!";
            }
            else
            {
                worldLocation = new Vector2(x, y - (72 - 48));
                PlayAnimation("Toad");
                Message = "Thank you for saving me, \nbut the princess is in another castle!";
            }

        }


        public override void Update(GameTime gameTime)
        {
            int elapsed = gameTime.ElapsedGameTime.Milliseconds;
            if (player.CollisionRectangle.Intersects(CollisionRectangle))
            {
                player.StopCinematic = true;
                player.PlayStanding(gameTime);
                player.WorldLocation = new Vector2(worldLocation.X - player.CollisionRectangle.Width,player.WorldLocation.Y);
                LevelManager.drawVictoryMessage = true;
                startTimer = true;
            }

            if (startTimer)
            {
                conversationTimer += elapsed;
            }
            if (conversationTimer >= maxConversationTimer)
            {
                LevelManager.currentLevel= 1;
                LevelManager.currentWorld++;
                LevelManager.currentWorld %= LevelManager.MaxWorld;
                player.canMove = true;
                player.StopCinematic = false;
                player.levelTimer = player.maxLevelTimer;
                LevelManager.LoadLevel(LevelManager.currentWorld, LevelManager.currentLevel);
            }

            base.Update(gameTime);
        }
    }
}
