﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class PulleyRope : GameObject
    {
        public PulleyRope(ContentManager content, int x, int y, bool RightAlign,bool horizontal)
        {
            worldLocation = new Vector2(x, y);

            if (RightAlign)
            {
                Flipped = true;
            }
            if (horizontal)
            {
                rotation = (float)(-90*Math.PI/180);
            }
            animations.Add("idle",
                   new AnimationStrip(
                       content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                       new Plist(@"content\Textures\Super_Mario_Sprites\Items.plist"),
                       "pulley_rope",
                       1));
            animations["idle"].LoopAnimation = true;
            animations["idle"].FrameLength = 0.37f;
            currentAnimation = "idle";
            animations["idle"].NextAnimation = "idle";
            drawDepth = 0.875f;

            PlayAnimation("idle");
            collisionRectangle = new Rectangle(0, 0, animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
            enabled = true;
        }
    }
}
