﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;


namespace Gemstone_Hunter
{
    class Bubble : GameObject
    {
        public float floatSpeed = -100f;
        public const float movementSpeed = 0f;
        private bool isPlayerFlipped = false;

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        public Bubble(int x, int y, ContentManager content, bool PlayerFlipped)
        {
            enabled = true;

            isVerticallyCollidable = false;
            isHorizontallyCollidable = false;

            isPlayerFlipped = PlayerFlipped;

                animations.Add("Bubble",
                    new AnimationStrip(
                        content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                        new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                        "bubble",
                        1));

                PlayAnimation("Bubble");

            worldLocation = new Vector2(x, y);
            velocity = new Vector2(movementSpeed, floatSpeed);
        }

        public override void Update(GameTime gameTime)
        {


            base.Update(gameTime);
        }

        public bool remove(int y)
        {
            if (WorldLocation.Y < 0)
            {
                LevelManager.removeBubble(y);

                return true;
            }
            return false;
        }
    }
}
