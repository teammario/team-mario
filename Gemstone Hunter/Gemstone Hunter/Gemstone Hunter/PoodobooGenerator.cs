﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class PoodobooGenerator:Enemy
    {
        public List<Poodoboo> PoodobooGen = new List<Poodoboo>();
        const float spawnTimer = .95f;
        Player player;
        ContentManager content;

        public PoodobooGenerator(Player player, ContentManager content, int cellX, int cellY)
            : base(player, content, cellX, cellY)
        {
            PoodobooGen.Clear();
            this.player = player;
            this.content = content;
            this.worldLocation = new Vector2(cellX, cellY);

            currentAnimation = "";
        }
        public void Jump(GameTime gameTime)
        {
            PoodobooGen.Add(new Poodoboo(player, content, (int)this.worldLocation.X, (int)this.worldLocation.Y));
        }
        public override void Update(GameTime gameTime)
        {
            float elapsedTimer = (float)gameTime.TotalGameTime.Milliseconds / 1000;
            
            if (spawnTimer <= elapsedTimer &&
                player.CollisionRectangle.X + (48 * 15) >= this.WorldRectangle.X &&
                PoodobooGen.Count < 1)
            {
                Jump(gameTime);
            }
            foreach (Poodoboo PG in PoodobooGen)
            {
                if (PG.dead)
                {
                    PoodobooGen.Remove(PG);
                    break;
                }
            }

            base.Update(gameTime);
        }
    }
}
