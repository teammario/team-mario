﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Gemstone_Hunter
{
    //class to handle objects warping the player from one location to another
    public class CastleDoor : GameObject
    {
        public string direction = "Right"; // can be Up or Right
        public string WarpInfo = "001_001_000_000";
        private int delayTimer = 0;
        private int castleDelayTime = 9000;
        public CastleFlag flag;
        private Vector2 flagStart;
        public bool fireworksFinished = false;
        public int numberFireworks = 7;
        public List<Firework> fireworks = new List<Firework>();
        private ContentManager content;

        #region Constructor
        public CastleDoor(ContentManager content, int cellX, int cellY, int width, int height, string warpInfo, string Direction,CastleFlag castleFlag)
        {
            this.content = content;
            worldLocation.X = cellX;
            worldLocation.Y = cellY;
            delayTimer = 0;
            flag = castleFlag;
            flagStart = flag.WorldLocation;

            WarpInfo = warpInfo;
            direction = Direction;
            CollisionRectangle = new Rectangle(0, 0, width, height);
            currentAnimation = "";
            enabled = true;
        }
        #endregion
        public void CountFireworks(Player player)
        {
            int onesposition = ((int)player.levelTimer / 1000)//full time
                                - ((int)(player.levelTimer / 100000) * 100)//hundreds position 
                                - ((((int)(player.levelTimer / 10000)) - ((int)(player.levelTimer / 100000) * 10)) * 10);//tens position;
            if(onesposition == 6)
            {
                numberFireworks = 6;
            }
            else if(onesposition == 3)
            {
                numberFireworks = 3;
            }
            else if(onesposition == 1)

            {
                numberFireworks = 1;
            }
            else
            {
                numberFireworks = 0;
            }
            for (int i = numberFireworks; i > 0; i--)
            {
                int flip = (i % 3 - 1);
                int flip2 = i %3 +i;

                fireworks.Add(new Firework(content, (int)(WorldLocation.X+(flip*i*Tile_Engine.TileMap.TileWidth)), (int)WorldLocation.Y - flip2 * Tile_Engine.TileMap.TileHeight));
            }
        }

        #region Helper Functions
        public void playFireworks(SpriteBatch spriteBatch, Player player)
        {
            if (numberFireworks > 0 && player.levelTimer <= 100)
            {
                if (!fireworks[numberFireworks-1].animationFinished)
                {
                    fireworks[numberFireworks-1].Draw(spriteBatch);
                }
                if (fireworks[numberFireworks-1].animationFinished)
                {
                    numberFireworks--;
                    Sound_Engine.SoundEngine.Play("Firework");
                    if (numberFireworks > 0)
                    {
                        fireworks[numberFireworks-1].Draw(spriteBatch);
                    }
                    else
                    {
                        fireworksFinished = true;
                    }
                }
            }
            else if(player.levelTimer <= 100)
            {
                fireworksFinished = true;
            }
        }
        public void playToWarpAnimation(Player player, GameTime gameTime)
        {
            int elapsed = gameTime.ElapsedGameTime.Milliseconds;

            if (delayTimer > castleDelayTime && player.levelTimer < 100 && fireworksFinished)
            {
                switch (direction)
                {
                    case "Right":
                        player.Enabled = true;
                        if (!player.StopCinematic)
                        {
                            player.PlayWalkRight(gameTime);
                        }
                        break;
                    case "Up":
                        string world = WarpInfo.Substring(0, 3);
                        string level = WarpInfo.Substring(4, 3);
                        string x = WarpInfo.Substring(8, 3);
                        string y = WarpInfo.Substring(12, 3);

                        LevelManager.LoadLevel(Convert.ToInt32(world), Convert.ToInt32(level));
                        player.WorldLocation = new Vector2(Convert.ToInt32(x) * Tile_Engine.TileMap.TileWidth, Convert.ToInt32(y) * Tile_Engine.TileMap.TileHeight);
                        Tile_Engine.Camera.Position = new Vector2(player.WorldLocation.X, Tile_Engine.Camera.Position.Y);
                        player.canMove = true;
                        player.IsHorizontallyCollidable = true;
                        player.IsVerticallyCollidable = true;
                        player.Enabled = true;
                        if(WarpInfo.EndsWith("R"))
                        {
                            player.levelTimer = player.maxLevelTimer;
                            player.countingTime = true;
                        }
                        break;
                }
            }
            else
            {
                delayTimer += elapsed;
                //update player
                if (player.levelTimer >= 0)
                {
                    player.levelTimer -= 1000;
                    player.Score += 50;
                }
                player.WorldLocation = WorldLocation;
                if (player.isBig || player.isFire)
                {
                    player.WorldLocation = new Vector2(player.WorldLocation.X, player.WorldLocation.Y - 48);
                }
                player.Enabled = false;

                //update castleFlag
                if (flagStart.Y - 91 < flag.WorldLocation.Y)
                {
                    flag.WorldLocation = new Vector2(flag.WorldLocation.X, flag.WorldLocation.Y - 1);
                }

            }
            
        }

        #endregion
    }
}
