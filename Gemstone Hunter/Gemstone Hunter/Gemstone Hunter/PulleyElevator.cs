﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class PulleyElevator : Elevators
    {
        public PulleyElevator(ContentManager content, int x, int y, String size, Player player)
            : base(content, x, y, "", size, player)
        {
            worldLocation = new Vector2(x, y);

            passable = false;
           
            collisionRectangle = new Rectangle(0, 0, animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
            enabled = true;
        }

        public override void Update(GameTime gameTime)
        {
            if (player.CollisionRectangle.Intersects(CollisionRectangle))
            {
                player.movingVertically = true;
                player.WorldLocation = new Vector2(player.WorldLocation.X, player.WorldLocation.Y + 1);
                player.onGrounds();
                WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y + 1);
            }
            base.Update(gameTime);
        }
    }
}
