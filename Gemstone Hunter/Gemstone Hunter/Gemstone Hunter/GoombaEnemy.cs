﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    class GoombaEnemy : Enemy
    {
        private string Type = "Red";
        private float moveAmount = 48;
        private bool right = true;
        private bool start = false;
        public int delay = 20;
        private float fallingSpeed = 5f;
        private bool squished = false;

        public override void Update(GameTime gameTime)
        {
            if (start == false)
            {
                if (WorldLocation.X < (Camera.Position.X + Camera.ViewPortWidth) && 
                    WorldLocation.X > Camera.Position.X)
                {
                    start = true;
                }
            }

            if (dead)
            {
                delay--;
                velocity = new Vector2(0, 0);
            }

            if (dead == false && start == true)
            {
                if (velocity.X > -30 && velocity.X < 30)
                {
                    if (right == true)
                    {
                        right = false;
                    }
                    else
                    {
                        right = true;
                    }
                }

                if (right == true)
                {
                    velocity = new Vector2(-1 * moveAmount, velocity.Y);
                }
                else
                {
                    velocity = new Vector2(moveAmount, velocity.Y);
                }
                if (dead == false)
                {
                    velocity = new Vector2(velocity.X, velocity.Y + fallingSpeed);
                }
            }
            else
            {
                velocity = new Vector2(0, 0);
            }

            base.Update(gameTime);
        }

        public void squish()
        {
            if (squished == false)
            {
                deadly = false;
                dead = true;
                squished = true;

                int theHeight = animations[currentAnimation].FrameRectangle.Height;

                if (Type == "Red")
                {
                    PlayAnimation("redSquish");
                }
                else
                {
                    PlayAnimation("blueSquish");
                }


                
                    WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y + (animations["red"].FrameRectangle.Height -
                        animations["redSquish"].FrameRectangle.Height));
                    dead = true;
                
            }
        }

        public GoombaEnemy(int x, int y, Player gamePlayer, ContentManager Content, string type = "Red") :
            base(gamePlayer, Content, x, y)
        {
            enabled = true;
            Type = type;
            topVulnerable = true;
            fireballVulnerable = true;
            canBounce = true;
            WorldLocation = new Vector2(x, y);
            CollisionRectangle = new Rectangle(0, 0, 48, 48);

            animations.Add("red",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                            "goomba_red",
                            2));

            animations.Add("blueSquish",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                            "goomba_blue_squish",
                            1));

            animations.Add("redSquish",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                            "goomba_red_squish",
                            1));

            animations.Add("blue",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                            "goomba_blue",
                            2));

            animations["blue"].FrameLength = 0.48f;
            animations["red"].FrameLength = 0.48f;
            animations["blue"].LoopAnimation = true;
            animations["red"].LoopAnimation = true;
            animations["blueSquish"].LoopAnimation = true;
            animations["redSquish"].LoopAnimation = true;

            if (Type == "Red")
            {
                PlayAnimation("red");
            }
            else
            {
                PlayAnimation("blue");
            }
        }
    }
}
