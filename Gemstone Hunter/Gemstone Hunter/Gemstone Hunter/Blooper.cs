﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class Blooper:Enemy
    {
        protected Vector2 verticalSpeed = new Vector2(0, 0);
        protected Vector2 currentPosition = new Vector2(0, 0);
        const int downwardMovementSpeed = 0;
        protected int upwardMovementSpeed;
        protected bool reachedTop = true;
        protected bool reachedBottom = true;

        public Blooper(Player player, ContentManager content, int cellX, int cellY):base(player,content, cellX, cellY)
        {
            topVulnerable = false;
            upwardMovementSpeed = 40;
            jumpHeight = 40;
            verticalSpeed.Y = -jumpHeight;
            fallSpeed = new Vector2(0, 30);
            isVerticallyCollidable = false;
            isHorizontallyCollidable = false;
            worldLocation = new Vector2(cellX, cellY);
            collisionRectangle = new Rectangle(0, 0, 48, 48);
            animations.Add("SwimUp", new AnimationStrip(EnemyTexture,EnemyPlist,"blooperUp",1));
            animations["SwimUp"].LoopAnimation = true;
            animations.Add("SwimDown", new AnimationStrip(EnemyTexture,EnemyPlist,"blooperDown", 1));
            animations["SwimDown"].LoopAnimation = true;

            currentAnimation = "SwimUp";
            Enabled = true;
        }
        public override void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.TotalGameTime.Milliseconds / 1000;
            if (worldLocation.Y < 0)
            {
                worldLocation.Y = 1;
            }

            // Determine Left/Right movement
            if (player.CollisionRectangle.X < this.CollisionRectangle.X && verticalSpeed.Y < 0)
            {
                movementSpeed = -upwardMovementSpeed;
            }
            else if (player.CollisionRectangle.X > this.CollisionRectangle.X && verticalSpeed.Y < 0)
            {
                movementSpeed = upwardMovementSpeed;
            }
            else
            {
                movementSpeed = downwardMovementSpeed;
            }

            // Determine Up/Down movement
            if ((player.CollisionRectangle.X != this.CollisionRectangle.X &&
                player.CollisionRectangle.Y > this.CollisionRectangle.Y) ||
                player.CollisionRectangle.Y > this.CollisionRectangle.Y)
            {
                if (reachedTop == true && reachedBottom == true)
                {
                    current();
                    reachedTop = false;
                    reachedBottom = false;
                }
                if (this.CollisionRectangle.Y > currentPosition.Y + (48 * 4) && reachedTop == false)
                {
                    verticalSpeed.Y = -jumpHeight;
                    if (this.CollisionRectangle.Y <= currentPosition.Y + (48 * 4))
                    {
                        reachedTop = true;
                        reachedBottom = false;
                    }
                }
                if (this.CollisionRectangle.Y < currentPosition.Y + (48 * 2) && reachedBottom == false)
                {
                    verticalSpeed.Y = fallSpeed.Y;
                    if (this.CollisionRectangle.Y >= currentPosition.Y + (48 * 2))
                    {
                        reachedTop = true;
                        reachedBottom = true;
                    }
                }
            }
            else if (player.CollisionRectangle.Y > this.CollisionRectangle.Y &&
                    player.CollisionRectangle.X == this.CollisionRectangle.X &&
                    player.CollisionRectangle.Y > this.CollisionRectangle.Y + 48)
            {
                verticalSpeed.Y = fallSpeed.Y;
            }
            else
            {
                if (reachedTop == true && reachedBottom == true)
                {
                    current();
                    reachedTop = false;
                    reachedBottom = false;
                }
                if (this.CollisionRectangle.Y > currentPosition.Y + (48 * 4) && reachedTop == false)
                {
                    verticalSpeed.Y = -jumpHeight;
                    if (this.CollisionRectangle.Y <= currentPosition.Y + (48 * 4))
                    {
                        reachedTop = true;
                        reachedBottom = false;
                    }
                }
                if (this.CollisionRectangle.Y < currentPosition.Y - (48 * 1) && reachedBottom == false)
                {
                    verticalSpeed.Y = fallSpeed.Y;
                    if (this.CollisionRectangle.Y >= currentPosition.Y - (48 * 1))
                    {
                        reachedTop = true;
                        reachedBottom = true;
                    }
                }
            }

            // Up/Down Animation
            if (verticalSpeed.Y > 0)
            {
                currentAnimation = "SwimDown";
            }
            else
            {
                currentAnimation = "SwimUp";
            }

            // AI Movement
            if (verticalSpeed.Y < 0)
            {
                velocity = new Vector2(movementSpeed, verticalSpeed.Y);
            }
            else
            {
                velocity = new Vector2(movementSpeed, verticalSpeed.Y);
            }
            
            if (player.CollisionRectangle.Intersects(this.CollisionRectangle))
            {
                player.Kill();
            }

            if (worldLocation.X <= 0 || worldLocation.X == TileMap.MapWidth - collisionRectangle.Width
                || worldLocation.Y <= TileMap.MapHeight - collisionRectangle.Height
                || worldLocation.Y >= (12 * 48)
                || player.CollisionRectangle.X - (48 * 12) >= this.WorldRectangle.X)
            {
                this.dead = true;
            }

            base.Update(gameTime);
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }
        public void current()
        {
            currentPosition.X = this.CollisionRectangle.X;
            currentPosition.Y = this.CollisionRectangle.Y;
        }
    }
}
