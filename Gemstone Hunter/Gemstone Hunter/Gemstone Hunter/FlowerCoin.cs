﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    class FlowerCoin : GameObject
    {
        private int growth;
        public string Type;
        private bool visible;
        private int max = 48;
        private int jumping = 25;
        private Player player;
        private ContentManager Content;

        public override void Draw(SpriteBatch spriteBatch)
        {
            int depth = 0;

            if (growth <= max)
            {
                depth = (48 - growth);
                if (Type == "Flower")
                    growth++;
                else
                    growth += jumping;
                jumping-= 2;
            }
            else
            {
                depth = 0;
            }


            Rectangle drawRectangle = new Rectangle((int)worldLocation.X, (int)(worldLocation.Y + depth),
             animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
            spriteBatch.Draw(
                animations[currentAnimation].Texture,
                Camera.WorldToScreen(drawRectangle),
                animations[currentAnimation].FrameRectangle,
                Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
            //base.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public bool remove(int x)
        {
            if ((growth >= max || jumping <= -14) && Type == "Coin")
            {
                LevelManager.scores.Add(new Score((int)(WorldCenter.X - Camera.Position.X),
                                (int)(WorldCenter.Y - Camera.Position.Y), "100", Content));
                player.Coins++;
                player.Score += 100;
                LevelManager.removeCoin(x);
                return true;
            }
            return false;
            
        }

        //possible type arguements: Flower, Coin
        public FlowerCoin(int x, int y, string type, ContentManager content, Player gamePlayer)
        {
            Type = type;
            enabled = true;
            growth = 0;
            visible = true;
            Content = content;
            player = gamePlayer;   
            worldLocation = new Vector2(x, y - 48);
            collisionRectangle = new Rectangle(0, 0, 48, 48);

            if (Type == "Flower")
            {
                max = 48;
                //insert logic stb_flower stb_coin
                animations.Add("Flower",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "flower",
                    4));

                animations["Flower"].FrameLength = 0.48f;
                animations["Flower"].LoopAnimation = true;
                PlayAnimation("Flower");
            
            }
            else
            {
                max = (5 * 48);
                //insert logic
                animations.Add("Coin",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "coin_col",
                    3));

                worldLocation = new Vector2(x + 10, y - 48);
                
                PlayAnimation("Coin");
            
            }

        }
    }
}
/*
 public int growth;
        private float fallspeed = 12f;
        public bool goRight;
        public string Type;

        public override void Draw(SpriteBatch spriteBatch)
        {

            int depth = 0;

            if (growth <= 48)
            {
                depth = (48 - growth);
                growth++;
            }
            else
            {
                depth = 0;
            }

            
            Rectangle drawRectangle = new Rectangle((int)worldLocation.X, (int)(worldLocation.Y + depth), 
             animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
                spriteBatch.Draw(
                    animations[currentAnimation].Texture,
                    Camera.WorldToScreen(drawRectangle),
                    animations[currentAnimation].FrameRectangle,
                    Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
            /
            /    spriteBatch.Draw(
            /        animations[currentAnimation].Texture,
            /        Camera.WorldToScreen(CollisionRectangle),
            /        animations[currentAnimation].FrameRectangle,
            /        Color.Black, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
            
            
            //base.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            //velocity = new Vector2(0, velocity.Y);

            if (growth >= 48)
            {
                if (velocity.X < 100 && velocity.X > -100)
                {
                    goRight = !goRight;
                }
                else if(worldLocation.X < 26)
                {
                    goRight = true;
                }

                if (goRight)
                    velocity = new Vector2(150, velocity.Y + fallspeed);
                else
                    velocity = new Vector2(-150, velocity.Y + fallspeed);
            }

            base.Update(gameTime);
        }

        //possible type arguments: Mushroom, OneUp
        public Mushroom(int x, int y, ContentManager content, string type)
        {
            //worldLocation = new Vector2(x, y);
            enabled = true;
            growth = 0;

            Type = type;

            if (type == "OneUp")
            {
                //animation logic
                animations.Add("OneUp",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "stb_1upmushroom",
                    1));

                PlayAnimation("OneUp");
               

                //animations["OneUp"].Play();
            }
            else
            {
                //animation logic
                animations.Add("Mushroom",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "stb_mushroom",
                    1));

                PlayAnimation("Mushroom");
               // animations["Mushroom"].Play();
            }

            CollisionRectangle = new Rectangle(0, 0, 48, 48);
            worldLocation = new Vector2(x, y);

        }
    }
}

*/