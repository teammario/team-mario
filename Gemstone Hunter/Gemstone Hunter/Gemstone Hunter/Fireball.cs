﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class Fireball : GameObject
    {
        private float fallSpeed = 20f;
        private float movementSpeed = 400f;
        private bool hitGround = false;
        private bool isPlayerFlipped = false;
        private bool isExploding = false;

        private static float fireballExplosionTimer = 0f;
        private static float fireballExplosionDelay = 250f;

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        public Fireball(int x, int y, ContentManager content, bool PlayerFlipped)
        {
            enabled = true;
            passable = false;

            fireballExplosionTimer = 0;

            isPlayerFlipped = PlayerFlipped;

                animations.Add("Fireball",
                    new AnimationStrip(
                        content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                        new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                        "fireball_generic",
                        1));

                PlayAnimation("Fireball");

                animations.Add("FireballCollide",
                    new AnimationStrip(
                        content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                        new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                        "firework",
                        1));

            CollisionRectangle = new Rectangle(0, 0, 25, 25);
            worldLocation = new Vector2(x, y);
            if (isPlayerFlipped == false)
            {
                velocity = new Vector2(movementSpeed, fallSpeed);
            }
            else
            {
                velocity = new Vector2(-movementSpeed, fallSpeed);
            }
        }

        public override void Update(GameTime gameTime)
        {
            // Flip fireball if player is flipped
            if (isPlayerFlipped == true)
            {
                flipped = true;
            }
            else
            {
                flipped = false;
            }

            // Spin fireball
            if (gameTime.TotalGameTime.Milliseconds % 50 == 0 &&
                (velocity.X > 50 || velocity.X < -50))
            {
                if (rotation < 1f)
                {
                    rotation += .25f;
                }
                else
                {
                    rotation = 0f;
                }
            }

            // Bounce
            if (isExploding == false)
            {
                if (!hitGround)
                {
                    velocity.Y = 150;
                }
                else
                {
                    if (onGround)
                    {
                        velocity.Y = -350;
                    }
                    velocity.Y += fallSpeed;
                }

                if (onGround)
                {
                    hitGround = true;
                }
            }

            base.Update(gameTime);
        }

        public bool remove(int x, float elapsed)
        {
            if ((velocity.X > -50 && velocity.X < 50) || WorldLocation.X <= 0)
            {
                removeCollision(x, elapsed);

                return true;
            }
            return false;
        }
        public void removeCollision(int x, float elapsed)
        {
            PlayAnimation("FireballCollide");
            isExploding = true;
            velocity = new Vector2(0, 0);

            if (fireballExplosionTimer >= fireballExplosionDelay)
            {
                LevelManager.removeFireball(x);
            }

            fireballExplosionTimer += elapsed;
        }
    }
}
