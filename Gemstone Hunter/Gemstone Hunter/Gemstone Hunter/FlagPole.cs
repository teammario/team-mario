﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class FlagPole : GameObject
    {
        public Flag flag;
        bool hasPlayed = false;
        public bool hasSlid = false;
        public bool playerRelocated = false;
        public int slideScore = 0;
        public FlagPole(ContentManager content, int x, int y, int width, int height)
        {
            flag = new Flag(content, x - TileMap.TileWidth/2, y+ TileMap.TileHeight);
            collisionRectangle = new Rectangle(0, 0, width, height);
            WorldLocation = new Vector2(x, y);
            
            playerRelocated = false;
            hasSlid = false;
            hasPlayed = false;
            currentAnimation = "";
        }

        public void SlideAnimation(Player player)
        {
            if (!playerRelocated)
            {
                player.countingTime = false;
                Sound_Engine.SoundEngine.Stop(TileMap.BackgroundMusic);
                //adjust player location
                player.canMove = false;
                player.isClimbing = true;
                player.Flipped = false;
                player.WorldLocation = new Vector2((WorldLocation.X-10), player.WorldLocation.Y);
                playerRelocated = true;
                player.PlayClimb();
            }
            //slide player
            if (TileMap.CellIsPassable(TileMap.GetCellByPixelX((int)player.WorldLocation.X+player.CollisionRectangle.Width), TileMap.GetCellByPixelY((int)player.WorldLocation.Y + player.CollisionRectangle.Height + 1)))
            {
                player.WorldLocation = new Vector2(player.WorldLocation.X, player.WorldLocation.Y + 2);
                if (slideScore < 5000)
                {
                    player.Score += 25;
                    slideScore += 25;
                }
            }
            //slide flag
           if (TileMap.CellIsPassable(TileMap.GetCellByPixelX((int)flag.WorldLocation.X+48), TileMap.GetCellByPixelY((int)flag.WorldLocation.Y + TileMap.TileHeight + 1)))
           {                
                if (flag.WorldLocation.Y % TileMap.TileHeight == 0)
                {
                    if (!hasPlayed)
                    {
                        Sound_Engine.SoundEngine.Play("FlagPole");
                        hasPlayed = true;
                    }
                }

                flag.WorldLocation = new Vector2(flag.WorldLocation.X, flag.WorldLocation.Y + 2);
             
            }
            else
            {
                hasSlid = true;
                Sound_Engine.SoundEngine.Play("VictoryFanfare");
            }
        }
    }
}
