﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class VineTop:GameObject
    {
        bool hasPlayed = false;
        public bool hasGrown = false;
        private string WarpInfo = "001_001_000_000";

        public VineTop(ContentManager content, int x, int y, string warpInfo)
        {
            hasPlayed = false;
            WorldLocation = new Vector2(x, y);
            WarpInfo = warpInfo;

            animations.Add("vine_double",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "vine_double",
                    1));

            animations["vine_double"].NextAnimation = "vine_double";
            animations["vine_double"].LoopAnimation = true;
            drawDepth = .90f;

            PlayAnimation("vine_double");
            enabled = true;
        }

        public void Grow()
        {
            if (WorldLocation.Y > -TileMap.TileHeight)
            {
                if (WorldLocation.Y % TileMap.TileHeight == 0)
                {
                    if (!hasPlayed)
                    {
                        Sound_Engine.SoundEngine.Play("Vine");
                        hasPlayed = true;
                    }
                }

                WorldLocation = new Vector2(WorldLocation.X,WorldLocation.Y -1);

                if (WorldLocation.Y % TileMap.TileHeight == 0)
                {
                    LevelManager.newVine((int)WorldLocation.X+4, (int)WorldLocation.Y + TileMap.TileHeight, WarpInfo);
                }

                
            }
            else
            {
                hasGrown = true;
            }
            
        }
    }
}
