﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;

namespace Gemstone_Hunter
{
    class CloudElevator : GameObject
    {
        #region Declarations
        public string directions = " ";
        public string type = " ";
        public float didtime = 0f; //used to config elapsed gametime
        public bool spawnChildElevator = true;
        public bool is_on = true;
        public bool is_off = false;
        public int flipperTimer = 0;
                    
        #endregion
      /*  public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }*/
        public CloudElevator(ContentManager content, int x, int y, int width, int height, string direction)
        {//should get apprpriate texture to turn to sprite
            Passable = false;
            worldLocation.X = x;
            worldLocation.Y = y;
            directions = direction;//storews direction
            
            
            animations.Add("idle",
                   new AnimationStrip(
                       content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                       new Plist(@"content\Textures\Super_Mario_Sprites\Items.plist"),
                       "platform_pearl",
                       1));

            animations["idle"].LoopAnimation = true;
            animations["idle"].FrameLength = 0.37f;
            currentAnimation = "idle";
            animations["idle"].NextAnimation = "idle";
            drawDepth = 0.875f;

            PlayAnimation("idle");
            collisionRectangle = new Rectangle(0, 0, animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
            enabled = true;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }







    }
}
