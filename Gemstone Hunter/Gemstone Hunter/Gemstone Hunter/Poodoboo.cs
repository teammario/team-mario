﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class Poodoboo:Enemy
    {
        protected Vector2 verticalSpeed = new Vector2(0, 0);

        public Poodoboo(Player player, ContentManager content, int cellX, int cellY):base(player,content, cellX, cellY)
        {
            topVulnerable = false;
            fireballVulnerable = false;
            movementSpeed = 0;
            jumpHeight = 630;
            verticalSpeed.Y = -jumpHeight;
            fallSpeed = new Vector2(0, 10);
            worldLocation = new Vector2(cellX, cellY);
            collisionRectangle = new Rectangle(0, 0, 48, 48);
            animations.Add("Jump", new AnimationStrip(EnemyTexture,EnemyPlist,"poodobooUp",1));
            animations["Jump"].LoopAnimation = true;
            animations.Add("Fall", new AnimationStrip(EnemyTexture, EnemyPlist, "poodobooDown", 1));
            animations["Fall"].LoopAnimation = true;

            currentAnimation = "Jump";
            Enabled = true;
        }
        public override void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.TotalGameTime.Milliseconds / 1000;
            if (worldLocation.Y < 0)
            {
                worldLocation.Y = 1;
            }
            currentAnimation = "Jump";
            velocity = new Vector2(movementSpeed, verticalSpeed.Y += fallSpeed.Y);

            if (verticalSpeed.Y >= 0)
            {
                currentAnimation = "Fall";
            }
            
            if (worldLocation.X <= 0 || worldLocation.X == TileMap.MapWidth - collisionRectangle.Width
                || worldLocation.Y <= TileMap.MapHeight - collisionRectangle.Height
                || worldLocation.Y >= (14 * 48)-24)
            {
                this.dead = true;
            }

            base.Update(gameTime);
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }
    }
}
