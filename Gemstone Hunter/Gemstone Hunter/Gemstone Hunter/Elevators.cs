﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class Elevators : GameObject

    {
        #region Declarations
        public string directions = " ";
        private string size;
        public string type = " ";
        public float didtime = 0f; //used to config elapsed gametime
        public bool spawnChildElevator = true;
        public bool is_on = true;
        public bool is_off = false;
        public int flipperTimer = 15000;
        static bool flipper = true;
        public Player player;
        private ContentManager Content;

        public static int n = 0;//used for controling distance traveled
        public static int u = 0;

        #endregion
      /*  public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }*/
        public Elevators(ContentManager content, int x, int y, string direction, String size, Player pr) {//should get apprpriate 
            // texture to turn to sprite
            this.size = size;
            Content = content;
            player = pr;
            Passable = false;
            worldLocation.X = x;
            worldLocation.Y = y;
            directions = direction;//storews direction
            
            if (size == "Small")
            {
                animations.Add("idle",
                       new AnimationStrip(
                           content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                           new Plist(@"content\Textures\Super_Mario_Sprites\Items.plist"),
                           "platform_metal_small",
                           1));
            }
            else
            {
                animations.Add("idle",
                       new AnimationStrip(
                           content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                           new Plist(@"content\Textures\Super_Mario_Sprites\Items.plist"),
                           "platform_metal_large",
                           1));
            }
            animations["idle"].LoopAnimation = true;
            animations["idle"].FrameLength = 0.37f;
            currentAnimation = "idle";
            animations["idle"].NextAnimation = "idle";
            drawDepth = 0.875f;
            
            PlayAnimation("idle");
            collisionRectangle = new Rectangle(0, 0, animations[currentAnimation].FrameRectangle.Width,animations[currentAnimation].FrameRectangle.Height);
            enabled = true;
        }
        
        public override void Update(GameTime gameTime)
        {
            if (flipperTimer == 0)
            {
                flipperTimer = n;
            }
            flipperTimer -= gameTime.ElapsedGameTime.Milliseconds;
            base.Update(gameTime);

            #region Right movement
            if (directions == "Right")
            {
                if (player.CollisionRectangle.Intersects(CollisionRectangle) && player.onGrounds() && (is_on == true))
                {
                    player.WorldLocation = new Vector2(player.WorldLocation.X + 1, player.WorldLocation.Y);

                    WorldLocation = new Vector2(WorldLocation.X + 1, WorldLocation.Y);
                    is_on = false;
                }
                if (is_on == false)
                {
                    WorldLocation = new Vector2(WorldLocation.X + 1, WorldLocation.Y);
                    if (player.CollisionRectangle.Intersects(CollisionRectangle) && player.onGrounds())
                    {
                        player.movingVertically = false;
                        player.WorldLocation = new Vector2(player.WorldLocation.X + 1, player.WorldLocation.Y);
                    }


                }


            }
            #endregion

            #region Horizontal Bounce
            if (directions == "HBounce")
            {

                if (u % 500 == 0 && u % 1000 == 0)
                {

                    flipper = false;
                }
                else if ((u % 500 == 0) && (u % 1000 != 0))
                {
                    flipper = true;

                }

                if (flipper == true)
                {
                    WorldLocation = new Vector2(WorldLocation.X + 1, WorldLocation.Y);

                    if (player.CollisionRectangle.Intersects(CollisionRectangle) && player.onGrounds())
                    {
                        player.WorldLocation = new Vector2(player.WorldLocation.X + 1, player.WorldLocation.Y);
                    }
                }

                if (flipper == false)
                {
                    WorldLocation = new Vector2(WorldLocation.X - 1, WorldLocation.Y);


                    if (player.CollisionRectangle.Intersects(CollisionRectangle) && player.onGrounds())
                    {
                        player.movingVertically = false;
                        player.WorldLocation = new Vector2(player.WorldLocation.X - 1, player.WorldLocation.Y);
                    }
                }
                n += gameTime.ElapsedGameTime.Milliseconds;
                if (n / 1000 > 0)
                {
                    u++;
                }

            }
            #endregion

            #region Vertical Bounce
            if (directions == "VBounce")
            {

                if (u % 500 == 0 && u % 1000 == 0)
                {

                    flipper = false;
                }
                else if ((u % 500 == 0) && (u % 1000 != 0))
                {
                    flipper = true;

                }

                if (flipper == true)
                {
                    WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y + 1);
                    player.movingVertically = true;
                    if (player.CollisionRectangle.Intersects(CollisionRectangle) && player.onGrounds())
                    {
                        player.WorldLocation = new Vector2(player.WorldLocation.X, player.WorldLocation.Y+1);
                    }
                }

                if (flipper == false)
                {
                    WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y - 1);


                    if (player.CollisionRectangle.Intersects(CollisionRectangle) && player.onGrounds())
                    {
                        player.movingVertically = true;
                        player.WorldLocation = new Vector2(player.WorldLocation.X, player.WorldLocation.Y -1);
                    }
                }
                n += gameTime.ElapsedGameTime.Milliseconds;
                if (n / 1000 > 0)
                {
                    u++;
                }

            }
            #endregion

            #region Up scrolling
            if (directions == "Up")
            {
                WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y - 1);
                if (WorldLocation.Y <= ((Camera.ViewPortHeight * 3) / 8) && spawnChildElevator)
                {

                    LevelManager.spawnElevator(Content, (int)WorldLocation.X, (int)Camera.ViewPortHeight,
                        directions, size);
                    spawnChildElevator = false;

                }
                if (player.CollisionRectangle.Intersects(CollisionRectangle) && player.onGrounds())
                {
                    player.movingVertically = false;
                    player.WorldLocation = new Vector2(player.WorldLocation.X, player.WorldLocation.Y - 1);
                }
                if (WorldLocation.Y == (Camera.ViewPortHeight % Camera.ViewPortHeight))
                {
                    LevelManager.removeElevator(this);
                }

            }
            #endregion 

            #region Down Scrolling
            if (directions == "Down")
            {
                WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y + 1);
                if (WorldLocation.Y >= ((Camera.ViewPortHeight) / 2) && spawnChildElevator)
                {
                    LevelManager.spawnElevator(Content, (int)WorldLocation.X, 0, directions,size);
                    spawnChildElevator = false;
                }
                if (player.CollisionRectangle.Intersects(CollisionRectangle))
                {
                    player.movingVertically = true;
                    player.WorldLocation = new Vector2(player.WorldLocation.X,player.WorldLocation.Y+1);
                }
                //else
                //{
                //    player.movingVertically = false;
                //}
                if (WorldLocation.Y == (Camera.ViewPortHeight - CollisionRectangle.Height))
                {
                    LevelManager.removeElevator(this);

                }

            }
            #endregion
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }


    }
}
