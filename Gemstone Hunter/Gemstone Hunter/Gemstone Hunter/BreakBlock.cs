﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    public class BreakBlock : GameObject
    {
        public bool CanBreak;
        public int type;
        public int delay;
        public int pieceNumber = -1;
        private float fallspeed = 1;
        public Rectangle Extended;
        public bool isBouncable = true;

        public override void Update(GameTime gameTime)
        {
            if (delay <= 8)
            {
                isBouncable = false;
            }

            if (pieceNumber > -1)
            {
                if(fallspeed < 25)
                fallspeed += 0.5f;

                switch (pieceNumber)
                {
                    case 1:
                        WorldLocation = new Vector2(WorldLocation.X - 6, WorldLocation.Y - 10 + fallspeed);
                        break;
                    case 2:
                        WorldLocation = new Vector2(WorldLocation.X + 6, WorldLocation.Y - 10 + fallspeed);
                        break;
                    case 3:
                        WorldLocation = new Vector2(WorldLocation.X - 5, WorldLocation.Y - 5 + fallspeed);
                        break;
                    case 4:
                        WorldLocation = new Vector2(WorldLocation.X + 5, WorldLocation.Y - 5 + fallspeed);
                        break;
                    default:
                        break;
                }

            }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            int height = 0;

            if (pieceNumber == -1)
            {
                switch (delay)
                {
                    case 10:
                        height = 1;
                        break;
                    case 9:
                        height = 2;
                        break;
                    case 8:
                        height = 3;
                        break;
                    case 7:
                        height = 4;
                        break;
                    case 6:
                        height = 5;
                        break;
                    case 5:
                        height = 4;
                        break;
                    case 4:
                        height = 3;
                        break;
                    case 3:
                        height = 2;
                        break;
                    case 2:
                        height = 1;
                        break;
                    case 1:
                        height = 0;
                        break;
                    default:
                        height = 0;
                        break;
                }
            }

            delay -= 1;

            Rectangle drawRectangle = new Rectangle((int)WorldLocation.X, (int)(WorldLocation.Y - height), animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
            spriteBatch.Draw(
                animations[currentAnimation].Texture,
                Camera.WorldToScreen(drawRectangle),
                animations[currentAnimation].FrameRectangle,
                Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.85f);
            //base.Draw(spriteBatch);
        }

        public BreakBlock(int x, int y, int tileNo, ContentManager content, bool Break, int Number = -1)
        {
            enabled = true;
            IsVerticallyCollidable = false;
            IsHorizontallyCollidable = false;
            passable = true;
            isBouncable = true;


            pieceNumber = Number;

            type = tileNo;

            WorldLocation = new Vector2(x, y);

            CanBreak = Break;

            CollisionRectangle = new Rectangle(0, 0, 48, 48);
            if ((Break == true && pieceNumber == 1) || Break == false)
            {
                Extended = new Rectangle((int)WorldLocation.X, (int)WorldLocation.Y - 12, 48, 48);
            }
            else
            {
                Extended = new Rectangle(0, 0, 0, 0);
            }
            if (CanBreak == false)
            {
                CollisionRectangle = new Rectangle(0, 0, 48, 48);

                switch (tileNo)
                {
                    case 2:
                        animations.Add("default",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                            "bricks_red",
                            1));
                        break;
                    case 3:
                        animations.Add("default",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                            "bricks_red",
                            1));
                        break;
                    case 31:
                        animations.Add("default",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "bricks_blue",
                    1));
                        break;
                    case 32:
                        animations.Add("default",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                            "bricks_blue",
                            1));
                        break;
                    case 60:
                        animations.Add("default",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "bricks_grey",
                    1));
                        break;
                    case 61:
                        animations.Add("default",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "bricks_grey",
                    1));
                        break;
                    default:
                        animations.Add("default",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "bricks_grey",
                    1));
                        break;
                }
                //29
                //32
                //61

                //animations["default"].LoopAnimation = true;
                //animations["default"].FrameLength = 0.37f;

            }
            else
            {
                CollisionRectangle = new Rectangle(0, 0, 48, 48);


                switch (tileNo)
                {
                    case 2:
                        animations.Add("default",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                            "break_red",
                            1));
                        break;
                    case 3:
                        animations.Add("default",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                            "break_red",
                            1));
                        break;
                    case 31:
                        animations.Add("default",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "break_green",
                    1));
                        break;
                    case 32:
                        animations.Add("default",
                        new AnimationStrip(
                            content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                            new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                            "break_green",
                            1));
                        break;
                    case 61:
                        animations.Add("default",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "break_grey",
                    1));
                        break;
                    case 62:
                        animations.Add("default",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "break_grey",
                    1));
                        break;
                    default:
                        animations.Add("default",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "break_grey",
                    1));
                        break;
                }
                
            }

            PlayAnimation("default");
            delay = 10;
        }
    }
}
