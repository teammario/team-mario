﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    public class Bridge : GameObject
    {
        //public List<BridgePiece> bridgePieces = new List<BridgePiece>();

        public int identifier;

        private int Length;
        private int intact;
        public bool isDestroyed = false;
        private const int delay = 12;
        private int delayed = 0;

        //private Player player;

//        public Vector2 worldLocation;
//        public Vector2 worldCenter;
//        public Rectangle collisionRectangle;
//        private string currentAnimation;

//        private Dictionary<string, AnimationStrip> animations =
//            new Dictionary<string, AnimationStrip>();


        public override void Update(GameTime gameTime)
        {
            if (isDestroyed)
            {
                if (intact > 0)
                {
                    if (delayed == 0)
                    {
                        intact -= 1;
                        delayed = delay;
                    }
                    else
                    {
                        delayed -= 1;
                    }
                }
            }
        }

        public void bridgeBroken()
        {
            isDestroyed = true;
            IsHorizontallyCollidable = false;
            IsVerticallyCollidable = false;
            passable = true;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {

            for (int x = 0; x < intact; x++)
            {
                Rectangle drawRectangle = new Rectangle((int)WorldLocation.X + (48 * x), (int)WorldLocation.Y, 
                    animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
                spriteBatch.Draw(
                    animations[currentAnimation].Texture,
                    Camera.WorldToScreen(drawRectangle),
                    animations[currentAnimation].FrameRectangle,
                    Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.85f);
            }

            if(intact == Length)
            {
//                currentAnimation = "Chain";
//                animations["Chain"].Play();

                PlayAnimation("Chain");

                Rectangle drawRectangle = new Rectangle((int)WorldLocation.X + ((48 * Length) - 48), (int)WorldLocation.Y - 48,
                    animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
                spriteBatch.Draw(
                    animations[currentAnimation].Texture,
                    Camera.WorldToScreen(drawRectangle),
                    animations[currentAnimation].FrameRectangle,
                    Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.85f);

//                currentAnimation = "Bridge";
//                animations["Bridge"].Play();

                PlayAnimation("Bridge");
            }


            //base.Draw(spriteBatch);
        }

        public Bridge(int x, int y, ContentManager content, int identity, int length = 13)
        {
            enabled = true;
            passable = false;
            identifier = identity;
            Length = length;
            intact = length;
            WorldLocation = new Vector2(x, y);
            //WorldCenter = new Vector2(x + ((48 * length) / 2), y + (48 / 2));
            CollisionRectangle = new Rectangle(0, 0, (48 * length), 48);
          //  player = gamePlayer;

            animations.Add("Bridge",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "bridge_bowser",
                    1));

            animations.Add("Chain",
            new AnimationStrip(
                content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                "bridge_chain",
                1));

//            currentAnimation = "Bridge";
//            animations["Bridge"].Play();

            PlayAnimation("Bridge");

        }
    }
}
