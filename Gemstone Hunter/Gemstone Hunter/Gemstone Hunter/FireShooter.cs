﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    public class FireShooter
    {
        public Vector2 WorldLocation = new Vector2(0, 0);
        private Random Rand = new Random();
        public bool shooting = false;
        public int location = 0;

        public void Update(GameTime gameTime)
        {
            int shoot = Rand.Next(0, 500);
            location = Rand.Next(20, 650);

            WorldLocation = new Vector2(Camera.Position.X + 800, WorldLocation.Y);

            if (shoot == 1)
            {
                shooting = true;
            }
            else
            {
                shooting = false;
            }

            //base.Update(gameTime);
        }



        public void Draw(SpriteBatch spriteBatch)
        {
            //base.Draw(spriteBatch);
        }


        public FireShooter(int y, Player gamePlayer, ContentManager content)
        {
            //enabled = true;
            //passable = true;
            WorldLocation = new Vector2(Camera.Position.X + 800, y);
            //Rectangle CollisionRectangle = new Rectangle(0, 0, 48, 48);
        }
    }
}
