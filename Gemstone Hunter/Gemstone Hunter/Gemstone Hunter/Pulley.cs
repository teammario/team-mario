﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class Pulley : GameObject
    {
        Player player;
        PulleyCorner LPulleyCorner;
        PulleyCorner RPulleyCorner;
        PulleyElevator LPulleyElevator;
        PulleyElevator RPulleyElevator;
        List<PulleyRope> RopeList = new List<PulleyRope>();
        public List<PulleyElevator> PulleyElevators = new List<PulleyElevator>();
        String size;
        int LRopeMin = 0;
        int RRopeMin = 0;
        int LRopeMax = 0;
        int RRopeMax = 0;
        int LRopeIndex = 0;
        int RRopeIndex = 0;
        int LRopeOffset = 0;
        int RRopeOffset = 0;
        bool Connected = false;
        bool valid = true;

        public Pulley(ContentManager Content, int x, int y, int width, int height,String size, Player player)
        {
            this.size = size;
            InitializeMembers(Content, x, y, width, height, player);
            PulleyElevators.Add(LPulleyElevator);
            PulleyElevators.Add(RPulleyElevator);
            currentAnimation = "";
            Enabled = true;
        }

        public void InitializeMembers(ContentManager Content, int x, int y, int width, int height, Player player)
        {
            this.player = player;
            this.worldLocation = new Vector2(x,y);
            this.CollisionRectangle = new Rectangle (0,0,width,height);
            Vector2 tileSize = new Vector2(TileMap.TileWidth,TileMap.TileHeight);
       
            //create our object based on size variable
                LPulleyCorner = new PulleyCorner(Content, (int)(x + tileSize.X), y, false);
                RPulleyCorner = new PulleyCorner(Content, (int)(x + width - (tileSize.X * 2)), y, true);

                LPulleyElevator = new PulleyElevator(Content, x+(int)tileSize.X, (int)(y + tileSize.Y*4), size, player);//set the y to one third of the tiles of the height
                RPulleyElevator = new PulleyElevator(Content, (int)(x + width - (tileSize.X)), (int)(y + height - tileSize.Y * 4), size, player);//set the x to two tiles from the right border

                LPulleyElevator.WorldLocation -= new Vector2(LPulleyElevator.CollisionRectangle.Width / 2,0);
                RPulleyElevator.WorldLocation -= new Vector2(RPulleyElevator.CollisionRectangle.Width / 2, 0);

                RopeList.Clear();
                int verticalRopeCount = (int)((height - LPulleyCorner.WorldLocation.Y+tileSize.Y)/tileSize.Y);
                for (int i = 1; i <= verticalRopeCount; i++) //create rope for left side
                {
                    RopeList.Add(new PulleyRope(Content, (int)LPulleyCorner.WorldLocation.X, (int)(LPulleyCorner.WorldLocation.Y + (tileSize.Y) +3), false, false));
                }
                for (int i = 1; i <= verticalRopeCount; i++) // for right side
                {
                    RopeList.Add(new PulleyRope(Content, (int)(RPulleyCorner.WorldLocation.X+tileSize.X-3), (int)(RPulleyCorner.WorldLocation.Y + (tileSize.Y) + 3), false, false));
                }
                int topRopeCount = (int)((RPulleyCorner.WorldLocation.X - (LPulleyCorner.WorldLocation.X + tileSize.X))/tileSize.X);
                for (int i = 1; i <= topRopeCount; i++) // for top side
                {
                    RopeList.Add(new PulleyRope(Content, (int)(LPulleyCorner.WorldLocation.X + (i * tileSize.X)), (int)(LPulleyCorner.WorldLocation.Y + 6), false, true));
                }

            //connect the string to the elevator in broad terms
            LRopeMin = 0;
            LRopeMax =verticalRopeCount-1;
            LRopeIndex = LRopeMax - 1;
            int min = LRopeMax;
            while (RopeList[LRopeMax].WorldLocation.Y + tileSize.Y < LPulleyElevator.WorldLocation.Y)
            {
                for (int i = LRopeMax; i >= min; i--)
                {
                    RopeList[i].WorldLocation = new Vector2(RopeList[i].WorldLocation.X, RopeList[i].WorldLocation.Y + tileSize.Y);
                }
                min--;
                LRopeIndex = min;
            }
            // now match exactly
            for (int d = 0; d < 48; d++)
            {
                if (RopeList[LRopeMax].WorldLocation.Y + tileSize.Y < LPulleyElevator.WorldLocation.Y)
                {
                    RopeList[LRopeMax].WorldLocation += new Vector2(0, 1);
                    if (RopeList[LRopeMax].WorldLocation.Y == LPulleyElevator.WorldLocation.Y - tileSize.Y)
                    {
                        LRopeOffset = d;
                        Connected = true;
                        break;
                    }
                }
                else if (RopeList[LRopeMax].WorldLocation.Y + tileSize.Y > LPulleyElevator.WorldLocation.Y)
                {
                    RopeList[LRopeMax].WorldLocation -= new Vector2(0, 1);
                    if (RopeList[LRopeMax].WorldLocation.Y == LPulleyElevator.WorldLocation.Y - tileSize.Y)
                    {
                        LRopeOffset = 48 -d;
                        Connected = true;
                        break;
                    }
                }
            }
            //same for right side
            //connect the string to the elevator in broad terms
            RRopeMin = LRopeMax + 1;
            RRopeMax = 2*LRopeMax+1;
            RRopeIndex = RRopeMax - 1;
            min = RRopeMax;
            while (RopeList[RRopeMax].WorldLocation.Y < RPulleyElevator.WorldLocation.Y - tileSize.Y)
            {
                for (int i = RRopeMax; i >= min; i--)
                {
                    RopeList[i].WorldLocation = new Vector2(RopeList[i].WorldLocation.X, RopeList[i].WorldLocation.Y + tileSize.Y);
                }
                min--;
                RRopeIndex = min;
            }
            // now match exactly
            for (int d = 0; d < 48; d++)
            {
                if (RopeList[RRopeMax].WorldLocation.Y + tileSize.Y < RPulleyElevator.WorldLocation.Y)
                {
                    RopeList[RRopeMax].WorldLocation += new Vector2(0, 1);
                    if (RopeList[RRopeMax].WorldLocation.Y == RPulleyElevator.WorldLocation.Y - tileSize.Y)
                    {
                        RRopeOffset = d;
                        Connected = true;
                        break;
                    }
                }
                else if (RopeList[RRopeMax].WorldLocation.Y + tileSize.Y > RPulleyElevator.WorldLocation.Y)
                {
                    RopeList[RRopeMax].WorldLocation -= new Vector2(0, 1);
                    if (RopeList[RRopeMax].WorldLocation.Y == RPulleyElevator.WorldLocation.Y - tileSize.Y)
                    {
                        RRopeOffset = 48 - d;
                        Connected = true;
                        break;
                    }
                }

            }
        }
        private void UpdateRope()
        {
            Vector2 tileSize = new Vector2(TileMap.TileWidth,TileMap.TileHeight);

            //reset our rope based on offsets
            RopeList[LRopeMax].WorldLocation -= new Vector2(0, LRopeOffset);
            LRopeOffset = 0;
            RopeList[RRopeMax].WorldLocation -= new Vector2(0, RRopeOffset);
            RRopeOffset = 0;

            if(RopeList[LRopeMax].WorldLocation.Y != LPulleyElevator.WorldLocation.Y -tileSize.Y)
            {
                //if elevator is below rope move rope down
                if ((RopeList[LRopeMax].WorldLocation.Y < LPulleyElevator.WorldLocation.Y - tileSize.Y))
                {
                    for (int d = 0; d < 48; d++)
                    {
                        RopeList[LRopeMax].WorldLocation += new Vector2(0, 1);
                        if (d >= 47)
                        {
                            for (int i = LRopeMax - 1; i >= LRopeIndex; i--)
                            {
                                RopeList[i].WorldLocation = new Vector2(RopeList[i].WorldLocation.X, RopeList[i].WorldLocation.Y + tileSize.Y);
                            }
                            d = 0;
                            LRopeIndex--;
                            if (RopeList[LRopeMin].WorldLocation.Y + tileSize.Y * 1.6 > LPulleyElevator.WorldLocation.Y || RopeList[RRopeMin].WorldLocation.Y + tileSize.Y * 1.6 > RPulleyElevator.WorldLocation.Y) //if we are out of bounds
                            {
                                Connected = false;
                                break;
                            }
                        }
                        if (RopeList[LRopeMax].WorldLocation.Y == LPulleyElevator.WorldLocation.Y - tileSize.Y)
                        {
                            LRopeOffset = d + 1;
                            break;
                        }
                    }
                }
                    //if elevator is above rope move rope up
                else if ((RopeList[LRopeMax].WorldLocation.Y >= LPulleyElevator.WorldLocation.Y - tileSize.Y))
                {
                    for (int i = LRopeIndex + 1; i < LRopeMax; i++)
                    {
                        RopeList[i].WorldLocation = new Vector2(RopeList[i].WorldLocation.X, RopeList[i].WorldLocation.Y - tileSize.Y);
                    }
                    LRopeIndex++;
                    //continue with updates
                    for ( int d = 0; d > -48; d--)
                    {
                        RopeList[LRopeMax].WorldLocation -= new Vector2(0, 1);
                        if (d == -47)
                        {
                            for (int i = LRopeIndex+1; i < LRopeMax; i++)
                            {
                                RopeList[i].WorldLocation = new Vector2(RopeList[i].WorldLocation.X, RopeList[i].WorldLocation.Y - tileSize.Y);
                            }
                            d = 0;
                            LRopeIndex++;
                            if (RopeList[LRopeMin].WorldLocation.Y + tileSize.Y * 1.6 > LPulleyElevator.WorldLocation.Y || RopeList[RRopeMin].WorldLocation.Y + tileSize.Y * 1.6 > RPulleyElevator.WorldLocation.Y) //if we are out of bounds
                            {
                                Connected = false;
                                break;
                            }
                        }
                        if (RopeList[LRopeMax].WorldLocation.Y == LPulleyElevator.WorldLocation.Y - tileSize.Y)
                        {
                            LRopeOffset =  (47 - (d+1));
                            break;
                        }
                    }
                }
            }
            //same for right side
            if (RopeList[RRopeMax].WorldLocation.Y != RPulleyElevator.WorldLocation.Y - tileSize.Y)
            {
                //if elevator is below rope move rope down
                if ((RopeList[RRopeMax].WorldLocation.Y < RPulleyElevator.WorldLocation.Y - tileSize.Y))
                {
                    for (int d = 0; d < 48; d++)
                    {
                        RopeList[RRopeMax].WorldLocation += new Vector2(0, 1);
                        if (d >= 47)
                        {
                            for (int i = RRopeMax - 1; i >= RRopeIndex; i--)
                            {
                                RopeList[i].WorldLocation = new Vector2(RopeList[i].WorldLocation.X, RopeList[i].WorldLocation.Y + tileSize.Y);
                            }
                            d = 0;
                            RRopeIndex--;
                            if (RopeList[LRopeMin].WorldLocation.Y + tileSize.Y * 1.6 > LPulleyElevator.WorldLocation.Y || RopeList[RRopeMin].WorldLocation.Y + tileSize.Y * 1.6 > RPulleyElevator.WorldLocation.Y) //if we are out of bounds
                            {
                                Connected = false;
                                break;
                            }
                        }
                        if (RopeList[RRopeMax].WorldLocation.Y == RPulleyElevator.WorldLocation.Y - tileSize.Y)
                        {
                            RRopeOffset = d + 1;
                            break;
                        }
                    }
                }
                //if elevator is above rope move rope up
                else if ((RopeList[RRopeMax].WorldLocation.Y >= RPulleyElevator.WorldLocation.Y - tileSize.Y))
                {
                    for (int i = RRopeIndex + 1; i < RRopeMax; i++)
                    {
                        RopeList[i].WorldLocation = new Vector2(RopeList[i].WorldLocation.X, RopeList[i].WorldLocation.Y - tileSize.Y);
                    }
                    RRopeIndex++;
                    //continue with updates
                    for (int d = 0; d > -48; d--)
                    {
                        RopeList[RRopeMax].WorldLocation -= new Vector2(0, 1);
                        if (d == -47)
                        {
                            for (int i = RRopeIndex + 1; i < RRopeMax; i++)
                            {
                                RopeList[i].WorldLocation = new Vector2(RopeList[i].WorldLocation.X, RopeList[i].WorldLocation.Y - tileSize.Y);
                            }
                            d = 0;
                            RRopeIndex++;
                            if (RopeList[LRopeMin].WorldLocation.Y + tileSize.Y * 1.6 > LPulleyElevator.WorldLocation.Y || RopeList[RRopeMin].WorldLocation.Y + tileSize.Y * 1.6 > RPulleyElevator.WorldLocation.Y) //if we are out of bounds
                            {
                                Connected = false;
                                break;
                            }
                        }
                        if (RopeList[RRopeMax].WorldLocation.Y == RPulleyElevator.WorldLocation.Y - tileSize.Y)
                        {
                            RRopeOffset = (47 - (d + 1));
                            break;
                        }
                    }
                }
            }

        }

        public override void Update(GameTime gameTime)
        {
            Rectangle test = new Rectangle(LPulleyElevator.CollisionRectangle.X+5, LPulleyElevator.CollisionRectangle.Y, LPulleyElevator.CollisionRectangle.Width-10, LPulleyElevator.CollisionRectangle.Height / 2);
            Rectangle test2 = new Rectangle(RPulleyElevator.CollisionRectangle.X+5, RPulleyElevator.CollisionRectangle.Y, RPulleyElevator.CollisionRectangle.Width-10, RPulleyElevator.CollisionRectangle.Height / 2);
            if (player.CollisionRectangle.Intersects(test) && Connected)
            {

                LPulleyElevator.WorldLocation = new Vector2(LPulleyElevator.WorldLocation.X, LPulleyElevator.WorldLocation.Y + 1);
                RPulleyElevator.WorldLocation = new Vector2(RPulleyElevator.WorldLocation.X, RPulleyElevator.WorldLocation.Y - 1);
                if (Connected)
                {
                    UpdateRope();
                }
                
                    player.movingVertically = true;
                    player.WorldLocation = new Vector2(player.WorldLocation.X, player.WorldLocation.Y + 1);
                
            }
            if (player.CollisionRectangle.Intersects(test2) && Connected)
            {

                LPulleyElevator.WorldLocation = new Vector2(LPulleyElevator.WorldLocation.X, LPulleyElevator.WorldLocation.Y - 1);
                RPulleyElevator.WorldLocation = new Vector2(RPulleyElevator.WorldLocation.X, RPulleyElevator.WorldLocation.Y + 1);
                if (Connected)
                {
                    UpdateRope();
                }

                player.movingVertically = true;
                player.WorldLocation = new Vector2(player.WorldLocation.X, player.WorldLocation.Y + 1);

            }
            if(!Connected && valid)//if we are out of bounds
            {
                RopeList.RemoveRange(0, RRopeMax + 1);
                valid = false;
                LPulleyElevator.WorldLocation += new Vector2(0, 5);
                RPulleyElevator.WorldLocation += new Vector2(0, 5);
            }
            else if (!Connected)
            {
                LPulleyElevator.WorldLocation += new Vector2(0, 5);
                RPulleyElevator.WorldLocation += new Vector2(0, 5);
            }

            if (LPulleyElevator.WorldLocation.Y == (Camera.ViewPortHeight))
            {
                PulleyElevators.Remove(LPulleyElevator);
            }

            if (RPulleyElevator.WorldLocation.Y == (Camera.ViewPortHeight))
            {
                PulleyElevators.Remove(RPulleyElevator);
            }
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            LPulleyCorner.Draw(spriteBatch);
            RPulleyCorner.Draw(spriteBatch);
            LPulleyElevator.Draw(spriteBatch);
            RPulleyElevator.Draw(spriteBatch);

            foreach (PulleyRope pr in RopeList)
            {
                pr.Draw(spriteBatch);
            }
        }
    }
}
