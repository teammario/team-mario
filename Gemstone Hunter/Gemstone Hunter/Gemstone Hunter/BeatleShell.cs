﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    public class BeatleShell: GameObject
    {
        //private Vector2 worldLocation = Vector2.Zero;
        //private Rectangle collisionRectangle;
        public int ID = 0;
        private int beatleIndex = -1;
        private List<BuzzyBeatle> beatles = new List<BuzzyBeatle>();
        private Player player;
        public string bcolor;
        /*
        public Vector2 WorldLocation
        {
            get { return worldLocation; }

            set { worldLocation = value; }
        }

        public Rectangle CollisionRectangle
        {
            get { return collisionRectangle; }

            set { collisionRectangle = value; }
        }
        */
        public override void Update(GameTime gameTime)
        {
            bool found = false;
            for (int index = beatles.Count - 1; index >= 0 && found == false; index--)
            {
                if (beatles[index].ID == ID)
                {
                    beatleIndex = index;
                    found = true;
                }
            }

            if (CollisionRectangle.Intersects(player.CollisionRectangle))
            {
                player.Kill();
            }

            if (beatleIndex == -1)
            {
                beatleIndex = 0;
            }

            if (beatles[beatleIndex].right == true)
            {
                WorldLocation = new Vector2(beatles[beatleIndex].WorldLocation.X + 12, beatles[beatleIndex].WorldLocation.Y + 7);
            }
            else
            {
                WorldLocation = new Vector2(beatles[beatleIndex].WorldLocation.X - 12, beatles[beatleIndex].WorldLocation.Y + 7);
            }

            base.Update(gameTime);
          //  CollisionRectangle = new Rectangle((int)WorldLocation.X, (int)WorldLocation.Y, 48, 48); 
            //base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            /*
            spriteBatch.Draw(
            animations[currentAnimation].Texture,
                Camera.WorldToScreen(CollisionRectangle),
                animations[currentAnimation].FrameRectangle,
                Color.Black, 0.0f, Vector2.Zero, SpriteEffects.None, 0.5f); 
            */
            //base.Draw(spriteBatch);
        }



        public BeatleShell(int x, int y, ContentManager content, Player gamePlayer, int id)
        {
            WorldLocation = new Vector2(x, y);

            enabled = true;
            player = gamePlayer;
           
           
                animations.Add("greenShell",
                            new AnimationStrip(
                                content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                                new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                                "buzzy_shell",
                                1));

                PlayAnimation("greenShell");
            

            CollisionRectangle = new Rectangle(x, y, 48, 48);

            ID = id;

            beatles = LevelManager.beatles;
            bool found = false;
            for (int index = beatles.Count - 1; index >= 0 && found == false; index--)
            {
                if (beatles[index].ID == ID)
                {
                    beatleIndex = index;
                    found = true;
                }
            }

            if (beatleIndex == -1)
            {
                beatleIndex = 0;
            }
        }
    }
}
