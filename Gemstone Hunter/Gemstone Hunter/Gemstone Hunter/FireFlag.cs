﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class FireFlag
    {
        private Player player;
        private Rectangle collisionRectangle;
        private Vector2 worldLocation;
        private bool flagged = false;
        private bool start = true;
        private List<FireShooter> fireShooters = new List<FireShooter>();
        private ContentManager Content;

        public void Update(GameTime gameTime)
        {
            if (collisionRectangle.Intersects(player.CollisionRectangle) && flagged == false)
            {
                if (start)
                {
                    fireShooters.Add(new FireShooter(48, player, Content));
                }
                else if(flagged == false)
                {
                    for (int x = fireShooters.Count - 1; x >= 0; x--)
                    {
                        fireShooters.RemoveAt(x);
                    }
                }

                flagged = true;
            }
        }

        public FireFlag(int x, int y, int height, Player gamePlayer, ContentManager content, List<FireShooter> fireshooters, bool starter)
        {
            worldLocation = new Vector2(x, y);
            collisionRectangle = new Rectangle(x, y, 24, height);
            player = gamePlayer;
            start = starter;
            fireShooters = fireshooters;
            Content = content;
        }
    }
}
