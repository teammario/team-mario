﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class Flag : GameObject
    {
        public Flag(ContentManager content, int x, int y)
        {
            WorldLocation = new Vector2(x, y);
            animations.Add("flag_idle",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "item_flag",
                    1));

            animations["flag_idle"].NextAnimation = "flag_idle";
            animations["flag_idle"].LoopAnimation = true;

            enabled = true;
            PlayAnimation("flag_idle");
        }

    }
}
