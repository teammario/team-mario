﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    public class Boss : Enemy
    {
        public bool Jump;
        public bool Fire;
        public bool Hammer;
        public bool Move;
        private Player player;
        private ContentManager Content;
        private bool canJump = false;
        private Random Rand = new Random();
        private int maxL;
        private int maxR;
        private float fallspeed = 6;
        private bool active = true;
        private float jumpheight = 300;
        private List<Bridge> Bridges = new List<Bridge>();
        private bool isFlipped = false;
        private int delay = 200;
        private int minHeight = 0;
        public bool isShootingFire = false;
        private float Movement = 0;
        private bool backup = false;
        private int delaying = 0;
        private const int DELAYS = 200;
        public bool hasThrown = false;
        public int numberThrown = 0;
        public const int HAMMERINTERVAL = 5;
        public int hammerDelay = 0;
        private bool upsideDown = false;

        public void bridges(List<Bridge> bridges)
        {
            Bridges = bridges;
        }

        public override void Update(GameTime gameTime)
        {
            if (Bridges.Count > 0)
            {
                for (int x = Bridges.Count - 1; x >= 0; x-- )
                {
                    if (Bridges[x].isDestroyed == true)
                    {
                        velocity = new Vector2(0, 0);
                        active = false;
                        if (currentAnimation == "boss")
                        {
                            PlayAnimation("pause");
                        }
                    }
                }
            }

            if (HP < 1)
            {
                active = false;
                animations["pause"].LoopAnimation = true;
                PlayAnimation("pause");
                upsideDown = true;
            }
            if (WorldLocation.X < Camera.Position.X + Camera.ViewPortWidth)
            {

                if (active)
                {

                    velocity = new Vector2(velocity.X, velocity.Y + fallspeed);

                    int jumping = Rand.Next(0, 400);
                    int throwing = Rand.Next(0, 300);
                    int moving = Rand.Next(0, 100);
                    int fires = Rand.Next(0, 350);

                    if (delaying > 0)
                    {
                        delaying--;
                    }
                    else if (backup == true)
                    {
                        backup = false;
                    }

                    if (moving == 1 && WorldLocation.X <= maxR)
                    {
                        Movement = 30;
                    }
                    else if (moving == 2 && WorldLocation.X >= maxL)
                    {
                        Movement = -30;
                    }
                    else if (WorldLocation.X >= maxR - 5)
                    {
                        Movement = -30;
                    }
                    else if (WorldLocation.X <= maxL + 5)
                    {
                        Movement = 30;
                    }

                    if (onGround)
                    {
                        canJump = true;
                    }

                    if (Jump)
                    {
                        if (jumping == 1 && onGround == true)
                        {
                            velocity = new Vector2(velocity.X, velocity.Y - jumpheight);
                        }
                    }
                    if (Hammer)
                    {
                        if (flipped == false)
                        {
                            if (throwing == 1)
                            {
                                hasThrown = true;
                            }
                        }
                    }
                    if (Fire)
                    {
                        if (flipped == false)
                        {
                            if (fires == 1)
                            {
                                isShootingFire = true;
                            }
                            else
                            {
                                isShootingFire = false;
                            }
                        }
                    }
                    if (Move && player.WorldLocation.X < WorldCenter.X)
                    {
                        //if (moving == 1 || worldLocation.X >= maxR)
                        // {
                        if (WorldLocation.X >= maxL && WorldLocation.X <= maxR)
                        {
                            //WorldLocation = new Vector2(WorldLocation.X - 7, WorldLocation.Y);
                            if (backup == false)
                            {
                                velocity = new Vector2(Movement, velocity.Y);
                            }
                        }
                        else if (WorldLocation.X <= maxL)
                        {
                            /*
                            if (velocity.X < 20 && velocity.X > -20)
                            {
                                WorldLocation = new Vector2(WorldLocation.X + 3, WorldLocation.Y);
                            }
                            */
                            backup = true;
                            delaying = DELAYS;
                            velocity.X = 0;
                            velocity = new Vector2(30, velocity.Y);
                        }
                        else if (WorldLocation.X >= maxR)
                        {
                            /*
                            if (velocity.X < 20 && velocity.X > -20)
                            {
                                WorldLocation = new Vector2(WorldLocation.X - 3, WorldLocation.Y);
                            }
                            */
                            backup = true;
                            delaying = DELAYS;
                            velocity = new Vector2(-30, velocity.Y);
                        }

                    }

                    if (player.WorldLocation.X > WorldCenter.X)
                    {
                        velocity = new Vector2(30, velocity.Y);
                    }


                }
                else
                {
                    if (delay > 0)
                    {
                        delay--;
                    }
                    else
                    {
                        isVerticallyCollidable = false;
                        isHorizontallyCollidable = false;
                        Passable = true;
                        WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y + 5);
                    }
                }
            }
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (player.WorldLocation.X > WorldCenter.X)
            {
                flipped = true;
            }
            else
            {
                flipped = false;
            }
            if (WorldLocation.Y < (Camera.Position.Y + Camera.ViewPortHeight - 24))
            {
                Rectangle drawRectangle = new Rectangle((int)WorldLocation.X, (int)WorldLocation.Y, animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
                if (upsideDown == false)
                {
                    spriteBatch.Draw(
                        animations[currentAnimation].Texture,
                        Camera.WorldToScreen(drawRectangle),
                        animations[currentAnimation].FrameRectangle,
                        Color.White, 0.0f, Vector2.Zero, flipped == true ? SpriteEffects.FlipHorizontally : SpriteEffects.None, 0.85f);
                }
                else
                {
                    spriteBatch.Draw(
                        animations[currentAnimation].Texture,
                        Camera.WorldToScreen(drawRectangle),
                        animations[currentAnimation].FrameRectangle,
                        Color.White, 0.0f, Vector2.Zero, SpriteEffects.FlipVertically, 0.85f);
                }
            }
            //base.Draw(spriteBatch);
        }

        public Boss(int x, int y, Player gamePlayer, ContentManager content, List<Bridge> newBridges, bool fire, bool jump, bool hammer, bool move) :
            base(gamePlayer, content, x, y)
        {
            enabled = true;
            topVulnerable = false;
            movementSpeed = 0;
            canBounce = false;
            HP = 3;
            Jump = jump;
            Fire = fire;
            Hammer = hammer;
            Move = move;
            player = gamePlayer;
            Content = content;

            bridges(newBridges);

            WorldLocation = new Vector2(x, y);
            CollisionRectangle = new Rectangle(x, y, (48 * 2), (48 * 2));

            maxL = (int)WorldLocation.X - 75;
            maxR = (int)WorldLocation.X + 75;
            minHeight = (int)WorldLocation.Y + 200;

            animations.Add("boss",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                    "bowser",
                    4));

            animations.Add("pause",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                    "bowser",
                    1));

            animations["pause"].NextAnimation = "jitter";
            animations["pause"].LoopAnimation = false;
            animations["pause"].FrameLength = 1.4f;

            animations.Add("jitter",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                    "bowser",
                    2));

            animations["jitter"].FrameLength = 0.1f;

            animations["boss"].FrameLength = 0.37f;


            PlayAnimation("boss");
        }

    }
}
