﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class Enemy : GameObject
    {
        protected bool useAI = true;
        protected int score = 100;
        protected float movementSpeed = 60.0f;
        protected Vector2 fallSpeed = new Vector2(0, 4);
        public bool deadly = true;
        protected bool topVulnerable = true;
        public bool fireballVulnerable = true;
	    protected bool underwater = TMXReader.underwater;
        protected bool canRespawn = false;
        protected bool canJump = false;
        protected bool isShelled = false;
        public int HP = 1;
        public bool canBounce = true;

        //public int Score = 0;

        protected int koopaID = -1;

        public bool dead = false;
        public List<String> spawns = new List<String>();
        protected ContentManager content;
        protected Texture2D EnemyTexture;
        protected Plist EnemyPlist;
        protected Player player;
        //protected Fireball fireball;
        protected int jumpHeight = 700;
        protected enum EnemyState {Winged, Walking, Kicked, InShell, Respawning};
        protected EnemyState enemyState = EnemyState.Walking;
        protected bool stateChanged = false;
        //public List<BreakBlock> BreakBlocks = new List<BreakBlock>();
        //public List<ItemBlock> ItemBlocks = new List<ItemBlock>();

        #region Constructor
        public Enemy(Player player, ContentManager content, int cellX, int cellY)
        {
            //BreakBlocks = LevelManager.BreakBlocks;
            //ItemBlocks = LevelManager.itemBlocks;

            this.player = player;
            passable = false;
            this.content = content;
            EnemyTexture = content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies");
            EnemyPlist = new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist");
        }
        #endregion

        #region Public Methods
        public void Jump()
        {
            velocity.Y = jumpHeight;
        }

        public void PlayDieAnimation(String animation)
        {
            useAI = false;
            deadly = false;
            isVerticallyCollidable = false;
            isHorizontallyCollidable = false;
            effect = SpriteEffects.FlipVertically;
            worldLocation += new Vector2(0, 4);
        }

        public override void Update(GameTime gameTime)
        {
            if (useAI)
            {
            if (LevelManager.BreakBlocks.Count > 0)
            {
                for (int A = LevelManager.BreakBlocks.Count - 1; A >= 0; A--)
                {
                    if (canBounce == true && LevelManager.BreakBlocks[A].isBouncable == true && CollisionRectangle.Intersects(LevelManager.BreakBlocks[A].Extended))
                    {
                        if (canBounce == true)
                        {
                            HP -= 1;
                        }
                    }
                }
            }
            if (LevelManager.itemBlocks.Count > 0)
            {
                for (int A = LevelManager.itemBlocks.Count - 1; A > -0; A--)
                {
                    if (LevelManager.itemBlocks[A].Jump > 0)
                    {
                        if(canBounce == true && CollisionRectangle.Intersects(LevelManager.itemBlocks[A].Extended()))
                            {
                                if (canBounce == true)
                                {
                                    HP -= 1;
                                }
                            }
                    }
                }
            }

            if (LevelManager.koopaShells.Count > 0)
            {
                for (int itrtr = LevelManager.koopaShells.Count - 1; itrtr >= 0; itrtr--)
                {
                    if (CollisionRectangle.Intersects(LevelManager.koopaShells[itrtr].CollisionRectangle) &&
                        (koopaID == -1 || koopaID != LevelManager.koopaShells[itrtr].ID))
                    {
                        HP -= 1;

                        
                    }
                }
            }

                stateChanged = false;
                if ((worldLocation.X <= 0 || worldLocation.X == TileMap.MapWidth - collisionRectangle.Width
                    || worldLocation.Y == TileMap.MapHeight - collisionRectangle.Height) && !canRespawn)
                {
                    this.dead = true;
                }
            if (player.CollisionRectangle.X - (48 * 12) >= this.WorldRectangle.X)
            {
                this.dead = true;
            }
                if (topVulnerable &&
                    CollisionRectangle.Top - player.CollisionRectangle.Bottom < 10
                    && CollisionRectangle.Top - player.CollisionRectangle.Bottom > -2
                    && player.CollisionRectangle.Right - CollisionRectangle.Left > 0
                    && player.CollisionRectangle.Right - CollisionRectangle.Left < player.CollisionRectangle.Width + CollisionRectangle.Width
                    )
                {
                    if (!isShelled)
                    {
                        HP -= 1;
                    }
                    else
                    {
                        if (enemyState == EnemyState.Winged)
                        {
                            enemyState = EnemyState.Walking;
                            stateChanged = true;
                        }
                        if (enemyState == EnemyState.Walking)
                        {
                            enemyState = EnemyState.InShell;
                            stateChanged = true;
                        }
                        else if (enemyState == EnemyState.InShell)
                        {
                         //   enemyState = EnemyState.Kicked;
                         //   stateChanged = true;
                        }
                    }
                    Sound_Engine.SoundEngine.Play("EnemyHit");
                    //player.Jump(false);
                    player.tinyJump();
                }
                if (HP < 1 && dead == false)
                {
                    dead = true;
                    player.Score += score;
                    LevelManager.scores.Add(new Score((int)(WorldCenter.X - Camera.Position.X), 
                        (int)(WorldCenter.Y - Camera.Position.Y), score.ToString(), content));
                }
            //if (fireball.CollisionRectangle.Intersects(this.CollisionRectangle))
            //{
            //    dead = true;
            //}
            if ((CollisionRectangle.Intersects(player.CollisionRectangle) && deadly && !dead) &&
                (player.isStarSmall || player.isStarBig))
            {
                this.dead = true;
            }
            else if (CollisionRectangle.Intersects(player.CollisionRectangle) && deadly && !dead)
                {
                    player.Kill();
                }

                base.Update(gameTime);
            }
        }
        #endregion
    }
}

