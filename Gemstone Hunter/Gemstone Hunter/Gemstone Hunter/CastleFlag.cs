﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class CastleFlag:GameObject
    {

        public CastleFlag(ContentManager content, int x, int y)
        {
            WorldLocation = new Vector2(x, y);
            animations.Add("castle_flag_idle",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "castle_flag",
                    1));

            animations["castle_flag_idle"].NextAnimation = "castle_flag_idle";
            animations["castle_flag_idle"].LoopAnimation = true;

            drawDepth = 1;
            enabled = true;
            PlayAnimation("castle_flag_idle");
        }
    }
}
