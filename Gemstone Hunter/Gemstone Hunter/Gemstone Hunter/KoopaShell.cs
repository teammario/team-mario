﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    public class KoopaShell : GameObject
    {
        //private Vector2 worldLocation = Vector2.Zero;
        //private Rectangle collisionRectangle;
        public int ID = 0;
        private int koopaIndex = -1;
        private List<Koopa> koopas = new List<Koopa>();
        private Player player;
        public string bcolor;
        /*
        public Vector2 WorldLocation
        {
            get { return worldLocation; }

            set { worldLocation = value; }
        }

        public Rectangle CollisionRectangle
        {
            get { return collisionRectangle; }

            set { collisionRectangle = value; }
        }
        */
        public override void Update(GameTime gameTime)
        {
            bool found = false;
            for (int index = koopas.Count - 1; index >= 0 && found == false; index--)
            {
                if (koopas[index].ID == ID)
                {
                    koopaIndex = index;
                    found = true;
                }
            }

            if (CollisionRectangle.Intersects(player.CollisionRectangle))
            {
                player.Kill();
            }

            if (koopaIndex == -1)
            {
                koopaIndex = 0;
            }

            if (koopas[koopaIndex].right == true)
            {
                WorldLocation = new Vector2(koopas[koopaIndex].WorldLocation.X + 12, koopas[koopaIndex].WorldLocation.Y + 7);
            }
            else
            {
                WorldLocation = new Vector2(koopas[koopaIndex].WorldLocation.X - 12, koopas[koopaIndex].WorldLocation.Y + 7);
            }

            base.Update(gameTime);
          //  CollisionRectangle = new Rectangle((int)WorldLocation.X, (int)WorldLocation.Y, 48, 48); 
            //base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            /*
            spriteBatch.Draw(
            animations[currentAnimation].Texture,
                Camera.WorldToScreen(CollisionRectangle),
                animations[currentAnimation].FrameRectangle,
                Color.Black, 0.0f, Vector2.Zero, SpriteEffects.None, 0.5f); 
            */
            //base.Draw(spriteBatch);
        }



        public KoopaShell(int x, int y, ContentManager content, Player gamePlayer, int id)
        {
            WorldLocation = new Vector2(x, y);

            enabled = true;
            player = gamePlayer;
          
                animations.Add("greenShell",
                            new AnimationStrip(
                                content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                                new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                                "koopa_green_shell",
                                1));

                PlayAnimation("greenShell");
            

            CollisionRectangle = new Rectangle(x, y, 48, 48);

            ID = id;

            koopas = LevelManager.koopas;
            bool found = false;
            for (int index = koopas.Count - 1; index >= 0 && found == false; index--)
            {
                if (koopas[index].ID == ID)
                {
                    koopaIndex = index;
                    found = true;
                }
            }

            if (koopaIndex == -1)
            {
                koopaIndex = 0;
            }
        }
    }
}
