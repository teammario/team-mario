using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        bool multiplayer = false;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Player player;
        Player player2;
        Player currentPlayer; //the current player being controlled
        SpriteFont pericles8;
        Vector2 scorePosition = new Vector2(20, 10); //sets position of score on hud
        enum GameState { TitleScreen, Playing, PlayerDead, GameOver };
        GameState gameState = GameState.TitleScreen;
        KeyboardState oldKeyboardState = new KeyboardState();

        Vector2 gameOverPosition = new Vector2(350, 300);//sets game over position
        Vector2 livesPosition = new Vector2(550, 10); //sets lives hud  position
        Vector2 timerPosition = new Vector2(650, 10); //sets timer hud position
        Vector2 worldNumber = new Vector2(450, 10);  //sets world number hud position
        Vector2 coinNumber = new Vector2(300, 10);  //sets the coins hud position

        Texture2D titleScreen;

        string MapLocation = @"Content\Maps\";
        int currentWorld = 1;
        int currentMap = 1;

        bool Paused = true;
        float deathTimer = 0.0f;
        float deathDelay = 3.0f;
        float elapseTime = 0f;
        int frameCounter = 0;
        int FPS = 0;

        
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            SoundEngine.Initialize(this.Content);
            
            SoundEngine.AddSoundInstance("BlockBreak", @"Sounds\BlockBreak");
            SoundEngine.AddSound("BowserFire", @"Sounds\BowserFire");
            SoundEngine.AddSound("BridgeCollapse", @"Sounds\BridgeCollapse");
            SoundEngine.AddSoundInstance("CastleTheme", @"Sounds\CastleTheme");
            SoundEngine.AddSoundInstance("CastleVictoryFanfare", @"Sounds\CastleVictoryFanfare");
            SoundEngine.AddSound("Coin", @"Sounds\Coin");
            SoundEngine.AddSound("Credits", @"Sounds\Credits");
            SoundEngine.AddSound("EnemyHit", @"Sounds\EnemyHit");
            SoundEngine.AddSound("Fireball", @"Sounds\Fireball");
            SoundEngine.AddSound("Firework", @"Sounds\Firework");
            SoundEngine.AddSound("FlagPole", @"Sounds\FlagPole");
            SoundEngine.AddSoundInstance("GameOver", @"Sounds\GameOver");
            SoundEngine.AddSoundInstance("GetPowerUp", @"Sounds\GetPowerUp");
            SoundEngine.AddSoundInstance("HitWall", @"Sounds\HitWall");
            SoundEngine.AddSoundInstance("InsideTheme", @"Sounds\InsideTheme");
            SoundEngine.AddSoundInstance("Jump", @"Sounds\Jump");
            SoundEngine.AddSound("Kick", @"Sounds\Kick");
            SoundEngine.AddSound("OneUp", @"Sounds\OneUp");
            SoundEngine.AddSoundInstance("OutsideTheme", @"Sounds\OutsideTheme");
            SoundEngine.AddSoundInstance("Pause", @"Sounds\Pause");
            SoundEngine.AddSoundInstance("Pipe", @"Sounds\Pipe");
            SoundEngine.AddSound("PlayerDie", @"Sounds\PlayerDie");
            SoundEngine.AddSound("SpawnItem", @"Sounds\SpawnItem");
            SoundEngine.AddSoundInstance("SuperJump", @"Sounds\SuperJump");
            SoundEngine.AddSoundInstance("StarTheme", @"Sounds\StarTheme");
            SoundEngine.AddSoundInstance("TimeWarning", @"Sounds\TimeWarning");
            SoundEngine.AddSoundInstance("UnderwaterTheme", @"Sounds\UnderwaterTheme");
            SoundEngine.AddSoundInstance("VictoryFanfare", @"Sounds\VictoryFanfare");
            SoundEngine.AddSound("Vine", @"Sounds\Vine");

            this.graphics.PreferredBackBufferWidth = 800;
            this.graphics.PreferredBackBufferHeight = 800;
            this.graphics.ApplyChanges();
            this.graphics.ToggleFullScreen();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            TileMap.Initialize(
                Content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\world48x48"),MapLocation + "\\"+ LevelManager.CurrentLevel +".tmx");
            TileMap.spriteFont =
                Content.Load<SpriteFont>(@"Fonts\Pericles8");

            pericles8 = Content.Load<SpriteFont>(@"Fonts\Pericles8");

            titleScreen = Content.Load<Texture2D>(@"Textures\TitleScreen");

            Camera.WorldRectangle = new Rectangle(0, 0, TMXReader.mapWidth * 48, 18 * 48);
            Camera.Position = new Vector2(0,96);
            Camera.ViewPortWidth = 800;
            Camera.ViewPortHeight = 800;

            player = new Player(Content);
            player2 = new Player(Content);
            currentPlayer = player;
            LevelManager.Initialize(Content, player);
        }

        private void StartNewGame()
        {
            currentPlayer.Revive();
            currentPlayer.LivesRemaining = 3;
            currentPlayer.WorldLocation = TileMap.StartLocation;
            currentWorld = 1;
            currentMap = 1;
            currentPlayer.levelTimer = currentPlayer.maxLevelTimer;

            LevelManager.LoadLevel(currentWorld,currentMap);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            elapseTime += (float)gameTime.ElapsedGameTime.Milliseconds;
            frameCounter++;
            
            if (elapseTime > 1000)
            {
                FPS = frameCounter;
                frameCounter = 0;
                elapseTime = 0;
            }
            base.Update(gameTime);
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back ==
                ButtonState.Pressed)
                this.Exit();
            
            KeyboardState keyState = Keyboard.GetState();
            GamePadState gamepadState = GamePad.GetState(PlayerIndex.One);
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (keyState.IsKeyDown(Keys.Escape))
            {
                this.graphics.ToggleFullScreen();
            }
            if (this.graphics.IsFullScreen)
            {
                Camera.ViewPortWidth = Window.ClientBounds.Width;
                Camera.ViewPortHeight = Window.ClientBounds.Height;
            }
            else
            {
                Camera.ViewPortWidth = 800;
                Camera.ViewPortHeight = 800;
            }
            if (gameState == GameState.TitleScreen)
            {
                Paused = true;
                Sound_Engine.SoundEngine.Play("OutsideTheme");
                if (keyState.IsKeyDown(Keys.Enter) ||
                    gamepadState.Buttons.A == ButtonState.Pressed)
                {
                    Sound_Engine.SoundEngine.Stop("OutsideTheme");
                    StartNewGame();
                    gameState = GameState.Playing;
                }
            }

            if (gameState == GameState.Playing)
            {
                if (currentPlayer.levelTimer <= 0 && currentPlayer.countingTime)
                {
                    currentPlayer.Kill(); //kills mario if no more timer
                }

                if (keyState.IsKeyDown(Keys.Enter) && !oldKeyboardState.IsKeyDown(Keys.Enter))
                {
                    Paused = !Paused; //toggle pause
                    Sound_Engine.SoundEngine.Play("Pause");
                }

                if (!Paused)
                {
                    LevelManager.Update(gameTime);
                    currentPlayer.Update(gameTime);
                    if (player.countingTime)
                    {
                        currentPlayer.levelTimer -= gameTime.ElapsedGameTime.Milliseconds;
                    }
                    if (currentPlayer.Dead)
                    {
                        if (currentPlayer.LivesRemaining > 0)
                        {
                            gameState = GameState.PlayerDead;
                            deathTimer = 0.0f;
                        }
                        else
                        {
                            gameState = GameState.GameOver;
                            deathTimer = 0.0f;
                        }
                    }
                }
                else
                {
                    Sound_Engine.SoundEngine.Pause(Tile_Engine.TMXReader.music);
                }
            }

            if (gameState == GameState.PlayerDead)
            {
                currentPlayer.Update(gameTime);
                LevelManager.Update(gameTime);
                deathTimer += elapsed;
                if (deathTimer > deathDelay)
                {
                    LevelManager.ReloadLevel();
                    if (multiplayer)
                    {
                        if(currentPlayer.Equals(player))
                        {
                            currentPlayer = player2;
                        }
                        else
                        {
                            currentPlayer = player;
                        }
                    }
                    currentPlayer.Revive();
                    SoundEngine.Play(TileMap.BackgroundMusic);
                    currentPlayer.levelTimer = currentPlayer.maxLevelTimer;
                    gameState = GameState.Playing;
                }
            }

            if (gameState == GameState.GameOver)
            {
                deathTimer += elapsed;
                if (deathTimer > deathDelay)
                {
                    gameState = GameState.TitleScreen;
                }
            }

            base.Update(gameTime);
            oldKeyboardState = keyState;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(TMXReader.background);

            spriteBatch.Begin(
                SpriteSortMode.BackToFront,
                BlendState.AlphaBlend);

            if (gameState == GameState.TitleScreen)
            {
                spriteBatch.Draw(titleScreen, new Rectangle(0,0,Window.ClientBounds.Width,Window.ClientBounds.Height), Color.White);
            }

            if ((gameState == GameState.Playing) ||
                (gameState == GameState.PlayerDead) ||
                (gameState == GameState.GameOver))
            {
                TileMap.Draw(spriteBatch);
                currentPlayer.Draw(spriteBatch);
                LevelManager.Draw(spriteBatch);
                //prints the score at the desired position was original to gem hunters game
                if (multiplayer && currentPlayer.Equals(player2))
                {
                    spriteBatch.DrawString(pericles8, "   Luigi \nScore: " + player.Score.ToString(), scorePosition, Color.White);
                }
                else
                {
                    spriteBatch.DrawString(pericles8, "   Mario \nScore: " + player.Score.ToString(), scorePosition, Color.White);
                }
                //gets the number of lives this was defaulted in the original gem hunters game
                spriteBatch.DrawString(pericles8, "Lives : " + currentPlayer.LivesRemaining.ToString(), livesPosition, Color.White);
                //gets the time left. I need someone to come up with a timer class that does a timer clock to count down when the level loads
                //and stops at the end of the level. It also has to kill mario if it reaches 000 before mario finishes the level
                spriteBatch.DrawString(pericles8, "Time : " + currentPlayer.levelTimer / 1000, timerPosition, Color.White);
                //spriteBatch.DrawString(pericles8,"Time : " + player.TimeRemaining.ToString(), timerPosition, Color.White);
                //prints the world number.  This works and is based on the original world numbers loaded to the LevelManager class
                spriteBatch.DrawString(pericles8, "World \n " + LevelManager.CurrentLevel.ToString(), worldNumber, Color.White);
                //prints the number of coins gotten.  This also works but assumes the gemstones are coins. It is figured in LevelManager
                //but sent to the Player class just like gem half of score is 
                spriteBatch.DrawString(pericles8, "Coins X  " + currentPlayer.Coins.ToString(), coinNumber, Color.White);
                if(Paused)
                    spriteBatch.DrawString(pericles8, "Paused", new Vector2(this.Window.ClientBounds.Width / 2-20, this.Window.ClientBounds.Height / 2), Color.White);
            }

            if (gameState == GameState.PlayerDead)
            {
                
            }

            if (gameState == GameState.GameOver)
            {
                spriteBatch.DrawString(
                    pericles8,
                    "G A M E  O V E R !",
                    gameOverPosition,
                    Color.White);
            }
            spriteBatch.DrawString(pericles8, "FPS " + ((int)FPS).ToString(), new Vector2(Window.ClientBounds.Width-100, Window.ClientBounds.Height-20), Color.Black);

            if (LevelManager.drawScore == true)
            {
                for (int x = LevelManager.flagPoles.Count - 1; x >= 0; x--)
                {
                    spriteBatch.DrawString(pericles8, LevelManager.flagPoles[x].slideScore.ToString(), Camera.WorldToScreen(new Vector2(LevelManager.flagPoles[x].WorldLocation.X + LevelManager.flagPoles[x].CollisionRectangle.Width,
                        LevelManager.flagPoles[x].WorldLocation.Y + LevelManager.flagPoles[x].CollisionRectangle.Height - (LevelManager.flagPoles[x].flag.WorldLocation.Y - LevelManager.flagPoles[x].WorldLocation.Y))), Color.White);
                }
            }
            if (LevelManager.drawVictoryMessage)
            {
                spriteBatch.DrawString(pericles8, LevelManager.princessToad.Message, Camera.WorldToScreen(new Vector2(LevelManager.princessToad.WorldLocation.X - 48*5,
                    LevelManager.princessToad.WorldLocation.Y - 48*6)), Color.White);

            }
            spriteBatch.End();

            base.Draw(gameTime);
        }

    }
}
