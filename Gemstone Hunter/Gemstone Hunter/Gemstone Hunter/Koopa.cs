﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    public class Koopa : Enemy
    {
        public bool inShell = false;
        private float fallSpeed = 5f;
        private Player player;
        private float walkSpeed = 30f;
        public bool start = false;
        private float kickSpeed = 200f;
        public bool right = false;
        public Rectangle extended;
        private int delay = 0;
        private const int STARTED = 12;
        private int pause = 0;
        private int pauseKick = 0;
        private int respawn = 0;
        private int respawning = 0;
        private const int PAUSESHELL = 30;
        private const int PAUSEKICKED = 30;
        private const int RESPAWN = 600;
        private const int RESPAWNING = 500;
        private EnemyState previousState = EnemyState.Walking;
        public int ID = 0;
        private ContentManager thisContent;
        private int Float = 0;
        private int max = 0;
        public string acolor;

        public Koopa(int x, int y, Player gamePlayer, ContentManager content, int id = 0) :
            base(gamePlayer, content, x, y)
        {
            enabled = true;
            topVulnerable = true;
            canBounce = true;
            movementSpeed = 0;
            isShelled = true;
            deadly = true;
            WorldLocation = new Vector2(x, y);
            extended = new Rectangle(x - 12, y + 5, (48 + 12), 48);
            //acolor = color;
            thisContent = content;

            ID = id;

            koopaID = ID;

            enemyState = EnemyState.Walking;

            player = gamePlayer;

           
                animations.Add("green",
                            new AnimationStrip(
                                content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                                new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                                "koopa_green",
                                2));

                animations.Add("greenShell",
                            new AnimationStrip(
                                content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                                new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                                "koopa_green_shell",
                                1));

                animations.Add("greenPeak",
                            new AnimationStrip(
                                content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                                new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                                "koopa_green_peak",
                                1));

                animations.Add("greenPeakShell",
                            new AnimationStrip(
                                content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Enemies"),
                                new Plist(@"Content\Textures\Super_Mario_Sprites\Enemies.plist"),
                                "koopa_green_shell",
                                1));

                animations["green"].FrameLength = 0.48f;

                animations["greenPeak"].NextAnimation = "greenPeakShell";
                animations["greenPeak"].LoopAnimation = false;
                animations["greenPeak"].FrameLength = 0.48f;
                animations["greenPeakShell"].NextAnimation = "greenPeak";
                animations["greenPeakShell"].LoopAnimation = false;
                animations["greenPeakShell"].FrameLength = 0.48f;

                PlayAnimation("green");

               
           

            max = animations[currentAnimation].FrameRectangle.Height;
            //CollisionRectangle = new Rectangle(0, 0, animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
        }
        
        
        
        public override void Update(GameTime gameTime)
        {
            if (animations[currentAnimation].FrameRectangle.Height < max)
            {
                Float = max - animations[currentAnimation].FrameRectangle.Height;
            }

            if (start == false)
            {
                if (WorldLocation.X < (Camera.Position.X + Camera.ViewPortWidth) && WorldLocation.X > Camera.Position.X)
                {
                    start = true;
                }
            }

            if (right == true)
            {
                extended = new Rectangle(0, 0, 0, 0);
                    //new Rectangle((int)WorldLocation.X - 12, (int)WorldLocation.Y + 12, 48, 48);
            }
            else
            {
                extended = new Rectangle(0, 0, 0, 0);
                    //new Rectangle((int)WorldLocation.X - 12, (int)WorldLocation.Y + 12, 48, 48);
            }
            if (start == true)
            {
                
                if (delay < STARTED)
                {
                    delay++;
                }
                if (enemyState == EnemyState.InShell)
                {
                    velocity = new Vector2(0, velocity.Y);
                    deadly = false;

                    if (respawn < RESPAWN)
                    {
                        respawn++;
                    }
                    else if (respawn >= RESPAWN)
                    {
                        enemyState = EnemyState.Respawning;
                        stateChanged = true;
                        respawning = 0;
                        pause = 0;
                    }

                    if (inShell == false)
                    {
                       
                            PlayAnimation("greenShell");
                       
                        inShell = true;
                    }

                    if (previousState != EnemyState.InShell)
                    {
                        pause = 0;
                    }
                    else if(pause < PAUSESHELL)
                    {
                        pause++;
                    }

                    if (CollisionRectangle.Intersects(player.ExtendedRectangle) && pause >= PAUSESHELL)
                    {
                        if (player.WorldCenter.X < WorldCenter.X)
                        {
                            right = true;
                            velocity = new Vector2(kickSpeed, velocity.Y);
                            enemyState = EnemyState.Kicked;
                            LevelManager.koopaShells.Add(new KoopaShell((int)WorldLocation.X, (int)WorldLocation.Y + 12, thisContent, player, ID)); 
                            stateChanged = true;
                        }
                        else if (player.WorldCenter.X > WorldCenter.X)
                        {
                            right = false;
                            velocity = new Vector2(-1 * kickSpeed, velocity.Y);
                            enemyState = EnemyState.Kicked;
                            LevelManager.koopaShells.Add(new KoopaShell((int)WorldLocation.X, (int)WorldLocation.Y + 12, thisContent, player, ID)); 
                            stateChanged = true;
                        }
                        pause = 0;
                    }
                    else
                    {
                        velocity = new Vector2(0, velocity.Y);
                    }

                    velocity = new Vector2(velocity.X, velocity.Y + fallSpeed);

                }
                else if (enemyState == EnemyState.Walking)
                {
                    if (velocity.X > -15 && velocity.X < 15 && start == true && delay >= STARTED)
                    {
                        if (right == true)
                        {
                            right = false;
                            velocity = new Vector2(-1 * walkSpeed, velocity.Y);
                        }
                        else
                        {
                            right = true;
                            velocity = new Vector2(walkSpeed, velocity.Y); 
                        }
                    }

                    if (right == true)
                    {
                        velocity = new Vector2(walkSpeed, velocity.Y);
                    }
                    else
                    {
                        velocity = new Vector2(-1 * walkSpeed, velocity.Y);
                    }
                    velocity = new Vector2(velocity.X, velocity.Y + fallSpeed);
                }
                else if (enemyState == EnemyState.Kicked)
                {
                    //deadly = true;
                    
                    if (velocity.X < 100 && velocity.X > -100)
                    {
                        if (right == true)
                        {
                            velocity = new Vector2(-1 * kickSpeed, velocity.Y);
                            right = false;
                        }
                        else
                        {
                            velocity = new Vector2(kickSpeed, velocity.Y);
                            right = true;
                        }
                    }
                    
                    if (previousState != EnemyState.Kicked)
                    {
                        pauseKick = 0;
                    }
                    else if (pauseKick < PAUSEKICKED)
                    {
                        pauseKick++;
                    }

                    if (CollisionRectangle.Intersects(player.ExtendedRectangle) && player.WorldCenter.Y < WorldLocation.Y && pauseKick >= PAUSEKICKED - 3)
                    {
                        enemyState = EnemyState.InShell;
                        LevelManager.removeShell(ID);
                        velocity = new Vector2(0, velocity.Y);
                        stateChanged = true;
                        pauseKick = 0;
                        pause = 0;
                        respawn = 0;
                        respawning = 0;
                    }

                    velocity = new Vector2(velocity.X, velocity.Y + fallSpeed);
                }
                else if (enemyState == EnemyState.Respawning)
                {
                    if (respawning == 0)
                    {
                        
                            PlayAnimation("greenPeak");
                      

                        
                    }

                    if (pause < PAUSESHELL)
                    {
                        pause++;
                    }

                    if (CollisionRectangle.Intersects(player.ExtendedRectangle) && pause >= PAUSESHELL)
                    {
                        if (player.WorldCenter.X < WorldCenter.X)
                        {
                            right = true;
                            velocity = new Vector2(kickSpeed, velocity.Y);
                            enemyState = EnemyState.Kicked;
                           
                                PlayAnimation("greenShell");
                           

                            
                            respawning = 0;
                            respawn = 0;
                            pause = 0;
                            LevelManager.koopaShells.Add(new KoopaShell((int)WorldLocation.X, (int)WorldLocation.Y + 12, thisContent, player, ID));
                            stateChanged = true;
                        }
                        else if (player.WorldCenter.X > WorldCenter.X)
                        {
                            right = false;
                            velocity = new Vector2(-1 * kickSpeed, velocity.Y);
                            enemyState = EnemyState.Kicked;
                            
                                PlayAnimation("greenShell");
                           
                            respawning = 0;
                            respawn = 0;
                            pause = 0;
                            LevelManager.koopaShells.Add(new KoopaShell((int)WorldLocation.X, (int)WorldLocation.Y + 12, thisContent, player, ID));
                            stateChanged = true;
                        }
                        pause = 0;
                    }

                    if (respawning < RESPAWNING)
                    {
                        respawning++;
                    }
                    else if (respawning >= RESPAWNING)
                    {
                        enemyState = EnemyState.Walking;
                        stateChanged = true;
                        deadly = true;
                        inShell = false;
                        respawning = 0;
                        respawn = 0;
                        WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y - Float);
                        
                        
                            PlayAnimation("green");
                        
                    }

                    velocity = new Vector2(velocity.X, velocity.Y + fallSpeed);
                }

                previousState = enemyState;
            }
            base.Update(gameTime);
        }

        public void fallOffScreen()
        {
            IsVerticallyCollidable = false;
            IsHorizontallyCollidable = false;
            passable = true;
            deadly = false;
            if (enemyState == EnemyState.Kicked)
            {
                enemyState = EnemyState.InShell;
            }
            velocity = new Vector2(0, velocity.Y + fallSpeed);
            dead = true;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            SpriteEffects spriteEffect = SpriteEffects.None;

            if (right == true && HP > 0)
            {
                spriteEffect = SpriteEffects.FlipHorizontally;
            }
            if (dead == true)
            {
                spriteEffect = SpriteEffects.FlipVertically;
            }

            Rectangle drawRectangle = new Rectangle((int)WorldLocation.X, (int)WorldLocation.Y, animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
            spriteBatch.Draw(
                animations[currentAnimation].Texture,
                Camera.WorldToScreen(drawRectangle),
                animations[currentAnimation].FrameRectangle,
                Color.White, 0.0f, Vector2.Zero, spriteEffect, 0.5f); 
            //base.Draw(spriteBatch);
        }

        
    }
}
