﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    class Axe : GameObject
    {
        public bool isVisible;
        public int identifier;
        private Player player;

        public override void Update(GameTime gameTime)
        {
            if (CollisionRectangle.Intersects(player.CollisionRectangle) && isVisible == true)
            {
                isVisible = false;
            }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (isVisible)
            {
                Rectangle drawRectangle = new Rectangle((int)worldLocation.X, (int)worldLocation.Y,
                             animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
                spriteBatch.Draw(
                    animations[currentAnimation].Texture,
                    Camera.WorldToScreen(drawRectangle),
                    animations[currentAnimation].FrameRectangle,
                    Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
            }

//            spriteBatch.Draw(
  //                  animations[currentAnimation].Texture,
    //                Camera.WorldToScreen(collisionRectangle),
      //              animations[currentAnimation].FrameRectangle,
        //            Color.Black, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);

        }

        public Axe(int x, int y, int identity, ContentManager content, Player gamePlayer)
        {
            enabled = true;
            identifier = identity;
            player = gamePlayer;
            worldLocation = new Vector2(x, y);
            CollisionRectangle = new Rectangle(x, y, 48, 48);
            worldLocation = new Vector2(x, y);
            isVisible = true;

            animations.Add("Axe",
            new AnimationStrip(
                content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                "axe",
                3));
            animations["Axe"].LoopAnimation = true;
            animations["Axe"].FrameLength = 0.37f;
            
            PlayAnimation("Axe");
        }
    }
}
