﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    public class FireShot : Enemy
    {
        private int playerAxis = 0;
        private bool Boss;
        private bool goUp = false;

        public override void Update(GameTime gameTime)
        {
            int offset = 0;

            if (Boss == true)
            {
                if (playerAxis > WorldLocation.Y && goUp == false)
                {
                    offset = 1;
                }
                else if (playerAxis < WorldLocation.Y && goUp == true)
                {
                    offset = -1;
                }
            }

            WorldLocation = new Vector2(WorldLocation.X - 2, WorldLocation.Y + offset);
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            Rectangle drawRectangle = new Rectangle((int)WorldLocation.X, (int)WorldLocation.Y, animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
            spriteBatch.Draw(
                animations[currentAnimation].Texture,
                Camera.WorldToScreen(drawRectangle),
                animations[currentAnimation].FrameRectangle,
                Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.85f);
            
            //base.Draw(spriteBatch);
        }


        public FireShot(int x, int y, Player theplayer, ContentManager content, bool boss = false) : base(theplayer, content, x, y)
        {
            enabled = true;
            passable = true;
            topVulnerable = false;
            canBounce = false;
            movementSpeed = 0;
            WorldLocation = new Vector2(x, y);
            CollisionRectangle = new Rectangle(0, 0, 48, 24);

            Boss = boss;

            if (Boss == true)
            {
                playerAxis = (int)theplayer.WorldLocation.Y;

                if (playerAxis > (int)WorldLocation.Y)
                {
                    goUp = false;
                }
                else
                {
                    goUp = true;
                }
            }
            else
            {
                playerAxis = (int)WorldLocation.Y;
            }

            animations.Add("fire",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "fireball_bowser",
                    2));

            //animations["pause"].NextAnimation = "jitter";
            animations["fire"].LoopAnimation = true;
            animations["fire"].FrameLength = 0.1f;

            PlayAnimation("fire");
        }

    }
}
