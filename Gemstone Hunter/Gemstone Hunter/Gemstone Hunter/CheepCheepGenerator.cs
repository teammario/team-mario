﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class CheepCheepGenerator:Enemy
    {
        public List<CheepCheep> CheepCheepGen = new List<CheepCheep>();
        const float spawnTimer = .95f;
        Player player;
        ContentManager content;
        String Direction;
        String Color;
        public int underwaterCount = 0;

        public CheepCheepGenerator(Player player, ContentManager content, int cellX, int cellY, String Direction, String Color)
            : base(player, content, cellX, cellY)
        {
            CheepCheepGen.Clear();
            this.player = player;
            this.content = content;
            this.Direction = Direction;
            this.Color = Color;
            this.worldLocation = new Vector2(cellX, cellY);

            currentAnimation = "";
        }
        public void Fly(GameTime gameTime)
        {
            CheepCheepGen.Add(new CheepCheep(player, content, (int)this.worldLocation.X, (int)this.worldLocation.Y, Direction, Color));
        }
        public override void Update(GameTime gameTime)
        {
            float elapsedTimer = (float)gameTime.TotalGameTime.Milliseconds / 1000;

            if (spawnTimer <= elapsedTimer &&
                player.CollisionRectangle.X + (48 * 15) >= this.WorldRectangle.X &&
                CheepCheepGen.Count < 1 &&
                underwater == false)
            {
                Fly(gameTime);
            }
            else if (spawnTimer <= elapsedTimer &&
                player.CollisionRectangle.X + (48 * 15) >= this.WorldRectangle.X &&
                CheepCheepGen.Count < 1 &&
                underwater == true &&
                underwaterCount < 1)
            {
                Fly(gameTime);
                if (underwater == true)
                {
                    underwaterCount++;
                }
            }
            foreach (CheepCheep CC in CheepCheepGen)
            {
                if (CC.dead)
                {
                    CheepCheepGen.Remove(CC);
                    break;
                }
            }

            base.Update(gameTime);
        }
    }
}
