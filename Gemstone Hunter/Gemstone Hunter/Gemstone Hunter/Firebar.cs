﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;

namespace Gemstone_Hunter
{
    class Firebar : Enemy
    {
        public double radius;
        //fireballVulnerable = false;
        public int degree = 0;
        public bool clockwise = true;
        private Vector2 Center;
        private float Rotation = 0;
        private const int INTERVAL = 5;
        private int delay = 5;
        private float rotator = 0;

        public override void Update(GameTime gameTime)
        {
            if (clockwise)
            {
                degree++;
            }
            else
            {
                degree--;
            }

            if (degree > 359)
            {
                degree = 0;
            }
            else if (degree < 0)
            {
                degree = 359;
            }

            double newLocX = (Center.X + (radius * (Math.Cos(Math.PI * degree / 180)))) - 12;
            double newLocY = (Center.Y + (radius * (Math.Sin(Math.PI * degree / 180)))) - 12;

            WorldLocation = new Vector2((int)newLocX, (int)newLocY);


            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            /*
            Rotation += 90;
            if (Rotation > (3 * 90))
            {
                Rotation = 0;
            }
            */

            int bumpX = animations[currentAnimation].FrameRectangle.Width;
            int bumpY = animations[currentAnimation].FrameRectangle.Height;

            int Xloc = (int)WorldLocation.X;
            int Yloc = (int)WorldLocation.Y;

            delay++;
            if (delay >= INTERVAL)
            {
                rotator -= (float)(Math.PI * 90 / 180);
                delay = 0;
            }
            
            if (rotator <= (Math.PI * -360 / 180) || rotator >= (Math.PI * 360 / 180))
            {
                rotator = 0;
            }



            if (rotator == (float)(Math.PI * -90 / 180))
            {
                Yloc += (int)bumpY ;
            }
            else if (rotator == (float)(Math.PI * -180 / 180))
            {
                Xloc += (int)bumpX ;
                Yloc += (int)bumpY ;
            }
            else if (rotator == (float)(Math.PI * -270 / 180))
            {
                Xloc += (int)bumpX;
            }


            Rectangle drawRectangle = new Rectangle(Xloc, Yloc, 
                animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
            spriteBatch.Draw(
                animations[currentAnimation].Texture,
                Camera.WorldToScreen(drawRectangle),
                animations[currentAnimation].FrameRectangle,
                Color.White, rotator, Vector2.Zero, SpriteEffects.None, 0.5f);

            //base.Draw(spriteBatch);
        }


        public Firebar(int centerX, int centerY, int number, bool Clockwise, ContentManager content, Player gamePlayer) : 
            base( gamePlayer, content, centerX, centerY)
        {
            topVulnerable = false;
            canBounce = false;
            enabled = true;
            WorldLocation = new Vector2(centerX - 12, centerY + (number * 24) - 12);
            CollisionRectangle = new Rectangle((int)WorldLocation.X, (int)WorldLocation.Y, 24, 24);
            clockwise = Clockwise;
            radius = number * 24;
            Center = new Vector2(centerX, centerY);

            animations.Add("fireball",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "fireball_generic",
                    1));

            PlayAnimation("fireball");
        }
    }
}
