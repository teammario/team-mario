﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Tile_Engine;
using Sound_Engine;

namespace Gemstone_Hunter
{
    public class Player : GameObject
    {
    // Declarations/Initializations
    #region Declarations/Initializations
        public bool didWarp = false;
        public bool isWarpingFrom = false;
        public bool StopCinematic = false;
        public int fireworks = 0;
        public bool countingTime = true;
        public int maxLevelTimer = 400000;
        public int levelTimer = 0;
        public int Coins = 0;
        public bool canMove = true;
        private Vector2 fallSpeed = new Vector2(0, 20);
        private Vector2 sinkSpeed = new Vector2(0, 5);
        private int walkspeed = 240;
        private int runspeed = 340;
        private int swimspeed = 140;
        private float moveScaleWalk = 0.0f;         // change as needed
        private float moveScaleSwim = 0.0f;
        private float acceleration = 10.0f;     // will be changed when walking/running
        private float deceleration = 10.0f;     // will be changed when crouching
        private bool dead = false;
        public bool isCrouching = false;
	    public bool isWarping = false;
        private Vector2 crouchLocation;
        private bool underwater = TMXReader.underwater;        // determine underwater or not
        private bool running = false;           // determine affect of other control inputs
        private int score = 0;
        private int livesRemaining = 3;
        public int fireballCount = 0;
        private const int fireballMax = 2;
        // Temp controls bool
        private bool hasBumped = false; // used to track if we have bumped for crouching
        private bool aPressable = true;
        private bool yPressable = true;
 	    private bool stateChanged = false;
 	    private bool xPressable = true;
        private bool rbPressable = true;
        private bool lbPressable = true;
        private bool rjPressable = true;
        private bool ljPressable = true;
        private bool bPressable = true;
        // States
        public bool isLuigi = false;
	    public bool isSmall = true;
        public bool isBig = false;
        public bool isInvulnerable = false;
        public bool isFire = false;
        public bool isStarBig = false;
        public bool isStarSmall = false;
        public bool isGrowing = false;
        public bool hasGrown = false;
        public bool isShrinking = false;
        public bool hasShrunk = false;
        public bool isPowerUp = false;
        public bool hasPoweredUp = false;
        //public bool isSwimming = false;       // replaced with bool underwater
        public bool isClimbing = false;
        public bool isPlayerFlipped = false;
        // Remember state for star
        public string wasState = null;
        public bool passable= false;
        // Random generator
        private Random rand = new Random();
        // Timer Delay
        private static float throwTimer = 0f;
        private static float throwDelay = 250f;
        private static float invulnTimer = 0f;
        private static float invulnDelay = 100000f;
        private static float starTimer = 0f;
        private static float starDelay = 300000f;
        private static float stateChangeTimer = 0f;
        private static float stateChangeDelay = 1000f;
        private static float stateChangeCount = 0;
        private Plist PlayerPlist;
        private Texture2D PlayerTexture;
        private bool hasJumped = false;
        private KeyboardState previous = Keyboard.GetState();
        private Rectangle extendedRectangle = new Rectangle(0, 0, 0, 0);

        public Rectangle ExtendedRectangle
        {
            get{return extendedRectangle;}
        }

    //player's reference to item blocks

  //  private List<ItemBlock> itemBlocks;
    private List<Bridge> bridges;

    public void getNewBlockList(List<ItemBlock> blocks, List<Bridge> bridge)
    {
    //    itemBlocks = blocks;
        bridges = bridge;
    }
        public bool Dead
        {
            get { return dead; }
        }

        public Vector2 Velocity
        {
            get { return velocity; }
        }

        public int Score
        {
            get { return score; }
            set { score = value; }
        }

        public int LivesRemaining
        {
            get { return livesRemaining; }
            set { livesRemaining = value; }
        }
        #endregion

        // Animation Strip Creation
        #region Constructor/Animation Initialization
        public Player(ContentManager content) : base()
        {
            PlayerTexture = content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Player");
            PlayerPlist = new Plist(@"Content\Textures\Super_Mario_Sprites\Player.plist");
        // Animations
            // Idle
            #region Idle
            animations.Add("m_small_idle",
                new AnimationStrip(PlayerTexture, PlayerPlist,"stb_m_small_idle",1));
            animations["m_small_idle"].LoopAnimation = true;

            animations.Add("m_big_idle",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_big_idle",
                    1));
            animations["m_big_idle"].LoopAnimation = true;

            animations.Add("l_small_idle",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_small_idle",
                    1));
            animations["l_small_idle"].LoopAnimation = true;

            animations.Add("l_big_idle",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_big_idle",
                    1));
            animations["l_big_idle"].LoopAnimation = true;

            animations.Add("m_fire_idle",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_f_big_idle",
                    1));
            animations["m_fire_idle"].LoopAnimation = true;

            animations.Add("s_big_idle",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_big_idle",
                    1));
            animations["s_big_idle"].LoopAnimation = true;

            animations.Add("s_small_idle",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_small_idle",
                    1));
            animations["s_small_idle"].LoopAnimation = true;
            #endregion

            // Idle Throw
            #region Idle Throw
            animations.Add("f_big_throw",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_f_big_walk",
                    1));
            animations["f_big_throw"].FrameLength = 1.0f;
            animations["f_big_throw"].LoopAnimation = true;

            animations.Add("s_big_throw",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_big_walk",
                    1));
            animations["s_big_throw"].FrameLength = 0.08f;
            #endregion

            // Jump
            #region Jump
            animations.Add("m_small_jump",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_small_jump",
                    1));
            animations["m_small_jump"].LoopAnimation = false;
            animations["m_small_jump"].FrameLength = 0.08f;

            animations.Add("m_big_jump",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_big_jump",
                    1));
            animations["m_big_jump"].LoopAnimation = false;
            animations["m_big_jump"].FrameLength = 0.08f;

            animations.Add("l_small_jump",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_small_jump",
                    1));
            animations["l_small_jump"].LoopAnimation = false;
            animations["l_small_jump"].FrameLength = 0.08f;

            animations.Add("l_big_jump",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_big_jump",
                    1));
            animations["l_big_jump"].LoopAnimation = false;
            animations["l_big_jump"].FrameLength = 0.08f;

            animations.Add("m_fire_jump",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_f_big_jump",
                    1));
            animations["m_fire_jump"].LoopAnimation = false;
            animations["m_fire_jump"].FrameLength = 0.08f;

            animations.Add("s_big_jump",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_big_jump",
                    1));
            animations["s_big_jump"].LoopAnimation = false;
            animations["s_big_jump"].FrameLength = 0.08f;

            animations.Add("s_small_jump",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_small_jump",
                    1));
            animations["s_small_jump"].LoopAnimation = false;
            animations["s_small_jump"].FrameLength = 0.08f;
            #endregion

            // Crouch (doesn't when small)
            #region Crouch
            animations.Add("m_big_crouch",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_big_crouch",
                    1));

            animations.Add("l_big_crouch",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_big_crouch",
                    1));

            animations.Add("m_fire_crouch",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_f_big_crouch",
                    1));

            animations.Add("s_big_crouch",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_big_crouch",
                    1));
            #endregion

            // Walk/Run
            #region Walk/Run
                // walk
            animations.Add("m_small_walk",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_small_walk",
                    3));
            animations["m_small_walk"].LoopAnimation = true;
            animations["m_small_walk"].FrameLength = 0.08f;

            animations.Add("m_big_walk",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_big_walk",
                    3));
            animations["m_big_walk"].LoopAnimation = true;
            animations["m_big_walk"].FrameLength = 0.08f;

            animations.Add("l_small_walk",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_small_walk",
                    3));
            animations["l_small_walk"].LoopAnimation = true;
            animations["l_small_walk"].FrameLength = 0.08f;

            animations.Add("l_big_walk",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_big_walk",
                    3));
            animations["l_big_walk"].LoopAnimation = true;
            animations["l_big_walk"].FrameLength = 0.08f;

            animations.Add("m_fire_walk",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_f_big_walk",
                    3));
            animations["m_fire_walk"].LoopAnimation = true;
            animations["m_fire_walk"].FrameLength = 0.08f;

            animations.Add("s_big_walk",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_big_walk",
                    3));
            animations["s_big_walk"].LoopAnimation = true;
            animations["s_big_walk"].FrameLength = 0.08f;

            animations.Add("s_small_walk",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_small_walk",
                    3));
            animations["s_small_walk"].LoopAnimation = true;
            animations["s_small_walk"].FrameLength = 0.08f;

                // run
            animations.Add("m_small_run",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_small_walk",
                    3));
            animations["m_small_run"].LoopAnimation = true;

            animations.Add("m_big_run",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_big_walk",
                    3));
            animations["m_big_run"].LoopAnimation = true;

            animations.Add("l_small_run",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_small_walk",
                    3));
            animations["l_small_run"].LoopAnimation = true;

            animations.Add("l_big_run",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_big_walk",
                    3));
            animations["l_big_run"].LoopAnimation = true;

            animations.Add("m_fire_run",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_f_big_walk",
                    3));
            animations["m_fire_run"].LoopAnimation = true;

            animations.Add("s_big_run",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_big_walk",
                    3));
            animations["s_big_run"].LoopAnimation = true;

            animations.Add("s_small_run",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_small_walk",
                    3));
            animations["s_small_run"].LoopAnimation = true;
            #endregion

            // Swimming
            #region Swimming
                // Swim
            animations.Add("m_small_swim",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_small_swim",
                    2));
            animations["m_small_swim"].LoopAnimation = true;
            animations["m_small_swim"].FrameLength = 0.08f;

            animations.Add("m_big_swim",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_big_swim",
                    2));
            animations["m_big_swim"].LoopAnimation = true;
            animations["m_big_swim"].FrameLength = 0.08f;

            animations.Add("l_small_swim",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_small_swim",
                    2));
            animations["l_small_swim"].LoopAnimation = true;
            animations["l_small_swim"].FrameLength = 0.08f;

            animations.Add("l_big_swim",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_big_swim",
                    2));
            animations["l_big_swim"].LoopAnimation = true;
            animations["l_big_swim"].FrameLength = 0.08f;

            animations.Add("m_fire_swim",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_f_big_swim",
                    2));
            animations["m_fire_swim"].LoopAnimation = true;
            animations["m_fire_swim"].FrameLength = 0.08f;

            animations.Add("s_big_swim",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_big_swim",
                    2));
            animations["s_big_swim"].LoopAnimation = true;
            animations["s_big_swim"].FrameLength = 0.08f;

            animations.Add("s_small_swim",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_small_swim",
                    2));
            animations["s_small_swim"].LoopAnimation = true;
            animations["s_small_swim"].FrameLength = 0.08f;

                // Swim Up
            animations.Add("m_small_swimUp",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_small_swim",
                    5));
            animations["m_small_swimUp"].LoopAnimation = true;
            animations["m_small_swimUp"].FrameLength = 0.08f;

            animations.Add("m_big_swimUp",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_big_swim",
                    5));
            animations["m_big_swimUp"].LoopAnimation = true;
            animations["m_big_swimUp"].FrameLength = 0.08f;

            animations.Add("l_small_swimUp",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_small_swim",
                    5));
            animations["l_small_swimUp"].LoopAnimation = true;
            animations["l_small_swimUp"].FrameLength = 0.08f;

            animations.Add("l_big_swimUp",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_big_swim",
                    5));
            animations["l_big_swimUp"].LoopAnimation = true;
            animations["l_big_swimUp"].FrameLength = 0.08f;

            animations.Add("m_fire_swimUp",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_f_big_swim",
                    5));
            animations["m_fire_swimUp"].LoopAnimation = true;
            animations["m_fire_swimUp"].FrameLength = 0.08f;

            animations.Add("s_big_swimUp",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_big_swim",
                    5));
            animations["s_big_swimUp"].LoopAnimation = true;
            animations["s_big_swimUp"].FrameLength = 0.08f;

            animations.Add("s_small_swimUp",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_small_swim",
                    5));
            animations["s_small_swimUp"].LoopAnimation = true;
            animations["s_small_swimUp"].FrameLength = 0.08f;
            #endregion

            // Die (only when small)
            #region Die
            animations.Add("mario_die",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_small_die",
                    1));
            animations["mario_die"].LoopAnimation = false;

            animations.Add("luigi_die",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_small_die",
                    1));
            animations["luigi_die"].LoopAnimation = false;
            #endregion

            // Climbing
            #region Climbing
                // Climbing Idle
            animations.Add("m_small_climb_idle",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_small_climb",
                    1));
            animations["m_small_climb_idle"].LoopAnimation = true;

            animations.Add("m_big_climb_idle",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_big_climb",
                    1));
            animations["m_big_climb_idle"].LoopAnimation = true;

            animations.Add("l_small_climb_idle",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_small_climb",
                    1));
            animations["l_small_climb_idle"].LoopAnimation = true;

            animations.Add("l_big_climb_idle",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_big_climb",
                    1));
            animations["l_big_climb_idle"].LoopAnimation = true;

            animations.Add("f_big_climb_idle",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_f_big_climb",
                    1));
            animations["f_big_climb_idle"].LoopAnimation = true;

            animations.Add("s_small_climb_idle",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_small_climb",
                    1));
            animations["s_small_climb_idle"].LoopAnimation = true;

            animations.Add("s_big_climb_idle",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_big_climb",
                    1));
            animations["s_big_climb_idle"].LoopAnimation = true;

                // Climb Up
            animations.Add("m_small_climb",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_small_climb",
                    2));
            animations["m_small_climb"].LoopAnimation = true;
            animations["m_small_climb"].FrameLength = .08f;

            animations.Add("m_big_climb",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_m_big_climb",
                    2));
            animations["m_big_climb"].LoopAnimation = true;
            animations["m_big_climb"].FrameLength = .08f;

            animations.Add("l_small_climb",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_small_climb",
                    2));
            animations["l_small_climb"].LoopAnimation = true;
            animations["l_small_climb"].FrameLength = .08f;

            animations.Add("l_big_climb",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_l_big_climb",
                    2));
            animations["l_big_climb"].LoopAnimation = true;
            animations["l_big_climb"].FrameLength = .08f;

            animations.Add("f_big_climb",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_f_big_climb",
                    2));
            animations["f_big_climb"].LoopAnimation = true;
            animations["f_big_climb"].FrameLength = .08f;

            animations.Add("s_small_climb",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_small_climb",
                    2));
            animations["s_small_climb"].LoopAnimation = true;
            animations["s_small_climb"].FrameLength = .08f;

            animations.Add("s_big_climb",
                new AnimationStrip(
                    PlayerTexture,
                    PlayerPlist,
                    "stb_s_big_climb",
                    2));
            animations["s_big_climb"].LoopAnimation = true;
            animations["s_big_climb"].FrameLength = .08f;
            #endregion

            currentAnimation = "m_small_idle";

            frameWidth = animations[currentAnimation].FrameWidth;
            frameHeight = animations[currentAnimation].FrameHeight;

            drawDepth = 0.825f;

            enabled = true;
            codeBasedBlocks = false;
            PlayAnimation("m_small_idle");
            
        }
        #endregion
        
        // States & Actions
        #region Public Methods
        public override void Update(GameTime gameTime)
        {
            float elapsed = gameTime.TotalGameTime.Milliseconds;
            extendedRectangle = new Rectangle((int)WorldLocation.X - 10, (int)WorldLocation.Y - 12, CollisionRectangle.Width + 20, CollisionRectangle.Height + 30);

            LevelManager.Update(gameTime);
            if (Coins >= 100)
            {
                Coins -= 100;
                livesRemaining++;
                SoundEngine.Play("OneUp");
            }
            if (!Dead)
            {
                if (isSmall)
                {
                    CanBreak = false;
                    canBump = true;
                }
                else
                {
                    CanBreak = true;
                    canBump = false;
                }

                // Growing
                #region Growing
                    // Mario Grow
                    #region Mario
                if (isGrowing == true && isLuigi == false)
                {
                    LevelManager.worldPaused = true;
                    canMove = false;
                    if (Sound_Engine.SoundEngine.getInstance("GetPowerUp").State != SoundState.Playing)
                    {
                        Sound_Engine.SoundEngine.Play("GetPowerUp");
                    }
                    if (stateChangeCount == 0)
                    {
                        if (stateChangeTimer == 0)
                        {
                            isBig = true;
                            isSmall = false;
                            PlayAnimation("m_big_idle");
                            BumpBig();
                        }
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = false;
                            isSmall = true;
                            PlayAnimation("m_small_idle");
                            BumpSmall();
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }
                    else if (stateChangeCount == 1 || stateChangeCount == 4 || stateChangeCount == 7 || stateChangeCount == 10)
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = true;
                            isSmall = false;
                            PlayAnimation("m_big_crouch");
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }
                    else if (stateChangeCount == 2 || stateChangeCount == 5 || stateChangeCount == 8)
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = true;
                            isSmall = false;
                            PlayAnimation("m_big_idle");
                            BumpBig();
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }
                    else if (stateChangeCount == 3 || stateChangeCount == 6 || stateChangeCount == 9)
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = false;
                            isSmall = true;
                            PlayAnimation("m_small_idle");
                            BumpSmall();
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }                    
                    else
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            stateChangeTimer = 0;
                            stateChangeCount = 0;
                            canMove = true;
                            LevelManager.worldPaused = false;
                            isGrowing = false;
                            hasGrown = true;
                            Big();
                        }
                    }
                }
                    #endregion
                    // Luigi
                    #region Luigi
                if (isGrowing == true && isLuigi == true)
                {
                    LevelManager.worldPaused = true;
                    canMove = false;
                    if (Sound_Engine.SoundEngine.getInstance("GetPowerUp").State != SoundState.Playing)
                    {
                        Sound_Engine.SoundEngine.Play("GetPowerUp");
                    }
                    if (stateChangeCount == 0)
                    {
                        if (stateChangeTimer == 0)
                        {
                            isBig = true;
                            isSmall = false;
                            PlayAnimation("l_big_idle");
                            BumpBig();
                        }
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = false;
                            isSmall = true;
                            PlayAnimation("l_small_idle");
                            BumpSmall();
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }
                    else if (stateChangeCount == 1 || stateChangeCount == 4 || stateChangeCount == 7 || stateChangeCount == 10)
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = true;
                            isSmall = false;
                            PlayAnimation("l_big_crouch");
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }
                    else if (stateChangeCount == 2 || stateChangeCount == 5 || stateChangeCount == 8)
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = true;
                            isSmall = false;
                            PlayAnimation("l_big_idle");
                            BumpBig();
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }
                    else if (stateChangeCount == 3 || stateChangeCount == 6 || stateChangeCount == 9)
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = false;
                            isSmall = true;
                            PlayAnimation("l_small_idle");
                            BumpSmall();
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }
                    else
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            stateChangeTimer = 0;
                            stateChangeCount = 0;
                            canMove = true;
                            LevelManager.worldPaused = false;
                            isGrowing = false;
                            hasGrown = true;
                            Big();
                        }
                    }
                }
                    #endregion
                #endregion

                // Shrinking
                #region Shrinking
                    // Mario Grow
                    #region Mario
                if (isShrinking == true && isLuigi == false)
                {
                    LevelManager.worldPaused = true;
                    canMove = false;
                    if (Sound_Engine.SoundEngine.getInstance("Pipe").State != SoundState.Playing)
                    {
                        Sound_Engine.SoundEngine.Play("Pipe");
                    }
                    if (stateChangeCount == 0)
                    {
                        if (stateChangeTimer == 0)
                        {
                            isBig = false;
                            isSmall = true;
                            PlayAnimation("m_small_idle");
                            BumpSmall();
                        }
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = true;
                            isSmall = false;
                            PlayAnimation("m_big_idle");
                            BumpBig();
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }
                    else if (stateChangeCount == 1 || stateChangeCount == 4 || stateChangeCount == 7 || stateChangeCount == 10)
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = false;
                            isSmall = true;
                            PlayAnimation("m_big_crouch");
                            BumpSmall();
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }
                    else if (stateChangeCount == 2 || stateChangeCount == 5 || stateChangeCount == 8)
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = false;
                            isSmall = true;
                            PlayAnimation("m_small_idle");
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }
                    else if (stateChangeCount == 3 || stateChangeCount == 6 || stateChangeCount == 9)
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = true;
                            isSmall = false;
                            PlayAnimation("m_big_idle");
                            BumpBig();
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }
                    else
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            stateChangeTimer = 0;
                            stateChangeCount = 0;
                            canMove = true;
                            LevelManager.worldPaused = false;
                            isShrinking = false;
                            hasShrunk = true;
                            Small();
                        }
                    }
                }
                    #endregion
                    // Luigi
                    #region Luigi
                if (isShrinking == true && isLuigi == false)
                {
                    LevelManager.worldPaused = true;
                    canMove = false;
                    if (Sound_Engine.SoundEngine.getInstance("Pipe").State != SoundState.Playing)
                    {
                        Sound_Engine.SoundEngine.Play("Pipe");
                    }
                    if (stateChangeCount == 0)
                    {
                        if (stateChangeTimer == 0)
                        {
                            isBig = false;
                            isSmall = true;
                            PlayAnimation("l_small_idle");
                            BumpSmall();
                        }
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = true;
                            isSmall = false;
                            PlayAnimation("l_big_idle");
                            BumpBig();
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }
                    else if (stateChangeCount == 1 || stateChangeCount == 4 || stateChangeCount == 7 || stateChangeCount == 10)
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = false;
                            isSmall = true;
                            PlayAnimation("l_big_crouch");
                            BumpSmall();
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }
                    else if (stateChangeCount == 2 || stateChangeCount == 5 || stateChangeCount == 8)
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = false;
                            isSmall = true;
                            PlayAnimation("l_small_idle");
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }
                    else if (stateChangeCount == 3 || stateChangeCount == 6 || stateChangeCount == 9)
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            isBig = true;
                            isSmall = false;
                            PlayAnimation("l_big_idle");
                            BumpBig();
                            stateChangeTimer = 0;
                            stateChangeCount++;
                        }
                    }
                    else
                    {
                        if (stateChangeTimer < stateChangeDelay)
                        {
                            stateChangeTimer += elapsed;
                        }
                        else
                        {
                            stateChangeTimer = 0;
                            stateChangeCount = 0;
                            canMove = true;
                            LevelManager.worldPaused = false;
                            isShrinking = false;
                            hasShrunk = true;
                            Small();
                        }
                    }
                }
                    #endregion
                #endregion

                // PowerUp

                if (canMove)
                {
                    string newAnimation = "m_small_idle";
                    stateChanged = false;
                    crouchLocation = this.WorldLocation;
  

                    if (newAnimation != currentAnimation)
                    {
                        resetCollisionRectangle();
                    }

                    // Player Controls
                    velocity = new Vector2(0, velocity.Y);
                    GamePadState gamePad = GamePad.GetState(PlayerIndex.One);
                    KeyboardState keyState = Keyboard.GetState();
                    // Player State Manual Triggers (until power-ups are added)
                    #region Player State Triggers
                    // Mario/Luigi
                    #region Mario/Luigi
                    if (lbPressable == true &&
                        (keyState.IsKeyDown(Keys.E) ||
                        (gamePad.Buttons.LeftShoulder == ButtonState.Pressed)))
                    {
                        if(!isLuigi)
                        {
                            isLuigi = true;
                        }
                        else
                        {
                            isLuigi = false;
                        }
                        lbPressable = false;
                    }
                    if (keyState.IsKeyUp(Keys.E) &&
                        (gamePad.Buttons.LeftShoulder == ButtonState.Released))
                    {
                        lbPressable = true;
                    }
                    #endregion

                    // Big/Small
                    #region Big/Small Mario
                    if (yPressable == true && (isSmall == true || isBig == true))
                    {
                        if (keyState.IsKeyDown(Keys.Y) ||
                            (gamePad.Buttons.Y == ButtonState.Pressed))
                        {
                            stateChanged = true;
                            if (!isBig)
                            {
                                Big();
                            }
                            else
                            {
                                Small();                                
                            }
                            yPressable = false;
                        }
                    }
                    if (keyState.IsKeyUp(Keys.Y) &&
                        (gamePad.Buttons.Y == ButtonState.Released))
                    {
                        yPressable = true;
                    }
                    #endregion

                    // Fire/Small
                    #region Fire/Small
                    if (xPressable == true && (isBig == true || isFire == true))
                    {
                        if (keyState.IsKeyDown(Keys.X) ||
                            (gamePad.Buttons.X == ButtonState.Pressed))
                        {
                            stateChanged = true;
                            if (!isFire)
                            {
                                Fire();
                            }
                            else
                            {
                                Small();

                            }
                            xPressable = false;
                        }
                    }
                    if (keyState.IsKeyUp(Keys.X) &&
                            (gamePad.Buttons.X == ButtonState.Released))
                    {
                        xPressable = true;
                    }
                    #endregion

                    // Star Big/Small
                    #region Star Big/Small
                    if (rbPressable == true)
                    {
                        if (keyState.IsKeyDown(Keys.R) ||
                            (gamePad.Buttons.RightShoulder == ButtonState.Pressed))
                        {
                            if (isStarBig == false && isStarSmall == false)
                            {
                                // store if state was fire or big
                                if (isBig == true)
                                {
                                    wasState = "Big";
                                }
                                else if (isFire == true)
                                {
                                    wasState = "Fire";
                                }
                                else
                                {
                                    wasState = "Small";
                                }

                                if (isSmall == true)
                                {
                                    StarSmall();
                                }
                                else
                                {
                                    StarBig();
                                }
                            }
                            else
                            {
                                if (wasState == "Big")
                                {
                                    Big();
                                }
                                else if (wasState == "Fire")
                                {
                                    Fire();
                                }
                                else
                                {
                                    Small();
                                }
                                wasState = null;
                            }
                            rbPressable = false;
                        }
                    }
                    if (keyState.IsKeyUp(Keys.R) &&
                        (gamePad.Buttons.RightShoulder == ButtonState.Released))
                    {
                        rbPressable = true;
                    }
                    #endregion

                    // Underwater
                    #region Underwater
                    if (rjPressable == true)
                    {
                        if (keyState.IsKeyDown(Keys.T) ||
                            (gamePad.Buttons.RightStick == ButtonState.Pressed))
                        {
                            if (underwater == false)
                            {
                                underwater = true;
                            }
                            else
                            {
                                underwater = false;
                            }
                            rjPressable = false;
                        }
                    }
                    if (keyState.IsKeyUp(Keys.T) &&
                        (gamePad.Buttons.RightStick == ButtonState.Released))
                    {
                        rjPressable = true;
                    }
                    #endregion
                    #endregion

                    // Idle/Swimming
                    #region Idle/Swimming State
                    if (underwater == true && !onGround && isSmall == true && !isLuigi && velocity.Y >= 0)
                    {
                        newAnimation = "m_small_swim";
                    }
                    else if (underwater == true && !onGround && isSmall == true && !isLuigi && velocity.Y < 0)
                    {
                        newAnimation = "m_small_swimUp";
                    }
                    else if (underwater == true && !onGround && isBig == true && !isLuigi && velocity.Y >= 0)
                    {
                        newAnimation = "m_big_swim";
                    }
                    else if (underwater == true && !onGround && isBig == true && !isLuigi && velocity.Y < 0)
                    {
                        newAnimation = "m_big_swimUp";
                    }
                    else if (underwater == true && !onGround && isSmall == true && isLuigi == true && velocity.Y >= 0)
                    {
                        newAnimation = "l_small_swim";
                    }
                    else if (underwater == true && !onGround && isSmall == true && isLuigi == true && velocity.Y < 0)
                    {
                        newAnimation = "l_small_swimUp";
                    }
                    else if (underwater == true && !onGround && isBig == true && isLuigi == true && velocity.Y >= 0)
                    {
                        newAnimation = "l_big_swim";
                    }
                    else if (underwater == true && !onGround && isBig == true && isLuigi == true && velocity.Y < 0)
                    {
                        newAnimation = "l_big_swimUp";
                    }
                    else if (underwater == true && onGround == false && isFire == true && velocity.Y >= 0)
                    {
                        newAnimation = "m_fire_swim";
                    }
                    else if (underwater == true && onGround == false && isFire == true && velocity.Y < 0)
                    {
                        newAnimation = "m_fire_swimUp";
                    }
                    else if (underwater == true && onGround == false && isStarBig == true && velocity.Y >= 0)
                    {
                        newAnimation = "s_big_swim";
                    }
                    else if (underwater == true && onGround == false && isStarBig == true && velocity.Y < 0)
                    {
                        newAnimation = "s_big_swimUp";
                    }
                    else if (underwater == true && onGround == false && isStarSmall == true && velocity.Y >= 0)
                    {
                        newAnimation = "s_small_swim";
                    }
                    else if (underwater == true && onGround == false && isStarSmall == true && velocity.Y < 0)
                    {
                        newAnimation = "s_small_swimUp";
                    }
                    else if (isStarBig == true)
                    {
                        newAnimation = "s_big_idle";
                    }
                    else if (isStarSmall == true)
                    {
                        newAnimation = "s_small_idle";
                    }
                    else if (isFire == true)
                    {
                        newAnimation = "m_fire_idle";
                    }
                    else if (isBig == true && !isLuigi)
                    {
                        newAnimation = "m_big_idle";
                    }
                    else if (isSmall == true && !isLuigi)
                    {
                        newAnimation = "m_small_idle";
                    }
                    else if (isBig == true && isLuigi == true)
                    {
                        newAnimation = "l_big_idle";
                    }
                    else
                    {
                        newAnimation = "l_small_idle";
                    }
                    #endregion

                    // Spawn fireballs
                    #region Fireballs
                    if ((isFire || wasState == "Fire") &&
                        bPressable &&
                        fireballCount < fireballMax &&
                        (keyState.IsKeyDown(Keys.LeftShift) ||
                        (gamePad.Buttons.B == ButtonState.Pressed)) &&
                        !isClimbing && !isCrouching)
                    {
                        if (keyState.IsKeyUp(Keys.W) && keyState.IsKeyUp(Keys.A) &&
                            keyState.IsKeyUp(Keys.S) && keyState.IsKeyUp(Keys.D) &&
                            gamePad.DPad.Up == ButtonState.Released && gamePad.DPad.Left == ButtonState.Released &&
                            gamePad.DPad.Down == ButtonState.Released && gamePad.DPad.Right == ButtonState.Released)
                        {
                            if (isFire == true)
                            {
                                if (throwTimer >= throwDelay)
                                {
                                    newAnimation = "f_big_throw";
                                    throwTimer += elapsed;
                                }
                                else
                                {
                                    newAnimation = "f_big_throw";
                                }
                            }
                            else
                            {
                                newAnimation = "s_big_throw";
                            }
                        }
                        LevelManager.newFireball();
                        bPressable = false;
                        fireballCount++;
                    }

                    if (keyState.IsKeyUp(Keys.LeftShift) &&
                        (gamePad.Buttons.B == ButtonState.Released))
                    {
                        bPressable = true;
                    }
                    #endregion

                    // Spawn bubbles
                    #region Bubbles
                    if (underwater == true)
                    {
                        if (rand.Next(0, 50) == 0)
                        {
                            LevelManager.newBubble();
                        }
                    }
                    #endregion

                    // Starmode Flash
                    #region Star Flash
                    if (isStarBig == true || isStarSmall == true)
                    {
                        if (starTimer < starDelay)
                        {
                            starTimer += elapsed;
                        }
                        else
                        {
                            if (wasState == "Big")
                            {
                                Big();
                            }
                            else if (wasState == "Fire")
                            {
                                Fire();
                            }
                            else
                            {
                                Small();
                            }
                            wasState = null;
                            starTimer = 0;
                        }
                    }

                    if (isStarBig == true || isStarSmall == true)
                    {
                        paintColor = (myColor)((gameTime.TotalGameTime.Milliseconds / 100) % 3);
                    }
                    else
                    {
                        paintColor = myColor.white;
                    }
                    #endregion

                    // Invulnerable Flash
                    #region Invulnerable Flash
                    if (isInvulnerable == true)
                    {
                        if (invulnTimer < invulnDelay)
                        {
                            invulnTimer += elapsed;
                        }
                        else
                        {
                            Small();
                            invulnTimer = 0;
                        }
                    }

                    if (ljPressable == true &&
                        (keyState.IsKeyDown(Keys.V) ||
                        gamePad.Buttons.LeftStick == ButtonState.Pressed))
                    {
                        if (isInvulnerable == false)
                        {
                            Invulnerable();
                        }
                        else
                        {
                            Small();
                        }
                        ljPressable = false;
                    }

                    if (keyState.IsKeyUp(Keys.V) &&
                        gamePad.Buttons.LeftStick == ButtonState.Released)
                    {
                        ljPressable = true;
                    }

                    if (isInvulnerable == true)
                    {
                        if ((gameTime.TotalGameTime.Milliseconds / 250) % 2 == 0)
                        {
                            visible = false;
                        }
                        else
                        {
                            visible = true;
                        }
                    }
                    else
                    {
                        visible = true;
                    }
                    #endregion
                                      
                    // Walk/Run
                    #region Walk/Run States
                    // Left Walk
                    if ((keyState.IsKeyDown(Keys.A) ||
                        (gamePad.DPad.Left == ButtonState.Pressed)) &&
                        !isClimbing && !isCrouching)
                    {
                        flipped = true;
                        isPlayerFlipped = true;
                        if (underwater == false || onGround == true)
                        {
                            if (isStarBig == true)
                            {
                                newAnimation = "s_big_walk";
                            }
                            else if (isStarSmall == true)
                            {
                                newAnimation = "s_small_walk";
                            }
                            else if (isFire == true)
                            {
                                newAnimation = "m_fire_walk";
                            }
                            else if (isBig == true && !isLuigi)
                            {
                                newAnimation = "m_big_walk";
                            }
                            else if (isSmall == true && !isLuigi)
                            {
                                newAnimation = "m_small_walk";
                            }
                            else if (isBig == true && isLuigi == true)
                            {
                                newAnimation = "l_big_walk";
                            }
                            else
                            {
                                newAnimation = "l_small_walk";
                            }
                        }

                        // walk acceleration
                        if (moveScaleWalk < walkspeed && underwater == false)
                        {
                            moveScaleWalk += acceleration;
                        }

                        if (moveScaleWalk > walkspeed && underwater == false &&
                            keyState.IsKeyUp(Keys.LeftShift) &&
                            gamePad.Buttons.B == ButtonState.Released)
                        {
                            moveScaleWalk -= deceleration;
                        }

                        // swim acceleration
                        if (moveScaleSwim < swimspeed && underwater == true)
                        {
                            moveScaleSwim += acceleration;
                        }

                        if (!underwater)
                        {
                            velocity = new Vector2(-moveScaleWalk, velocity.Y);
                        }
                        else
                        {
                            velocity = new Vector2(-moveScaleSwim, velocity.Y);
                        }
                    }

                    // Right Walk
                    if (keyState.IsKeyDown(Keys.D) ||
                        (gamePad.DPad.Right == ButtonState.Pressed) &&
                        !isClimbing && !isCrouching)
                    {
                        flipped = false;
                        isPlayerFlipped = false;
                        if (underwater == false || onGround == true)
                        {
                            if (isStarBig == true)
                            {
                                newAnimation = "s_big_walk";
                            }
                            else if (isStarSmall == true)
                            {
                                newAnimation = "s_small_walk";
                            }
                            else if (isFire == true)
                            {
                                newAnimation = "m_fire_walk";
                            }
                            else if (isBig == true && !isLuigi)
                            {
                                newAnimation = "m_big_walk";
                            }
                            else if (isSmall == true && !isLuigi)
                            {
                                newAnimation = "m_small_walk";
                            }
                            else if (isBig == true && isLuigi == true)
                            {
                                newAnimation = "l_big_walk";
                            }
                            else
                            {
                                newAnimation = "l_small_walk";
                            }
                        }

                        // walk acceleration
                        if (moveScaleWalk < walkspeed && underwater == false)
                        {
                            moveScaleWalk += acceleration;
                        }

                        if (moveScaleWalk > walkspeed && underwater == false &&
                            keyState.IsKeyUp(Keys.LeftShift) &&
                            gamePad.Buttons.B == ButtonState.Released)
                        {
                            moveScaleWalk -= deceleration;
                        }

                        // swim acceleration
                        if (moveScaleSwim < swimspeed && underwater == true)
                        {
                            moveScaleSwim += acceleration;
                        }

                        if (!underwater)
                        {
                            velocity = new Vector2(moveScaleWalk, velocity.Y);
                        }
                        else
                        {
                            velocity = new Vector2(moveScaleSwim, velocity.Y);
                        }
                    }

                    // Left Run
                    if ((keyState.IsKeyDown(Keys.A) && keyState.IsKeyDown(Keys.LeftShift) ||
                        (gamePad.DPad.Left == ButtonState.Pressed && gamePad.Buttons.B == ButtonState.Pressed)) &&
                        !isClimbing && !underwater)
                    {
                        flipped = true;
                        isPlayerFlipped = true;
                        if (isStarBig == true)
                        {
                            newAnimation = "s_big_run";
                        }
                        else if (isStarSmall == true)
                        {
                            newAnimation = "s_small_run";
                        }
                        else if (isFire == true)
                        {
                            newAnimation = "m_fire_run";
                        }
                        else if (isBig == true && !isLuigi)
                        {
                            newAnimation = "m_big_run";
                        }
                        else if (isSmall == true && !isLuigi)
                        {
                            newAnimation = "m_small_run";
                        }
                        else if (isBig == true && isLuigi == true)
                        {
                            newAnimation = "l_big_run";
                        }
                        else
                        {
                            newAnimation = "l_small_run";
                        }

                        // run acceleration
                        if (moveScaleWalk < runspeed)
                        {
                            moveScaleWalk += acceleration;
                        }

                        velocity = new Vector2(-moveScaleWalk, velocity.Y);
                    }

                    // Right Run
                    if ((keyState.IsKeyDown(Keys.D) && keyState.IsKeyDown(Keys.LeftShift) ||
                        (gamePad.DPad.Right == ButtonState.Pressed && gamePad.Buttons.B == ButtonState.Pressed)) &&
                        !isClimbing && !underwater)
                    {
                        flipped = false;
                        isPlayerFlipped = false;
                        if (isStarBig == true)
                        {
                            newAnimation = "s_big_run";
                        }
                        else if (isStarSmall == true)
                        {
                            newAnimation = "s_small_run";
                        }
                        else if (isFire == true)
                        {
                            newAnimation = "m_fire_run";
                        }
                        else if (isBig == true && !isLuigi)
                        {
                            newAnimation = "m_big_run";
                        }
                        else if (isSmall == true && !isLuigi)
                        {
                            newAnimation = "m_small_run";
                        }
                        else if (isBig == true && isLuigi == true)
                        {
                            newAnimation = "l_big_run";
                        }
                        else
                        {
                            newAnimation = "l_small_run";
                        }

                        // run acceleration
                        if (moveScaleWalk < runspeed)
                        {
                            moveScaleWalk += acceleration;
                        }

                        velocity = new Vector2(moveScaleWalk, velocity.Y);
                    }

                    // Reset moveScaleWalk
                    if (keyState.IsKeyUp(Keys.A) && keyState.IsKeyUp(Keys.D) &&
                        gamePad.DPad.Left == ButtonState.Released && gamePad.DPad.Right == ButtonState.Released
                        && moveScaleWalk != 0 && isCrouching == false)
                    {
                        acceleration = 0.0f;
                        deceleration = 10.0f;
                        if (moveScaleWalk > 0.0f)
                        {
                            moveScaleWalk -= deceleration;
                        }
                    }
                    #endregion

                    // Jump
                    #region Jump State
                    if (underwater == false &&
                        (keyState.IsKeyDown(Keys.Space) ||
                        (gamePad.Buttons.A == ButtonState.Pressed)) &&
                        isClimbing == false &&
                        isCrouching == false)
                    {
                        if (onGround)
                        {
                            Jump(true);

                            if (isStarBig == true)
                            {
                                newAnimation = "s_big_jump";
                            }
                            else if (isStarSmall == true)
                            {
                                newAnimation = "s_small_jump";
                            }
                            else if (isFire == true)
                            {
                                newAnimation = "m_fire_jump";
                            }
                            else if (isBig == true && !isLuigi)
                            {
                                newAnimation = "m_big_jump";
                            }
                            else if (isSmall == true && !isLuigi)
                            {
                                newAnimation = "m_small_jump";
                            }
                            else if (isBig == true && isLuigi == true)
                            {
                                newAnimation = "l_big_jump";
                            }
                            else
                            {
                                newAnimation = "l_small_jump";
                            }
                        }
                    }


                    //Added code to make player have variable jump height
                    if (!keyState.IsKeyDown(Keys.Space))
                    {
                        if (previous.IsKeyDown(Keys.Space) && hasJumped == true)
                        {
                            if (velocity.Y < 0)
                            {
                                velocity = new Vector2(velocity.X, velocity.Y / 2);
                            }
                        }
                    }

                    if (onGround && !keyState.IsKeyDown(Keys.Space) && hasJumped == true)
                    {
                        hasJumped = false;
                    }
                    
                    #endregion

                    // Swim Up
                    #region Swim Up State
                    if (underwater == true &&
                        aPressable == true &&
                        (keyState.IsKeyDown(Keys.Space) ||
                        (gamePad.Buttons.A == ButtonState.Pressed)))
                    {
                        Swim();
                        aPressable = false;
                    }

                    // Release A button
                    if (keyState.IsKeyUp(Keys.Space) &&
                        (gamePad.Buttons.A == ButtonState.Released))
                    {
                        aPressable = true;
                    }
                    #endregion

                    // Crouch
                    #region Crouch/Slide States
                    if (isBig == true || isFire == true || isStarBig == true)
                    {
                        if (keyState.IsKeyUp(Keys.LeftShift) ||
                            gamePad.Buttons.B == ButtonState.Released)
                        {
                            if (keyState.IsKeyDown(Keys.S) ||
                            gamePad.DPad.Down == ButtonState.Pressed)
                            {
                                if (isStarBig == true)
                                {
                                    newAnimation = "s_big_crouch";
                                }
                                else if (isFire == true)
                                {
                                    newAnimation = "m_fire_crouch";
                                }
                                else if (isLuigi == true)
                                {
                                    newAnimation = "l_big_crouch";
                                }
                                else
                                {
                                    newAnimation = "m_big_crouch";
                                }

                                PlayAnimation("m_big_crouch");
                                if (onGround && !hasBumped)
                                {
                                    BumpSmall();
                                    hasBumped = true;
                                }
                                isCrouching = true;
                            }
                        }
                    }

                    // Release Crouch
                    if (isBig == true || isFire == true || isStarBig == true)
                    {
                        if (keyState.IsKeyUp(Keys.S) &&
                            gamePad.IsButtonUp(Buttons.DPadDown)
                            && isCrouching == true)
                        {
                            if (isStarBig == true)
                            {
                                newAnimation = "s_big_idle";
                            }
                            else if (isFire == true)
                            {
                                newAnimation = "m_fire_idle";
                            }
                            else if (isLuigi == true)
                            {
                                newAnimation = "l_big_idle";
                            }
                            else
                            {
                                newAnimation = "m_big_idle";
                            }
                            
                            PlayAnimation("m_big_idle");
                            if (onGround)
                            {
                                BumpSmall();
                                hasBumped = false;
                            }
                            isCrouching = false;
                        }
                    }

                    // Slide
                    if (underwater == false && (isBig == true || isFire == true || isStarBig == true))
                    {
                        if (keyState.IsKeyDown(Keys.LeftShift) ||
                            gamePad.Buttons.B == ButtonState.Pressed)
                        {
                            if (keyState.IsKeyDown(Keys.S) ||
                                gamePad.DPad.Down == ButtonState.Pressed)
                            {
                                if (isStarBig == true)
                                {
                                    newAnimation = "s_big_crouch";
                                }
                                else if (isFire == true)
                                {
                                    newAnimation = "m_fire_crouch";
                                }
                                else if (isLuigi == true)
                                {
                                    newAnimation = "l_big_crouch";
                                }
                                else
                                {
                                    newAnimation = "m_big_crouch";
                                }
                                isCrouching = true;
                            }
                        }
                    }
                    #endregion

                    // Accel/Decel
                    #region Accel/Decel
                    // Deceleration
                    if (isCrouching == true)
                    {
                        acceleration = 0.0f;
                        if (moveScaleWalk > 0.0f)
                        {
                            moveScaleWalk -= deceleration;
                        }
                    }

                    // Acceleration
                    if (isCrouching == false)
                    {
                        acceleration = 10.0f;
                        deceleration = 10.0f;
                    }

                    // Negative Movement Speed Check
                    if (moveScaleWalk < 0)
                    {
                        moveScaleWalk = 0;
                    }
                    #endregion

                    // Climb
                    #region Climb State
                    if (isClimbing)
                    {
                        isVerticallyCollidable = false;
                        if (isStarBig)
                        {
                            newAnimation = "s_big_climb_idle";
                        }
                        else if (isStarSmall)
                        {
                            newAnimation = "s_small_climb_idle";
                        }
                        else if (isFire)
                        {
                            newAnimation = "f_big_climb_idle";
                        }
                        else if (isBig && !isLuigi)
                        {
                            newAnimation = "m_big_climb_idle";
                        }
                        else if (isSmall && !isLuigi)
                        {
                            newAnimation = "m_small_climb_idle";
                        }
                        else if (isBig && isLuigi)
                        {
                            newAnimation = "l_big_climb_idle";
                        }
                        else
                        {
                            newAnimation = "l_small_climb_idle";
                        }

                        velocity = new Vector2(0, 0);

                        if (keyState.IsKeyDown(Keys.S) ||
                            gamePad.DPad.Down == ButtonState.Pressed ||
                            keyState.IsKeyDown(Keys.W) ||
                            gamePad.DPad.Up == ButtonState.Pressed)
                        {
                            if (isStarBig)
                            {
                                newAnimation = "s_big_climb";
                            }
                            else if (isStarSmall)
                            {
                                newAnimation = "s_small_climb";
                            }
                            else if (isFire)
                            {
                                newAnimation = "f_big_climb";
                            }
                            else if (isBig && !isLuigi)
                            {
                                newAnimation = "m_big_climb";
                            }
                            else if (isSmall && !isLuigi)
                            {
                                newAnimation = "m_small_climb";
                            }
                            else if (isBig && isLuigi)
                            {
                                newAnimation = "l_big_climb";
                            }
                            else
                            {
                                newAnimation = "l_small_climb";
                            }

                            if (keyState.IsKeyDown(Keys.S) ||
                            gamePad.DPad.Down == ButtonState.Pressed)
                            {
                                velocity = new Vector2(0, 100);
                            }
                            if (keyState.IsKeyDown(Keys.W) ||
                            gamePad.DPad.Up == ButtonState.Pressed)
                            {
                                velocity = new Vector2(0, -100);
                            }

                        }
                    }
                    #endregion

                    // Update Jump
                    #region Update Jump
                    if (currentAnimation == "m_small_jump" && onGround == false && isClimbing == false)
                    {
                        newAnimation = "m_small_jump";
                    }

                    if (currentAnimation == "m_big_jump" && onGround == false && isClimbing == false)
                    {
                        newAnimation = "m_big_jump";
                    }

                    if (currentAnimation == "m_fire_jump" && onGround == false)
                    {
                        newAnimation = "m_fire_jump";
                    }

                    if (currentAnimation == "s_big_jump" && onGround == false)
                    {
                        newAnimation = "s_big_jump";
                    }

                    if (currentAnimation == "s_small_jump" && onGround == false)
                    {
                        newAnimation = "s_small_jump";
                    }

                    if (currentAnimation == "l_big_jump" && onGround == false)
                    {
                        newAnimation = "l_big_jump";
                    }

                    if (currentAnimation == "l_small_jump" && onGround == false)
                    {
                        newAnimation = "l_small_jump";
                    }
                    #endregion

                    // Update Animation
                    #region Update Animation
                    if (newAnimation != currentAnimation)
                    {
                        resetCollisionRectangle();
                        PlayAnimation(newAnimation);
                        resetCollisionRectangle();
                    }
                    #endregion

                    //logic for interaction with itemblocks
                    //affects falling code
                    //check to ensure does not interfere with other code

                    int sidelength;
                    if (isSmall == false)
                    {
                        sidelength = 96;
                    }
                    else
                    {
                        sidelength = 44;
                    }
                    KeyboardState currentState = Keyboard.GetState();
                    bool canFall = true;

                    Rectangle afterFallRect;

                    //Rectangle afterJumpRect;
                    Rectangle afterHorizontalRect;

                    if (currentState.IsKeyDown(Keys.A))
                    {
                        afterHorizontalRect = new Rectangle((int)(worldLocation.X - 10), (int)worldLocation.Y, 58, sidelength);
                    }
                    else if (currentState.IsKeyDown(Keys.D))
                    {
                        afterHorizontalRect = new Rectangle((int)worldLocation.X, (int)worldLocation.Y, 58, sidelength);
                    }
                    else
                    {
                        afterHorizontalRect = collisionRectangle;
                    }
                    /*
                    if (velocity.Y < 0)
                    {
                        afterJumpRect = new Rectangle((int)worldLocation.X, (int)(worldLocation.Y - 10), 48, sidelength);
                    }
                    else
                    {
                        afterJumpRect = collisionRectangle;
                    }
                    */

                    if (currentState.IsKeyDown(Keys.Space) && velocity.Y <= 0)
                    {
                        afterFallRect = new Rectangle((int)worldLocation.X, (int)(worldLocation.Y - 10), 48, sidelength);
                    }
                    else
                    {
                        afterFallRect = new Rectangle((int)worldLocation.X, (int)(worldLocation.Y + (fallSpeed.Y / 2)), 48, sidelength);
                    }
                    /*
                    for (int temp = itemBlocks.Count - 1; temp >= 0; temp-- )
                    {
                        if ((WorldCenter.Y < itemBlocks[temp].worldLocation.Y) && afterFallRect.Intersects(itemBlocks[temp].collisionRectangle))
                        {
                            canFall = false;
                        }
                        else if ((WorldCenter.Y > itemBlocks[temp].worldCenter.Y) && afterFallRect.Intersects(itemBlocks[temp].collisionRectangle))
                        {
                            velocity.Y = 0;
                        }

                        if ((WorldCenter.X < itemBlocks[temp].worldCenter.X) && (afterHorizontalRect.Intersects(itemBlocks[temp].collisionRectangle)))
                        {
                            velocity.X = 0;
                        }
                        else if ((WorldCenter.X > itemBlocks[temp].worldCenter.X) && (afterHorizontalRect.Intersects(itemBlocks[temp].collisionRectangle)))
                        {
                            velocity.X = 0;
                        }
                    }
                    */
                    
                    //edit to fall code below

                    if (canFall)
                    {
                        if (this.IsVerticallyCollidable && !underwater)
                        {
                            velocity += fallSpeed;
                        }
                        else
                        {
                            velocity += sinkSpeed;
                        }
                    }
                    else
                    {
                        velocity.Y = 0f;
                        onGround = true;
                    }
                    //end change
                    
                    repositionCamera();

                    base.Update(gameTime);
                    if (WorldLocation.X < Camera.Position.X)
                    {
                        worldLocation.X = Camera.Position.X;
                    }
                    if (worldLocation.Y >= Camera.Position.Y + Camera.ViewPortHeight-CollisionRectangle.Height-10)
                    {
                        Kill();
                    }
                    if (onGround && this.animations[currentAnimation].frameUpdated)
                    {
                        if (isBig)
                        {
                           // BumpSmall();
                        }
                        if (isSmall)
                        {
                            //BumpBig();
                        }
                    }
                    previous = Keyboard.GetState();
                }
                else
                {
                    int i = 0;
                    //we have taken control of the player
                }
            }
        }

        public void Jump(bool playSound)
        {
            hasJumped = true;
            velocity.Y = -700;
            if (playSound)
            {
                if (isSmall || isStarSmall)
                {
                    if (Sound_Engine.SoundEngine.getInstance("Jump").State != SoundState.Playing)
                    {
                        Sound_Engine.SoundEngine.Play("Jump");
                    }
                }
                else if (isBig || isFire || isStarBig)
                {
                    if (Sound_Engine.SoundEngine.getInstance("SuperJump").State != SoundState.Playing)
                    {
                        Sound_Engine.SoundEngine.Play("SuperJump");
                    }
                }
            }
        }

        public void smallJump()
        {
            velocity.Y = -700;
        }

        public void bigJump()
        {
            velocity.Y = -900;
        }

        public void tinyJump()
        {
            velocity.Y = -400;
        }

        public void Swim()
        {
            velocity.Y = -250;
        }

        public void StopClimbing()
        {
            isClimbing = false;
            onGround = true;
            isVerticallyCollidable = true;
        }

        public void Climb()
        {
            isClimbing = true;
        }

        public void Kill()
        {
            if (isInvulnerable == false && isStarBig == false && isStarSmall == false &&
                isBig == false && isFire == false)
            {
                LevelManager.loopMusic = false;
                TileMap.StopBackgroundMusic();
                SoundEngine.Play("PlayerDie");
                if (!isLuigi)
                {
                    PlayAnimation("mario_die");
                }
                else
                {
                    PlayAnimation("luigi_die");
                }
                LivesRemaining--;
                velocity.X = 0;
                dead = true;
            }
            else if (isInvulnerable == false && isStarBig == false && isStarSmall == false &&
                isBig == true && isFire == true)
            {
                Small();
                Invulnerable();
            }
        }

        //public void Grow()
        //{
        //    LevelManager.worldPaused = true;
        //    canMove = false;
        //    float elapsed = gametime.TotalGameTime.Milliseconds;
        //    PlayAnimation("m_big_idle");
        //    BumpBig();
        //    while (stateChangeCount < 3)
        //    {
        //        while (stateChangeTimer < stateChangeDelay)
        //        {
        //            stateChangeTimer += elapsed;
        //        }
        //        PlayAnimation("m_small_idle");
        //        BumpSmall();
        //        stateChangeTimer = 0;
        //        while (stateChangeTimer < stateChangeDelay)
        //        {
        //            stateChangeTimer += elapsed;
        //        }
        //        PlayAnimation("m_big_idle");
        //        BumpBig();
        //        stateChangeTimer = 0;
        //        stateChangeCount++;
        //    }
        //    stateChangeCount = 0;
        //    canMove = true;
        //    LevelManager.worldPaused = false;
        //}

        //public void Shrink()
        //{
        //    LevelManager.worldPaused = true;
        //    canMove = false;
        //    PlayAnimation("m_small_idle");
        //    BumpSmall();
        //    while (stateChangeCount < 3)
        //    {
        //        if (stateChangeTimer < stateChangeDelay)
        //        {
        //            stateChangeTimer += elapsed;
        //        }
        //        else
        //        {
        //            PlayAnimation("m_big_idle");
        //            BumpBig();
        //            stateChangeTimer = 0;
        //        }
        //        if (stateChangeTimer < stateChangeDelay)
        //        {
        //            stateChangeTimer += elapsed;
        //        }
        //        else
        //        {
        //            PlayAnimation("m_small_idle");
        //            BumpSmall();
        //            stateChangeTimer = 0;
        //        }
        //        stateChangeCount++;
        //    }
        //    stateChangeCount = 0;
        //    canMove = true;
        //    LevelManager.worldPaused = true;
        //}

        public void FireUpgrade()
        {
            //if (stateChangeTimer < stateChangeDelay)
            //{
            //    stateChangeTimer += elapsed;
            //}
            //else
            //{

            //}
        }

        public void Revive()
        {
            worldLocation = LevelManager.respawnLocation;
            if (!isLuigi)
            {
                PlayAnimation("m_small_idle");
            }
            else
            {
                PlayAnimation("l_small_idle");
            }
            dead = false;
        }

        public void Invulnerable()
        {
            isBig = false;
            isInvulnerable = true;
            isFire = false;
            isStarBig = false;
            isStarSmall = false;


            resetCollisionRectangle();
        }

        public void Small()
        {
            if (isStarSmall == true)
            {
                wasState = "StarSmall";
            }
            if (isInvulnerable == true)
            {
                wasState = "Invulnerable";
            }

            if (hasShrunk == false)
            {
                isShrinking = true;
            }

            if (hasShrunk == true)
            {
                isSmall = true;
                isBig = false;
                isInvulnerable = false;
                isFire = false;
                isStarBig = false;
                isStarSmall = false;

                if (onGround && stateChanged && !isLuigi)
                {
                    PlayAnimation("m_small_idle");
                    //if (wasState != "StarSmall" &&
                    //    wasState != "Invulnerable")
                    //{
                    //    BumpSmall();
                    //}
                }
                else
                {
                    PlayAnimation("l_small_idle");
                    //if (wasState != "StarSmall" &&
                    //    wasState != "Invulnerable")
                    //{
                    //    BumpSmall();
                    //}
                }
                resetCollisionRectangle();
                hasShrunk = false;
            }
        }

        public void Big()
        {
            if (isFire == true)
            {
                wasState = "Fire";
            }
            if (isStarBig == true)
            {
                wasState = "StarBig";
            }

            if (hasGrown == false)
            {
                isGrowing = true;
            }

            if (hasGrown == true)
            {
                isSmall = false;
                isBig = true;
                isInvulnerable = false;
                isFire = false;
                isStarBig = false;
                isStarSmall = false;

                if (onGround && stateChanged && !isLuigi)
                {
                    PlayAnimation("m_big_idle");
                    if (wasState != "Fire" && wasState != "StarBig")
                    {
                        BumpBig();
                    }
                }
                else
                {
                    PlayAnimation("l_big_idle");
                    if (wasState != "Fire" && wasState != "StarBig")
                    {
                        BumpBig();
                    }
                }
                resetCollisionRectangle();
                hasGrown = false;
            }
        }

        public void Fire()
        {
            FireUpgrade();

            isSmall = false;
            isBig = false;
            isInvulnerable = false;
            isFire = true;
            isStarBig = false;
            isStarSmall = false;

            if (onGround && stateChanged)
            {
                PlayAnimation("f_big_idle");
            }
            resetCollisionRectangle();
        }

        public void StarBig()
        {
            if (isBig == true)
            {
                wasState = "Big";
            }
            else
            {
                wasState = "Fire";
            }

            isSmall = false;
            isBig = false;
            isInvulnerable = false;
            isFire = false;
            isStarBig = true;
            isStarSmall = false;

            resetCollisionRectangle();
        }

        public void StarSmall()
        {
            wasState = "Small";

            isSmall = false;
            isBig = false;
            isInvulnerable = false;
            isFire = false;
            isStarBig = false;
            isStarSmall = true;

            resetCollisionRectangle();
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Warp Player to given world and level at world location (x, y)
        /// </summary>
        /// <param name="World">world to load</param>
        /// <param name="Level">level to load</param>
        /// <param name="x">location in pixels</param>
        /// <param name="y">location in pixels</param>
        public void WarpPlayer(int World, int Level, int x, int y,bool ResetTimer)
        {
            LevelManager.LoadLevel(World, Level);
            WorldLocation = new Vector2(x, y);
            Tile_Engine.Camera.Position = new Vector2(x,y);
            countingTime = true;
            if (ResetTimer)
            {
                levelTimer = maxLevelTimer;
            }
        }

        protected void BumpBig()
        {
            //find the location of the block you are standing in and bump above it
            resetCollisionRectangle();
            Rectangle x = TileMap.CellWorldRectangle(TileMap.GetCellByPixelX((int)WorldLocation.X), TileMap.GetCellByPixelY((int)WorldLocation.Y) + 1);
            WorldLocation = new Vector2(WorldLocation.X, TileMap.CellWorldRectangle(TileMap.GetCellByPixelX((int)WorldLocation.X), TileMap.GetCellByPixelY((int)WorldLocation.Y) + 1).Y);
            WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y - collisionRectangle.Height);
        }

        protected void BumpSmall()
        {
            //find the location of the block you are hovering above and bump to be touching it
            resetCollisionRectangle();
            Rectangle x = TileMap.CellWorldRectangle(TileMap.GetCellByPixelX((int)WorldLocation.X), TileMap.GetCellByPixelY((int)WorldLocation.Y) + 1);
            WorldLocation = new Vector2(WorldLocation.X, TileMap.CellWorldRectangle(TileMap.GetCellByPixelX((int)WorldLocation.X), TileMap.GetCellByPixelY((int)WorldLocation.Y) + 2).Y);

            //while (TileMap.CellIsPassableByPixel(WorldLocation))
            //{
            //    WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y + 1);
            //}
            WorldLocation = new Vector2(WorldLocation.X, WorldLocation.Y - collisionRectangle.Height);
        }

        public void repositionCamera()
        {
            int screenLocX = (int)Camera.WorldToScreen(worldLocation).X;

            if (screenLocX > 500)
            {
                Camera.Move(new Vector2(screenLocX - 500, 0));
            }

            if (screenLocX < 200)
            {
                Camera.Move(new Vector2(screenLocX - 200, 0));
            }
        }

        #endregion

        #region Cimematic functions
        public void PlayStanding(GameTime gameTime)
        {
            isVerticallyCollidable = true;
            isHorizontallyCollidable = true;
            string newAnimation = "m_small_idle";
            flipped = false;
            isPlayerFlipped = false;

            if (isStarBig == true)
            {
                newAnimation = "s_big_idle";
            }
            else if (isStarSmall == true)
            {
                newAnimation = "s_small_idle";
            }
            else if (isFire == true)
            {
                newAnimation = "m_fire_idle";
            }
            else if (isBig == true && !isLuigi)
            {
                newAnimation = "m_big_idle";
            }
            else if (isSmall == true && !isLuigi)
            {
                newAnimation = "m_small_idle";
            }
            else if (isBig == true && isLuigi == true)
            {
                newAnimation = "l_big_idle";
            }
            else
            {
                newAnimation = "l_small_idle";
            }

            if (newAnimation != currentAnimation)
            {
                resetCollisionRectangle();
                PlayAnimation(newAnimation);
                resetCollisionRectangle();
            }
            repositionCamera();


            base.Update(gameTime);
        }

        public void PlayWalkRight(GameTime gameTime)
        {
            isVerticallyCollidable = true;
            isHorizontallyCollidable = true;
            string newAnimation = "m_small_walk";
            flipped = false;
            isPlayerFlipped = false;
            if (underwater == false || onGround == true)
            {
                if (isStarBig == true)
                {
                    newAnimation = "s_big_walk";
                }
                else if (isStarSmall == true)
                {
                    newAnimation = "s_small_walk";
                }
                else if (isFire == true)
                {
                    newAnimation = "m_fire_walk";
                }
                else if (isBig == true && !isLuigi)
                {
                    newAnimation = "m_big_walk";
                }
                else if (isSmall == true && !isLuigi)
                {
                    newAnimation = "m_small_walk";
                }
                else if (isBig == true && isLuigi == true)
                {
                    newAnimation = "l_big_walk";
                }
                else
                {
                    newAnimation = "l_small_walk";
                }
            }
            if (!underwater)
            {
                velocity = new Vector2((float)swimspeed, 0);
            }
            else
            {
                velocity = new Vector2(walkspeed, 0);
            }
            if (newAnimation != currentAnimation)
            {
                resetCollisionRectangle();
                PlayAnimation(newAnimation);
                resetCollisionRectangle();
            }
            repositionCamera();

               if (this.IsVerticallyCollidable && !underwater)
                {
                    velocity += new Vector2(0,100);
                }
                else
                {
                    velocity += sinkSpeed;
                }
               
            base.Update(gameTime);
        }

        public void PlayClimb()
        {
            if (isClimbing)
            {
                string newAnimation = "m_small_climb_idle";
                isVerticallyCollidable = false;
                if (isStarBig)
                {
                    newAnimation = "s_big_climb_idle";
                }
                else if (isStarSmall)
                {
                    newAnimation = "s_small_climb_idle";
                }
                else if (isFire)
                {
                    newAnimation = "f_big_climb_idle";
                }
                else if (isBig && !isLuigi)
                {
                    newAnimation = "m_big_climb_idle";
                }
                else if (isSmall && !isLuigi)
                {
                    newAnimation = "m_small_climb_idle";
                }
                else if (isBig && isLuigi)
                {
                    newAnimation = "l_big_climb_idle";
                }
                else
                {
                    newAnimation = "l_small_climb_idle";
                }

                PlayAnimation(newAnimation);
            }
        }

        
        #endregion
    }
}
