﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class PiranhaPlant: Enemy
    {
        public int maxHeight;
        bool movingDown = true;//on true plant moves down on false plant moves up
        public bool canMoveUp = true;

        public PiranhaPlant(Player player,ContentManager content, int x, int y)
            : base(player, content, x, y)
        {
            maxHeight = y;
            topVulnerable = false;
            movementSpeed = .5f;
            worldLocation = new Vector2(x, y);
            collisionRectangle = new Rectangle(0, 0, 48, 48);

            isHorizontallyCollidable = false;
            isVerticallyCollidable = false;

            animations.Add("Chomp", new AnimationStrip(EnemyTexture, EnemyPlist, "piranha", 2));
            animations["Chomp"].LoopAnimation = true;
            animations["Chomp"].FrameLength = .5f;

            currentAnimation = "Chomp";
            Enabled = true;
            flipped = true;
        }

        public override void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.TotalGameTime.Milliseconds / 1000;
            if(movingDown)
            {
                worldLocation += new Vector2(0,movementSpeed);
            }
            else
            {
                if (canMoveUp || worldLocation.Y < maxHeight + CollisionRectangle.Height-5)
                {
                    worldLocation += new Vector2(0, -movementSpeed);
                }
            }
            if (worldLocation.Y > maxHeight + CollisionRectangle.Height)
            {
                movingDown = false;
            }
            if (worldLocation.Y == maxHeight)
            {
                movingDown = true;
            }
            base.Update(gameTime);
        }
    }
}
