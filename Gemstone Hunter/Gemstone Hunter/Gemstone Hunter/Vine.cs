﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class Vine:GameObject
    {
        public string WarpInfo = "001_001_000_000";

        public int i = 0;
        #region Constructor
        public Vine(ContentManager content, int cellX, int cellY, string warpInfo)
        {
            
            worldLocation.X = cellX;
            worldLocation.Y = cellY;

            WarpInfo = warpInfo;
            CollisionRectangle = new Rectangle(0, 0, TileMap.TileWidth, TileMap.TileHeight);
            currentAnimation = "";
            enabled = true;
            i = 0;

            animations.Add("vine_body",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "vine_body",
                    1));

            animations["vine_body"].NextAnimation = "vine_body";
            animations["vine_body"].LoopAnimation = true;

            PlayAnimation("vine_body");
        }
        #endregion

        public void Warp(Player player)
        {
            string world = WarpInfo.Substring(0, 3);
            string level = WarpInfo.Substring(4, 3);
            string x = WarpInfo.Substring(8, 3);
            string y = WarpInfo.Substring(12, 3);

            LevelManager.LoadLevel(Convert.ToInt32(world), Convert.ToInt32(level));
            player.WorldLocation = new Vector2(Convert.ToInt32(x) * Tile_Engine.TileMap.TileWidth, Convert.ToInt32(y) * Tile_Engine.TileMap.TileHeight);
        }
    }
}
