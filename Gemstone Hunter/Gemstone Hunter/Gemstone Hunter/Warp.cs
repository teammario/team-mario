﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Gemstone_Hunter
{
    //class to handle objects warping the player from one location to another
    public class Warp:GameObject
    {
        public string direction = "Down"; // can be Up,Down,Left, or Right
        public string WarpInfo = "001_001_000_000";
        string world;
        string level;
        string x;
        string y;
        #region Constructor
        public Warp(int cellX, int cellY, int width, int height, string warpInfo, string Direction)
        {
            worldLocation.X = cellX;
            worldLocation.Y = cellY;

            WarpInfo = warpInfo;
            direction = Direction;
            CollisionRectangle = new Rectangle(0, 0, width, height);
            currentAnimation = "";
            enabled = true;
        }
        #endregion

        #region Helper Functions
        public void playToWarpAnimation(Player player, GameTime gameTime)
        {
            int elapsed = gameTime.ElapsedGameTime.Milliseconds;
            
            if (Sound_Engine.SoundEngine.getInstance("Pipe").State != SoundState.Playing)
            {
                Sound_Engine.SoundEngine.Play("Pipe");
            }
            player.canMove = false;
            
             world = WarpInfo.Substring(0, 3);
             level = WarpInfo.Substring(4, 3);
             x = WarpInfo.Substring(8, 3);
             y = WarpInfo.Substring(12, 3);
            bool ResetTimer = WarpInfo.ToUpper().EndsWith("R");

            if (player.isWarping)
            {
                switch (direction)
                {
                    case "Down":
                        player.IsVerticallyCollidable = false;
                        if (player.isBig)
                        {
                            player.PlayAnimation("m_big_idle");
                        }
                        else
                        {
                            player.PlayAnimation("m_small_idle");
                        }
                        player.WorldLocation = new Vector2(WorldLocation.X + this.CollisionRectangle.Width / 2 - player.CollisionRectangle.Width / 2,
                        player.WorldLocation.Y + 1);

                        if (player.WorldLocation.Y >= this.WorldLocation.Y + this.CollisionRectangle.Height)
                        {
                            player.isWarping = false;
                            player.isWarpingFrom = true;
                        }
                        break;
                    case "Right":
                        player.IsHorizontallyCollidable = false;
                        if (player.isBig)
                        {
                            player.PlayAnimation("m_big_walk");
                        }
                        else
                        {
                            player.PlayAnimation("m_small_walk");
                        }
                        player.WorldLocation = new Vector2(player.WorldLocation.X + 1,
                             WorldLocation.Y + this.CollisionRectangle.Height - player.CollisionRectangle.Height);

                        if (player.WorldLocation.X >= this.WorldLocation.X + this.CollisionRectangle.Width)
                        {
                            player.canMove = true;
                            player.IsHorizontallyCollidable = true;
                            player.IsVerticallyCollidable = true;
                            player.isWarping = false;
                            player.isWarpingFrom = true;
                        }
                        break;
                }
            }
            else if (player.isWarping  == false)
            {
                if (player.isWarpingFrom)
                {
                    if (!player.didWarp)
                    {
                        player.WarpPlayer(Convert.ToInt32(world), Convert.ToInt32(level),
                            Convert.ToInt32(x) * Tile_Engine.TileMap.TileWidth,
                            Convert.ToInt32(y) * Tile_Engine.TileMap.TileHeight + player.CollisionRectangle.Height *4, ResetTimer);
                        player.WorldLocation = new Vector2(player.WorldLocation.X + this.CollisionRectangle.Width / 2 - player.CollisionRectangle.Width / 2, player.WorldLocation.Y);
                        player.didWarp = true;
                    }
                    playFromWarpAnimation(direction, player);
                }
            }
        }

        public void playFromWarpAnimation(string directionFrom,Player player)
        {
            
            switch (directionFrom)
            {
                case "Down":
                    player.WorldLocation = new Vector2(player.WorldLocation.X,
                    player.WorldLocation.Y - 2);
                    if (player.isBig)
                    {
                        player.PlayAnimation("m_big_idle");
                    }
                    else
                    {
                        player.PlayAnimation("m_small_idle");
                    }
                    if (player.WorldLocation.Y <= Convert.ToInt32(y) * Tile_Engine.TileMap.TileHeight - player.CollisionRectangle.Height/2)
                    {
                        player.isWarpingFrom = false;
                        player.IsVerticallyCollidable = true;
                        player.canMove = true;
                        player.didWarp = false;
                    }
                    break;
                
                case "Right":
                    player.canMove = true;
                    player.IsHorizontallyCollidable = true;
                    player.isWarpingFrom = false;
                    break;
            }
        }
        #endregion
    }
}
