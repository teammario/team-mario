﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class CheepCheep:Enemy
    {
        int center;
        String direction;
        String type;
        protected Vector2 verticalSpeed = new Vector2(0, 0);
        protected int swimMovementSpeed = 30;
        protected int swimJumpHeight = 0;
        protected float verticalMovement = 0f;
        protected Vector2 swimVerticalSpeed = new Vector2(0, 0);
        protected Vector2 swimFallSpeed = new Vector2(0, 1);

        public CheepCheep(Player player, ContentManager content, int cellX, int cellY, String Direction, String Color):base(player,content, cellX, cellY)
        {
            direction = Direction;
            type = Color;
            center = (int)this.worldLocation.Y;
            topVulnerable = false;
            movementSpeed = 150;
            jumpHeight = 800;
            verticalSpeed.Y = -jumpHeight;
            swimVerticalSpeed.Y = swimJumpHeight;
            fallSpeed = new Vector2(0, 10);
            isVerticallyCollidable = false;
            isHorizontallyCollidable = false;
            worldLocation = new Vector2(cellX, cellY);
            collisionRectangle = new Rectangle(0, 0, 48, 48);
            if (type == "Red")
            {
                animations.Add("Fly", new AnimationStrip(EnemyTexture, EnemyPlist, "cheep_red", 2));
            }
            else
            {
                animations.Add("Fly", new AnimationStrip(EnemyTexture, EnemyPlist, "cheep_grey", 2));
            }
            animations["Fly"].LoopAnimation = true;
            animations["Fly"].FrameLength = .75f;
            if (direction == "Left")
            {
                movementSpeed = -movementSpeed;
                swimMovementSpeed = -swimMovementSpeed;
                flipped = false;
            }
            else
            {
                flipped = true;
            }

            currentAnimation = "Fly";
            Enabled = true;
        }
        public override void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.TotalGameTime.Milliseconds / 1000;
            if (worldLocation.Y < 0)
            {
                worldLocation.Y = 1;
            }
            currentAnimation = "Fly";

            // AI Movement
            if (underwater == false)    // not underwater
            {
                velocity = new Vector2(movementSpeed, verticalSpeed.Y += fallSpeed.Y);
            }
            else                        // underwater
            {
                if (type == "Red")      // move straight
                {
                    velocity = new Vector2(swimMovementSpeed, swimVerticalSpeed.Y);
                }
                else                    // move up & down while going forward
                {
                    verticalMovement += .1f;
                    if (verticalMovement >= 360)
                        verticalMovement = 0;
                    swimJumpHeight = (int)(center + ((24) * Math.Sin(Math.PI * 4 * verticalMovement / 180)));
                    swimVerticalSpeed.Y = swimJumpHeight;
                    velocity = new Vector2(swimMovementSpeed, swimVerticalSpeed.Y);
                }
            }
            Vector2 moveAmount = velocity;
            if (player.CollisionRectangle.Intersects(this.CollisionRectangle))
            {
                player.Kill();
            }

            // Cheep Cheep hits border
            if (worldLocation.X <= 0 || worldLocation.X == TileMap.MapWidth - collisionRectangle.Width
                || worldLocation.Y <= TileMap.MapHeight - collisionRectangle.Height
                || worldLocation.Y >= (14*48)
                || player.CollisionRectangle.X - (48 * 12) >= this.WorldRectangle.X)
            {
                this.dead = true;
            }

            base.Update(gameTime);
        }
        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }
    }
}
