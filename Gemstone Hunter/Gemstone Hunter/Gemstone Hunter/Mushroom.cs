﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class Mushroom : GameObject
    {
       
        public int growth;
        private float fallspeed = 12f;
        public bool goRight;
        public string Type;
        private List<BreakBlock> BreakBlocks = new List<BreakBlock>();
        private List<ItemBlock> ItemBlocks = new List<ItemBlock>();
        private const int MAX = 15;
        private int delay = 15;

        public override void Draw(SpriteBatch spriteBatch)
        {

            int depth = 0;

            if (growth <= 48)
            {
                depth = (48 - growth);
                growth++;
            }
            else
            {
                depth = 0;
            }

            
            Rectangle drawRectangle = new Rectangle((int)worldLocation.X, (int)(worldLocation.Y + depth), 
             animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
                spriteBatch.Draw(
                    animations[currentAnimation].Texture,
                    Camera.WorldToScreen(drawRectangle),
                    animations[currentAnimation].FrameRectangle,
                    Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
            /*
                spriteBatch.Draw(
                    animations[currentAnimation].Texture,
                    Camera.WorldToScreen(CollisionRectangle),
                    animations[currentAnimation].FrameRectangle,
                    Color.Black, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
            */
            
            //base.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime)
        {
            //velocity = new Vector2(0, velocity.Y);
            if (delay < MAX)
            {
                delay++;
            }
            else if (passable == true && delay >= MAX)
            {
                IsHorizontallyCollidable = true;
                IsVerticallyCollidable = true;
                passable = false;
            }

            if (growth >= 48)
            {
                if (velocity.X < 50 && velocity.X > -50)
                {
                    goRight = !goRight;
                }
                else if(worldLocation.X < 26)
                {
                    goRight = true;
                }

                if (BreakBlocks.Count > 0)
                {
                    for (int A = BreakBlocks.Count - 1; A >= 0; A--)
                    {
                        if(passable == false && BreakBlocks[A].isBouncable == true && CollisionRectangle.Intersects(BreakBlocks[A].Extended))
                        {
                            velocity = new Vector2(velocity.X, -200);
                            IsHorizontallyCollidable = false;
                            IsVerticallyCollidable = false;
                            passable = true;
                            delay = 0;
                            //goRight = !goRight;
                            if (goRight)
                            {
                                goRight = false;
                            }
                            else
                            {
                                goRight = true;
                            }
                        }
                    }
                }

                if (ItemBlocks.Count > 0)
                {
                    for (int A = ItemBlocks.Count - 1; A >= 0; A--)
                    {
                        if (passable == false && ItemBlocks[A].Jump > 0 && CollisionRectangle.Intersects(ItemBlocks[A].Extended()))
                        {
                            velocity = new Vector2(velocity.X, -200);
                            IsHorizontallyCollidable = false;
                            IsVerticallyCollidable = false;
                            passable = true;
                            delay = 0;
                            //goRight = !goRight;
                            if (goRight)
                            {
                                goRight = false;
                            }
                            else
                            {
                                goRight = true;
                            }
                        }
                    }
                }

                    if (goRight)
                        velocity = new Vector2(80, velocity.Y + fallspeed);
                    else
                        velocity = new Vector2(-80, velocity.Y + fallspeed);
                

                if (Type == "Star")
                {
                //    velocity.X = 150;

                    if (onGround)
                    {
                        velocity = new Vector2(velocity.X , -400);
                    }
                    
                }
            }

            base.Update(gameTime);
        }

        //possible type arguments: Mushroom, OneUp //try also for star
        public Mushroom(int x, int y, ContentManager content, string type, List<BreakBlock> breakBlocks)
        {
            BreakBlocks = breakBlocks;

            ItemBlocks = LevelManager.itemBlocks;
            //worldLocation = new Vector2(x, y);
            enabled = true;
            growth = 0;

            Type = type;

            if (type == "OneUp")
            {
                //animation logic
                animations.Add("OneUp",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "mushroom_1up",
                    1));

                PlayAnimation("OneUp");
               

                //animations["OneUp"].Play();
            }
            else if (type == "Star")
            {
                animations.Add("Star",
                    new AnimationStrip(
                        content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                        new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                        "starman",
                        5));
                animations["Star"].FrameLength = 0.48f;
                PlayAnimation("Star");
            }
            else
            {
                //animation logic
                animations.Add("Mushroom",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "mushroom_regular",
                    1));

                PlayAnimation("Mushroom");
                // animations["Mushroom"].Play();
            }

            CollisionRectangle = new Rectangle(0, 0, 48, 48);
            worldLocation = new Vector2(x, y);

        }
    }
}
