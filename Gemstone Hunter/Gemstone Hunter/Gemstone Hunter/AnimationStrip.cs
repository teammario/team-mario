﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Gemstone_Hunter
{
    public class AnimationStrip
    {
        #region Declarations
        private Texture2D texture;
        private Plist plist;
        
        private float frameTimer = 0f;
        private float frameDelay = 0.05f;

        private int currentFrame;
        public bool frameUpdated;
        private int frameCount;

        private bool loopAnimation = true;
        private bool finishedPlaying = false;

        private string name;
        private string nextAnimation;
        #endregion

        #region Properties
        public int CurrentFrame
        {
            get {return this.currentFrame; }
            set { currentFrame = value; }
        }
        public int FrameWidth
        {
            get { return plist.Sprites[name+"_"+this.currentFrame+".png"].Width; }
        }

        public int FrameHeight
        {
            get { return plist.Sprites[name + "_" + this.currentFrame + ".png"].Height; }
        }

        public Texture2D Texture
        {
            get { return texture; }
            set { texture = value; }
        }

        public Plist Plist
        {
            get { return plist; }
            set { plist = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string NextAnimation
        {
            get { return nextAnimation; }
            set { nextAnimation = value; }
        }

        public bool LoopAnimation
        {
            get { return loopAnimation; }
            set { loopAnimation = value; }
        }

        public bool FinishedPlaying
        {
            get { return finishedPlaying; }
        }

        public int FrameCount
        {
            get { return frameCount; }
            set { frameCount = value; }
        }

        public float FrameLength
        {
            get { return frameDelay; }
            set { frameDelay = value; }
        }

        public Rectangle FrameRectangle
        {
            get
            {
                return plist.Sprites[name+"_"+currentFrame+".png"];
            }
        }
        #endregion

        #region Constructor
        public AnimationStrip(Texture2D texture, Plist plist, string name, int frames)
        {
            this.texture = texture;
            this.plist = plist;
            this.FrameCount = frames;
            if (name.EndsWith(".png"))
            {
                name = name.Replace(".png", "");
            }
            this.name = name;
        }
        #endregion

        #region Public Methods
        public void Play()
        {
            currentFrame = 0;
            finishedPlaying = false;
        }

        public void Update(GameTime gameTime)
        {
            frameUpdated = false;
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            frameTimer += elapsed;

            if (frameTimer >= frameDelay)
            {
                currentFrame++;
                frameUpdated = true;
                if (currentFrame >= FrameCount)
                {
                    frameUpdated = false;
                    if (loopAnimation)
                    {
                        currentFrame = 0;
                    }
                    else
                    {
                        currentFrame = FrameCount - 1;
                        finishedPlaying = true;
                    }
                }

                frameTimer = 0f;
            }
        }
        #endregion

    }
}
