﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class BulletBillGenerator:Enemy
    {
        public List<BulletBill> BulletBillGen = new List<BulletBill>();
        Player player;
        ContentManager content;
        String Direction;

        public BulletBillGenerator(Player player, ContentManager content, int cellX, int cellY, String Direction)
            : base(player, content, cellX, cellY)
        {
            BulletBillGen.Clear();
            this.player = player;
            this.content = content;
            this.Direction = Direction;
            this.worldLocation = new Vector2(cellX, cellY);

            currentAnimation = "";
        }
        public void Shoot(GameTime gameTime)
        {
            BulletBillGen.Add(new BulletBill(player, content, (int)this.worldLocation.X, (int)this.worldLocation.Y, Direction));
        }
        public override void Update(GameTime gameTime)
        {
            if (player.CollisionRectangle.X + (48 * 15) >= this.WorldRectangle.X && BulletBillGen.Count < 2)
            {
                Shoot(gameTime);
            }
            foreach (BulletBill BB in BulletBillGen)
            {
                if (BB.dead)
                {
                    BulletBillGen.Remove(BB);
                    Shoot(gameTime);
                    break;
                }
            }

            base.Update(gameTime);
        }
    }
}
