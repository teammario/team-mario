﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class Trampoline : GameObject
    {

//        private Dictionary<string, AnimationStrip> animations =
//            new Dictionary<string, AnimationStrip>();

//        public Vector2 worldLocation;
//        public Vector2 worldCenter;
//        public Rectangle collisionRectangle;
 //       private string currentAnimation;
        private Player player;
        private bool canBounce = true;
        private KeyboardState previous;
        private int counter = 0;
        private Rectangle afterFallRectangle;

        public override void Update(GameTime gameTime)
        {
            //base.Update(gameTime);

            KeyboardState keyState = Keyboard.GetState();

            afterFallRectangle = new Rectangle((int)player.WorldLocation.X, (int)player.WorldLocation.Y + (player.CollisionRectangle.Height) + 5, 
                                                         (int)player.CollisionRectangle.Width, 12);

            if (afterFallRectangle.Intersects(CollisionRectangle))
            {
                player.Passable = true;
                Passable = true;
            }
            else if (player.WorldLocation.X > WorldLocation.X - 60
                && player.WorldLocation.X < WorldLocation.X + 98)
            {
                player.Passable = false;
                Passable = false;
            }

            if (player.CollisionRectangle.Intersects(CollisionRectangle) && player.WorldCenter.Y < WorldCenter.Y + 12)
            {
                if (canBounce)
                {
                   // animations["Bounce"].Play();
                   // currentAnimation = "Bounce";
                   // canBounce = false;

                    PlayAnimation("Bounce");
                    canBounce = false;
                }

            //    if (animations["Bounce"].FinishedPlaying)
               // {
              //      PlayAnimation("Trampoline");
              //  }

                if ((player.isSmall && player.WorldCenter.Y > worldLocation.Y) || (player.isBig && player.WorldCenter.Y > worldLocation.Y - 18)
                    || (player.WorldCenter.Y > WorldCenter.Y))
                {
                    if (keyState.IsKeyDown(Keys.Space) && !(previous.IsKeyDown(Keys.Space)))
                    {
                        player.bigJump();
                    }
                    else
                    {
                        player.smallJump();
                    }
                }
                
            }

            if (!(player.CollisionRectangle.Intersects(CollisionRectangle)) && canBounce == false)
            {
                canBounce = true;
            }

            if (counter >= 7)
            {
                previous = keyState;
                counter = 0;
            }
            
            counter++;

            base.Update(gameTime);
            //updateAnimation(gameTime);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            int depth;

            depth = (48 + 24) - animations[currentAnimation].FrameRectangle.Height;

            if (Passable)
            {
                Rectangle drawRectangle = new Rectangle((int)worldLocation.X, (int)(worldLocation.Y + depth),
                 animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
                spriteBatch.Draw(
                    animations[currentAnimation].Texture,
                    Camera.WorldToScreen(drawRectangle),
                    animations[currentAnimation].FrameRectangle,
                    Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
            }
            else
            {
                Rectangle drawRectangle = new Rectangle((int)worldLocation.X, (int)(worldLocation.Y + depth),
                 animations[currentAnimation].FrameRectangle.Width, animations[currentAnimation].FrameRectangle.Height);
                spriteBatch.Draw(
                    animations[currentAnimation].Texture,
                    Camera.WorldToScreen(drawRectangle),
                    animations[currentAnimation].FrameRectangle,
                    Color.White, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
            }
            /*
                   spriteBatch.Draw(
                animations[currentAnimation].Texture,
                Camera.WorldToScreen(afterFallRectangle),
                animations[currentAnimation].FrameRectangle,
                player.Passable ? Color.White : Color.Black, 0.0f, Vector2.Zero, SpriteEffects.None, 0.9f);
            */
            //base.Draw(spriteBatch);
        }

//        private void updateAnimation(GameTime gameTime)
//        {
//            if (animations.ContainsKey(currentAnimation))
//            {
//                if (animations[currentAnimation].FinishedPlaying)
//                {
//                    currentAnimation = "Trampoline";
//                    animations["Trampoline"].Play();
//                }
//                else
//                {
//                    animations[currentAnimation].Update(gameTime);
//                }
//            }
//        }

        public Trampoline(int x, int y, ContentManager content, Player gamePlayer)
        {
            enabled = true;
            player = gamePlayer;
            collisionRectangle = new Rectangle(x, y, 48, 48 + 24);
            worldLocation = new Vector2(x, y);
            Passable = false;
            //IsHorizontallyCollidable = true;
            //IsVerticallyCollidable = false;
            
            //worldCenter = new Vector2(x + 24, y + 48);
            animations.Add("Trampoline",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "trampoline",
                    1));

            animations.Add("Bounce",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "trampoline",
                    3));

            animations["Bounce"].LoopAnimation = false;
            animations["Bounce"].FrameLength = 0.07f;
            animations["Bounce"].NextAnimation = "Trampoline";
            

            animations["Trampoline"].LoopAnimation = true;
            animations["Trampoline"].FrameLength = 0.37f;
//            animations["Trampoline"].Play();
//            currentAnimation = "Trampoline";


            
            PlayAnimation("Trampoline");

            previous = Keyboard.GetState();

        }
    }
}
