﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using Tile_Engine;

namespace Gemstone_Hunter
{
    public class Firework : GameObject
    {
        public Firework(ContentManager content, int x, int y)
        {
            WorldLocation = new Vector2(x, y);
            animations.Add("firework",
                new AnimationStrip(
                    content.Load<Texture2D>(@"Textures\Super_Mario_Sprites\Items"),
                    new Plist(@"Content\Textures\Super_Mario_Sprites\Items.plist"),
                    "firework",
                    3));

            animations["firework"].LoopAnimation = false;
            animations["firework"].FrameLength = .33f;

            enabled = true;
            PlayAnimation("firework");
        }

        public override void Update(GameTime gameTime)
        {
            if (animations[currentAnimation].frameUpdated)
            {
               // worldLocation = new Vector2(worldLocation.X + CollisionRectangle.Width / 2, worldLocation.Y - CollisionRectangle.Height / 2);
            }
            base.Update(gameTime);
        }
    }
}