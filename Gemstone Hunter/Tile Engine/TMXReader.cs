﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.Xna.Framework;
namespace Tile_Engine
{
 
        public static class TMXReader
        {
            public static List<List<int>> tileLayers = new List<List<int>>();
            public static Dictionary<string, int> tileLayerList = new Dictionary<string, int>();
            public static List<Boolean> passable = new List<bool>();
            public static List<Boolean> breakable = new List<bool>();
            public static List<Boolean> climbable = new List<bool>();

            public static List<List<TMXObject>> objectLayers = new List<List<TMXObject>>();
            // to pull info from a new object use objectLayers["LayerName"].propertyname
            public static Dictionary<string, int> objectLayerList = new Dictionary<string, int>();
            

            public static int mapWidth = 1;
            public static int mapHeight = 1;
            public static int tileWidth = 1;
            public static int tileHeight = 1;
            public static Color background = Color.CornflowerBlue;
            public static string music = "OutsideTheme";
            public static bool underwater = false;

            private static void Initialize()
            {
                tileLayers = new List<List<int>>();
                tileLayerList = new Dictionary<string, int>();
                passable = new List<bool>();
                breakable = new List<bool>();
                climbable = new List<bool>();

                objectLayers = new List<List<TMXObject>>();
                objectLayerList = new Dictionary<string, int>();

                mapWidth = 1;
                mapHeight = 1;
                tileWidth = 1;
                tileHeight = 1;
            }

            public static void read(string filename)
            {
                Initialize();

                XmlReader reader = XmlReader.Create(filename);
                string name = "";
                bool isObject = false;
                int tileID = 0;

                //tile type layers
                tileLayerList.Add("Background", tileLayerList.Count);
                tileLayers.Add(new List<int>());
                tileLayerList.Add("Foreground", tileLayerList.Count);
                tileLayers.Add(new List<int>());

                objectLayerList.Add("Enemy", objectLayerList.Count);
                objectLayers.Add(new List<TMXObject>());
                objectLayerList.Add("Interaction", objectLayerList.Count);
                objectLayers.Add(new List<TMXObject>());
                objectLayerList.Add("Spawn", objectLayerList.Count);
                objectLayers.Add(new List<TMXObject>());

                while (reader.Read())
                {
                    if (reader.HasAttributes)
                    {

                        switch (reader.Name)
                        {
                            case "map": //map properties
                                mapWidth = Convert.ToInt32(reader.GetAttribute("width"));
                                mapHeight = Convert.ToInt32(reader.GetAttribute("height"));
                                tileWidth = Convert.ToInt32(reader.GetAttribute("tilewidth"));
                                tileHeight = Convert.ToInt32(reader.GetAttribute("tileheight"));
                                string colorstring = reader.GetAttribute("backgroundcolor");
                                colorstring = colorstring.Replace("#", "");
                                int r = Convert.ToInt32(colorstring.Substring(0, 2),16);
                                int g = Convert.ToInt32(colorstring.Substring(2, 2),16);
                                int b = Convert.ToInt32(colorstring.Substring(4, 2),16);
                                background = new Color(r,g,b);
                                break;
                            case "layer": //tile layers
                                name = reader.GetAttribute("name");
                                break;
                            case "tile":
                                if (tileLayerList.ContainsKey(name))
                                {
                                    int i = Convert.ToInt32(reader.GetAttribute("gid"));

                                    tileLayers[tileLayerList[name]].Add(i);
                                }
                                else if (objectLayerList.ContainsKey(name))
                                {
                                    objectLayers[objectLayerList[name]].Add(new TMXObject(reader.GetAttribute("name"),
                                        Convert.ToInt32(reader.GetAttribute("x")), Convert.ToInt32(reader.GetAttribute("y")),
                                        Convert.ToInt32(reader.GetAttribute("width")), Convert.ToInt32(reader.GetAttribute("height"))));
                                }
                                else
                                {
                                    tileID =Convert.ToInt32(reader.GetAttribute("id"))+1; //there is a discrepency in how TMX files write tiles
                                }
                                break;
                            case "objectgroup": //object layers
                                name = reader.GetAttribute("name");

                                break;
                            case "object":
                                isObject = true;
                                objectLayers[objectLayerList[name]].Add(new TMXObject(reader.GetAttribute("name"),
                                    Convert.ToInt32(reader.GetAttribute("x")), Convert.ToInt32(reader.GetAttribute("y")),
                                    Convert.ToInt32(reader.GetAttribute("width")), Convert.ToInt32(reader.GetAttribute("height"))));

                                break;
                            case "property": //a property set for an object or tile
                                if (reader.GetAttribute("name") == "Color")
                                {
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count-1].color = reader.GetAttribute("value");
                                }
                                if (reader.GetAttribute("name") == "BackgroundMusic")
                                {
                                    music = reader.GetAttribute("value");
                                }
                                if (reader.GetAttribute("name") == "Underwater")
                                {
                                    underwater = Convert.ToBoolean(reader.GetAttribute("value"));
                                }
                                if (reader.GetAttribute("name") == "Direction")
                                {
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count-1].direction = reader.GetAttribute("value");
                                }
                                if (reader.GetAttribute("name") == "ObjectTypes")
                                {
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count-1].objectTypes = reader.GetAttribute("value");
                                }
                                if (reader.GetAttribute("name") == "Item")
                                {
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count-1].item = reader.GetAttribute("value");
                                }
                                if (reader.GetAttribute("name") == "Clockwise")
                                {
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count - 1].clockwise = Convert.ToBoolean(reader.GetAttribute("value"));
                                }
                                if (reader.GetAttribute("name") == "Number")
                                {
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count - 1].fireNo = Convert.ToInt32(reader.GetAttribute("value"));
                                }
                                if (reader.GetAttribute("name") == "Count")
                                {
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count-1].count = Convert.ToInt32(reader.GetAttribute("value"));
                                }
                                if (reader.GetAttribute("name") == "Jump")
                                {
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count - 1].bossJump = Convert.ToBoolean(reader.GetAttribute("value"));
                                }
                                if (reader.GetAttribute("name") == "Fire")
                                {
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count - 1].bossFire = Convert.ToBoolean(reader.GetAttribute("value"));
                                }
                                if (reader.GetAttribute("name") == "Hammer")
                                {
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count - 1].bossHammer = Convert.ToBoolean(reader.GetAttribute("value"));
                                }
                                if (reader.GetAttribute("name") == "Move")
                                {
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count - 1].bossMove = Convert.ToBoolean(reader.GetAttribute("value"));
                                }
                                if (reader.GetAttribute("name") == "Block_type")
                                {
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count - 1].blockType = reader.GetAttribute("value");
                                }
                                if (reader.GetAttribute("name") == "StartFlag")
                                {
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count - 1].startFlag = Convert.ToBoolean(reader.GetAttribute("value"));
                                }
                                if (reader.GetAttribute("name") == "GoombaColor")
                                {
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count - 1].goombaType = reader.GetAttribute("value");
                                }
                                if (reader.GetAttribute("name") == "WarpInfo")
                                {
                                    string temp = reader.GetAttribute("value");
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count-1].warpInfo = temp; 
                                }
                                if (reader.GetAttribute("name") == "ID")
                                {
                                    objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count-1].ID = Convert.ToInt32(reader.GetAttribute("value"));
                                }
                                if (reader.GetAttribute("name") == "Breakable")
                                {
                                    if (isObject)
                                    {
                                        objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count].breakable = Convert.ToBoolean(reader.GetAttribute("value"));
                                    }
                                    else
                                    {
                                        if (breakable.Count < tileID)
                                        {
                                            for (int j = breakable.Count; j < tileID; j++)
                                            {
                                                breakable.Add(false);
                                            }
                                        }
                                        breakable.Add(Convert.ToBoolean(reader.GetAttribute("value")));
                                    }
                                }
                                if (reader.GetAttribute("name") == "Climbable")
                                {
                                    if (isObject)
                                    {
                                        objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count].climbable = Convert.ToBoolean(reader.GetAttribute("value"));
                                    }
                                    else
                                    {
                                        if (climbable.Count < tileID)
                                        {
                                            for (int j = climbable.Count; j < tileID; j++)
                                            {
                                                climbable.Add(false);
                                            }
                                        }
                                        climbable.Add(Convert.ToBoolean(reader.GetAttribute("value")));
                                    }
                                }
                                if (reader.GetAttribute("name") == "Passable")
                                {
                                    if (isObject)
                                    {
                                        objectLayers[objectLayerList[name]][objectLayers[objectLayerList[name]].Count-1].passable = Convert.ToBoolean(reader.GetAttribute("Passable"));
                                    }
                                    else 
                                    {
                                        if (passable.Count < tileID)
                                        {
                                            for (int j = passable.Count; j < tileID; j++)
                                            {
                                                passable.Add(true);
                                            }
                                        }
                                        passable.Add(Convert.ToBoolean(reader.GetAttribute("value")));
                                    }

                                }
                                break;
                        }

                    }
                }
                reader.Close();
            }
        }

        public class TMXObject
        {
            public string name = "";
            public int x = 0;
            public int y = 0;
            public int width = 0;
            public int height = 0;
            public string color = "";
            public string direction = "";
            public string objectTypes = ""; //walker, jumper, Swimmer, Shooter, Piranha Plant, Firebar
            public string item = "";
            public int count = 0;
            public string warpInfo = "";
            public int ID = 0;
            public bool passable = true;
            public bool breakable = false;
            public bool climbable = false;

            public string goombaType = "";
            public bool startFlag = true;
            public bool bossMove = false;
            public bool bossFire = false;
            public bool bossHammer = false;
            public bool bossJump = false;
            public bool clockwise = true;
            public int fireNo = 1;
            public string blockType = "";
            public TMXObject(string name, int x, int y, int width, int height)


            {
                this.name = name;
                this.x = x;
                this.y = y;
                this.width = width;
                this.height = height;
            }
        }
    }