﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Tile_Engine
{
    [Serializable]
    public class MapSquare
    {
        #region Declarations
        public int[] LayerTiles = new int[2];
        public bool Passable = true;
        public bool Breakable = false;
        public bool Climbable = false;
        #endregion

        #region Constructor
        public MapSquare(
            int background,
            int foreground,
            bool passable,
            bool breakable,
            bool climbable)
        {
            LayerTiles[0] = background;
            LayerTiles[1] = foreground;
            Passable = passable;
            Breakable = breakable;
            Climbable = climbable;
        }
        #endregion

        #region Public Methods
        public void TogglePassable()
        {
            Passable = !Passable;
        }
        #endregion

    }
}
