using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Storage;
using System.IO;
using System.Xml.Serialization;

namespace Tile_Engine
{
    public static class TileMap
    {
        #region Declarations
        public static int TileWidth = 48;
        public static int TileHeight = 48;
        public static int MapWidth = 160;
        public static int MapHeight = 12;
        public static int MapLayers = 2;
        public static bool WaterLevel = false;
        public static string BackgroundMusic;
        public static Vector2 StartLocation = Vector2.Zero;

        public static MapSquare[,] mapCells;
        
        public static SpriteFont spriteFont;
        private static Texture2D tileSheet;
        #endregion

        public static MapSquare[,] MapCells
        {
            get
            {
                return mapCells;
            }
            set
            {
                mapCells = value;
            }
        }

        #region Initialization
        static public void Initialize(Texture2D tileTexture, string fileName)
        {
            tileSheet = tileTexture;
            LoadMap(fileName);
        }
        #endregion

        #region Tile and Tile Sheet Handling
        public static int TilesPerRow
        {
            get { return tileSheet.Width / TileWidth; }
        }

        public static Rectangle TileSourceRectangle(int tileIndex)
        {
            return new Rectangle(
                ((tileIndex % TilesPerRow) * TileWidth)-TileWidth,
                (tileIndex / TilesPerRow) * TileHeight,
                TileWidth,
                TileHeight);
        }
        #endregion

        #region Information about Map Cells
        static public int GetCellByPixelX(int pixelX)
        {
            return pixelX / TileWidth;
        }

        static public int GetCellByPixelY(int pixelY)
        {
            return pixelY / TileHeight;
        }

        static public Vector2 GetCellByPixel(Vector2 pixelLocation)
        {
            return new Vector2(
                GetCellByPixelX((int)pixelLocation.X),
                GetCellByPixelY((int)pixelLocation.Y));
        }

        static public Vector2 GetCellCenter(int cellX, int cellY)
        {
            return new Vector2(
                (cellX * TileWidth) + (TileWidth / 2),
                (cellY * TileHeight) + (TileHeight / 2));
        }

        static public Vector2 GetCellCenter(Vector2 cell)
        {
            return GetCellCenter(
                (int)cell.X,
                (int)cell.Y);
        }

        static public Rectangle CellWorldRectangle(int cellX, int cellY)
        {
            return new Rectangle(
                cellX * TileWidth,
                cellY * TileHeight,
                TileWidth,
                TileHeight);
        }

        static public Rectangle CellWorldRectangle(Vector2 cell)
        {
            return CellWorldRectangle(
                (int)cell.X,
                (int)cell.Y);
        }

        static public Rectangle CellScreenRectangle(int cellX, int cellY)
        {
            return Camera.WorldToScreen(CellWorldRectangle(cellX, cellY));
        }

        static public Rectangle CellSreenRectangle(Vector2 cell)
        {
            return CellScreenRectangle((int)cell.X, (int)cell.Y);
        }

        static public bool CellIsPassable(int cellX, int cellY)
        {
            MapSquare square = GetMapSquareAtCell(cellX, cellY);

            if (square == null)
                return false;
            else
                return square.Passable;
        }

        static public bool CellIsPassable(Vector2 cell)
        {
            return CellIsPassable((int)cell.X, (int)cell.Y);
        }

        static public bool CellIsPassableByPixel(Vector2 pixelLocation)
        {
            return CellIsPassable(
                GetCellByPixelX((int)pixelLocation.X),
                GetCellByPixelY((int)pixelLocation.Y));
        }

        static public bool CellIsBreakable(int cellX, int cellY)
        {
            MapSquare square = GetMapSquareAtCell(cellX, cellY);

            if (square == null)
            {
                return false;
            }
            else
            {
                return square.Breakable;
            }
        }

        static public bool CellIsBreakable(Vector2 cell)
        {
            return CellIsBreakable((int)cell.X, (int)cell.Y); 
        }

        static public bool CellIsBreakableByPixel(Vector2 pixelLocation)
        {
            return CellIsBreakable(
                GetCellByPixelX((int)pixelLocation.X),
                GetCellByPixelY((int)pixelLocation.Y));
            
        }

        #endregion

        #region Information about MapSquare objects
        static public MapSquare GetMapSquareAtCell(int tileX, int tileY)
        {
            if ((tileX >= 0) && (tileX < MapWidth) &&
                (tileY >= 0) && (tileY < MapHeight))
            {
                return mapCells[tileX, tileY];
            }
            else
            {
                return null;
            }
        }

        static public void SetMapSquareAtCell(
            int tileX,
            int tileY,
            MapSquare tile)
        {
            if ((tileX >= 0) && (tileX < MapWidth) &&
                (tileY >= 0) && (tileY < MapHeight))
            {
                mapCells[tileX, tileY] = tile;
            }
        }

        static public void SetTileAtCell(
            int tileX,
            int tileY,
            int layer,
            int tileIndex)
        {
            if ((tileX >= 0) && (tileX < MapWidth) &&
                (tileY >= 0) && (tileY < MapHeight))
            {
                mapCells[tileX, tileY].LayerTiles[layer] = tileIndex;
            }
        }

        static public MapSquare GetMapSquareAtPixel(int pixelX, int pixelY)
        {
            return GetMapSquareAtCell(
                GetCellByPixelX(pixelX),
                GetCellByPixelY(pixelY));
        }

        static public MapSquare GetMapSquareAtPixel(Vector2 pixelLocation)
        {
            return GetMapSquareAtPixel(
                (int)pixelLocation.X,
                (int)pixelLocation.Y);
        }

        #endregion

        #region Loading and Clearing Maps
        public static void LoadMap(string filename)
        {
            try
            {
                Sound_Engine.SoundEngine.Stop(BackgroundMusic);
                TMXReader.read(filename);
                MapHeight = TMXReader.mapHeight;
                MapWidth = TMXReader.mapWidth;
                TileHeight = TMXReader.tileHeight;
                TileWidth = TMXReader.tileWidth;

                mapCells = new MapSquare[MapWidth, MapHeight];
                BackgroundMusic = TMXReader.music;
                for (int x = 0; x < MapWidth; x++)
                    for (int y = 0; y < MapHeight; y++)
                    {
                        mapCells[x, y] = new MapSquare(
                            TMXReader.tileLayers[TMXReader.tileLayerList["Background"]][x + (MapWidth * y)],
                            TMXReader.tileLayers[TMXReader.tileLayerList["Foreground"]][x + (MapWidth * y)],
                            TMXReader.passable[TMXReader.tileLayers[TMXReader.tileLayerList["Background"]][x + (MapWidth * y)]]
                                && TMXReader.passable[TMXReader.tileLayers[TMXReader.tileLayerList["Foreground"]][(x + (MapWidth * y))]],
                                TMXReader.breakable[TMXReader.tileLayers[TMXReader.tileLayerList["Background"]][x + (MapWidth * y)]]
                                || TMXReader.breakable[TMXReader.tileLayers[TMXReader.tileLayerList["Foreground"]][(x + (MapWidth * y))]] ,
                                TMXReader.climbable[TMXReader.tileLayers[TMXReader.tileLayerList["Background"]][x + (MapWidth * y)]]
                                && TMXReader.climbable[TMXReader.tileLayers[TMXReader.tileLayerList["Foreground"]][(x + (MapWidth * y))]]);
                    }
            }
            catch
            {
                ClearMap();
            }

            WaterLevel = TMXReader.underwater;
            
            for (int k = 0; k < TMXReader.objectLayers[TMXReader.objectLayerList["Spawn"]].Count; k++)
            {
                if (TMXReader.objectLayers[TMXReader.objectLayerList["Spawn"]][k].name == "Start")
                {
                    StartLocation = new Vector2(TMXReader.objectLayers[TMXReader.objectLayerList["Spawn"]][k].x, TMXReader.objectLayers[TMXReader.objectLayerList["Spawn"]][k].y);
                }
            }

            //sample of how to get all of a type of object from TMXReader
            //for (int k = 0; k < TMXReader.objectLayers[TMXReader.objectLayerList["Spawn"]].Count; k++)
            //{
            //    if (TMXReader.objectLayers[TMXReader.objectLayerList["Spawn"]][k].name == "QuestionBlock")
            //    {
            //        //sample of how to use an object
            //    }
            //}
        }

        public static void ClearMap()
        {
            for (int x = 0; x < MapWidth; x++)
                for (int y = 0; y < MapHeight; y++)
                    {
                        mapCells[x, y] = new MapSquare(0, 0, true, false,false);
                    }
        }

        public static void StopBackgroundMusic()
        {
            Sound_Engine.SoundEngine.Stop(BackgroundMusic);
        }
        #endregion

        #region Drawing
        static public void Draw(SpriteBatch spriteBatch)
        {
            int startX = GetCellByPixelX((int)Camera.Position.X);
            int endX = GetCellByPixelX((int)Camera.Position.X +
                  Camera.ViewPortWidth);

            int startY = GetCellByPixelY((int)Camera.Position.Y);
            int endY = GetCellByPixelY((int)Camera.Position.Y +
                      Camera.ViewPortHeight);

            for (int x = startX; x <= endX; x++)
                for (int y = startY; y <= endY; y++)
                {
                    for (int z = 0; z < MapLayers; z++)
                    {
                        if ((x >= 0) && (y >= 0) &&
                            (x < MapWidth) && (y < MapHeight))
                        {
                            if (mapCells[x, y].LayerTiles[z] != 0)
                            {
                                spriteBatch.Draw(
                                  tileSheet,
                                  CellScreenRectangle(x, y),
                                  TileSourceRectangle(mapCells[x, y].LayerTiles[z]),
                                  Color.White,
                                  0.0f,
                                  Vector2.Zero,
                                  SpriteEffects.None,
                                  1f - ((float)z * 0.5f));
                            }
                        }
                    }

                }
        }
        #endregion

    }
}
